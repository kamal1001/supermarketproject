using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Text.Style;
using Android.Util;
using Android.Views;
using Android.Widget;
using SupermarketProject.Common;
using SupermarketProject.Common.CustomRenderers;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Droid.Renderers;
using Microsoft.AppCenter.Crashes;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using XLabs.Forms.Controls;

[assembly: ExportRenderer(typeof(CustomizableLabel), typeof(CustomizableLabelRenderer))]
namespace SupermarketProject.Droid.Renderers
{
    public class CustomizableLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement != null || Element == null)
                return;
            var labelElement = Element as CustomizableLabel;
            if (labelElement == null)
                return;

            var label = Control; // for example
            label.SetLineSpacing(label.LineSpacingExtra, label.LineSpacingMultiplier * labelElement.LineSpacingMultiplier);
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, $"fonts/{labelElement.FontType}.ttf");


            if (labelElement.StrikeThroughTextEnabled && labelElement.StrikeThroughSpansIndexList.Count == 0)
                label.PaintFlags |= PaintFlags.StrikeThruText;

            if (labelElement.UnderlineEnabled)
                label.PaintFlags |= PaintFlags.UnderlineText;

            if (labelElement.FormattedText != null)
            {
                if (labelElement.Text != null && labelElement.Text.Length > 0)
                {
                    HandleFormattedText(labelElement, font);
                }
                return;
            }

            label.Typeface = font;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName != "FormattedText" && e.PropertyName != "UnderlineEnabled" && e.PropertyName != "LineSpacingMultiplier" && e.PropertyName != "StrikeThroughTextEnabled")
                return;
            var labelElement = Element as CustomizableLabel;
            var label = Control; // for example

            if (label == null || labelElement == null)
                return;
            if (e.PropertyName == "FormattedText")
            {
                Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, $"fonts/{labelElement.FontType}.ttf");

                if (labelElement.FormattedText != null)
                {
                    if (labelElement.Text != null && labelElement.Text.Length > 0)
                    {
                        HandleFormattedText(labelElement, font);
                    }
                    return;
                }

                label.Typeface = font;
            }
            else if (e.PropertyName == "UnderlineEnabled")
            {
                if (labelElement.UnderlineEnabled)
                {
                    label.PaintFlags |= PaintFlags.UnderlineText;
                }
                else
                {
                    label.PaintFlags &= ~PaintFlags.UnderlineText;
                }
            }
            else if (e.PropertyName == "LineSpacingMultiplier")
            {
                label.SetLineSpacing(label.LineSpacingExtra, label.LineSpacingMultiplier * labelElement.LineSpacingMultiplier);
            }
            else if (e.PropertyName == "StrikeThroughTextEnabled")
            {
                if (labelElement.StrikeThroughTextEnabled)
                {
                    label.PaintFlags |= PaintFlags.StrikeThruText;
                }
                else
                {
                    label.PaintFlags &= ~PaintFlags.StrikeThruText;
                }
            }
        }

        private void HandleFormattedText(CustomizableLabel label, Typeface font)
        {
            //if (label == null || !UsesCustomFonts(label))
            //	return;

            var builder = new SpannableStringBuilder();

            foreach (var span in label.FormattedText.Spans)
            {
                //CustomTypefaceSpan
                ForegroundColorSpan spanForegroundColor = span.ForegroundColor == Xamarin.Forms.Color.Default ? null : new ForegroundColorSpan(span.ForegroundColor.ToAndroid());
                BackgroundColorSpan spanBackgroundColor = span.BackgroundColor == Xamarin.Forms.Color.Default ? null : new BackgroundColorSpan(span.BackgroundColor.ToAndroid());
                StyleSpan spanStyle = GetSpanStyle(span);
                AbsoluteSizeSpan spanSize = double.IsNaN(span.FontSize) ? null : spanSize = new AbsoluteSizeSpan((int)span.FontSize, true);
                //Typeface tf = (string.IsNullOrEmpty(span.FontFamily)) ? Typeface.Default : GetCustomFont(span.FontFamily, span.FontAttributes);

                //// we don't cache the spans' spanTypeface. create new (based on the cached font typeface)
                CustomTypefaceSpan spanTypeface = new CustomTypefaceSpan(span.FontFamily, font, span.ForegroundColor.ToAndroid());

                var lastPosition = builder.Length();

                builder.Append(string.IsNullOrEmpty(span.Text) ? "" : span.Text);

                var len = builder.Length();
                if (lastPosition < len)
                {
                    try
                    {
                        if (spanForegroundColor != null)
                            builder.SetSpan(spanForegroundColor, lastPosition, len, SpanTypes.Composing);

                        if (spanBackgroundColor != null)
                            builder.SetSpan(spanBackgroundColor, lastPosition, len, SpanTypes.Composing);

                        builder.SetSpan(spanTypeface, lastPosition, len, SpanTypes.Composing);


                        if (spanStyle != null)
                            builder.SetSpan(spanStyle, lastPosition, len, SpanTypes.Composing);

                        if (spanSize != null)
                            builder.SetSpan(spanSize, lastPosition, len, SpanTypes.Composing);

                        if (label.StrikeThroughTextEnabled && label.StrikeThroughSpansIndexList.Contains(label.FormattedText.Spans.IndexOf(span)))
                            builder.SetSpan(new StrikethroughSpan(), lastPosition, len, SpanTypes.Composing);
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex);
                        System.Diagnostics.Debug.WriteLine(ex);
                    }
                }
            }

            Control.TextFormatted = builder;
        }

        private StyleSpan GetSpanStyle(Span span)
        {
            if (span == null || span.FontAttributes == FontAttributes.None)
                return null;

            var style = TypefaceStyle.Normal;

            if ((span.FontAttributes & FontAttributes.Bold) != 0 && (span.FontAttributes & FontAttributes.Italic) != 0)
            {
                style = TypefaceStyle.BoldItalic;
            }
            else if ((span.FontAttributes & FontAttributes.Bold) != 0)
            {
                style = TypefaceStyle.Bold;
            }
            else if ((span.FontAttributes & FontAttributes.Italic) != 0)
            {
                style = TypefaceStyle.Italic;
            }

            return new StyleSpan(style);
        }

        private void SetUnderline(bool underlined)
        {
            try
            {
                var textView = (TextView)Control;
                if (underlined)
                {
                    textView.PaintFlags |= PaintFlags.UnderlineText;
                }
                else
                {
                    textView.PaintFlags &= ~PaintFlags.UnderlineText;
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                Console.WriteLine("Cannot underline Label. Error: ", ex.Message);
            }
        }
    }

    public class CustomTypefaceSpan : TypefaceSpan
    {
        private readonly Typeface _newType;
        private readonly Android.Graphics.Color _fontColor;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomTypefaceSpan"/> class.
        /// </summary>
        /// <param name="family">The family.</param>
        /// <param name="type">The type.</param>
        /// <param name="color">The color.</param>
        public CustomTypefaceSpan(String family, Typeface type, Android.Graphics.Color color)
            : base(family)
        {

            _newType = type;
            _fontColor = color;
        }


        /// <summary>
        /// Handles the Update Draw State request
        /// </summary>
        /// <param name="ds">The ds.</param>
        public override void UpdateDrawState(TextPaint ds)
        {
            ApplyCustomTypeFace(ds, _newType, _fontColor);
        }


        /// <summary>
        /// Handles the update measure state request
        /// </summary>
        /// <param name="paint">The paint.</param>
        public override void UpdateMeasureState(TextPaint paint)
        {

            ApplyCustomTypeFace(paint, _newType, _fontColor);
        }

        /// <summary>
        /// Applies the custom type face.
        /// </summary>
        /// <param name="paint">The paint.</param>
        /// <param name="tf">The tf.</param>
        /// <param name="color">The color.</param>
        private static void ApplyCustomTypeFace(Paint paint, Typeface tf, Android.Graphics.Color color)
        {
            int oldStyle;
            Typeface old = paint.Typeface;
            if (old == null)
            {
                oldStyle = 0;
            }
            else
            {
                oldStyle = (int)old.Style;
            }

            int fake = oldStyle & ~(int)tf.Style;
            if ((fake & (int)TypefaceStyle.Bold) != 0)
            {
                paint.FakeBoldText = true;
            }

            if ((fake & (int)TypefaceStyle.Italic) != 0)
            {
                paint.TextSkewX = -0.25f;
            }


            paint.SetARGB(color.A, color.R, color.G, color.B);

            paint.SetTypeface(tf);
        }
    }
}