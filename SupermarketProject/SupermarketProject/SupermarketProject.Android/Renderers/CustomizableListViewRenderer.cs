using System;
using System.ComponentModel;
using Android.Util;
using Android.Widget;
using SupermarketProject.Common.CustomRenderers;
using SupermarketProject.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ListView = Xamarin.Forms.ListView;


[assembly: ExportRenderer(typeof(CustomizableListView), typeof(CustomizableListViewRenderer))]
namespace SupermarketProject.Droid.Renderers
{
	public class CustomizableListViewRenderer : ListViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);
			if (e.OldElement != null || Element == null || Control == null)
				return;
			Control.ScrollStateChanged += ControlOnScrollStateChanged;
			Control.Scroll += ControlOnScroll;
			
            var listView = this.Control as Android.Widget.ListView;


            if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop)
            {
                listView.NestedScrollingEnabled = true;
            }

			var listViewElement = Element as CustomizableListView;
			if (listViewElement == null) return;

			if (!listViewElement.IsSelectionEnabled)
			{
				Control.SetSelector(Android.Resource.Color.Transparent);
				Control.CacheColorHint = Color.Transparent.ToAndroid();
			}

			Control.Divider.Alpha = 0;
			Control.DividerHeight = listViewElement.RowSpacing;
		}

		private void ControlOnScroll(object sender, AbsListView.ScrollEventArgs scrollEventArgs)
		{
			var customizableListView = Element as CustomizableListView;
			if (customizableListView != null)
				customizableListView.YOffset = scrollEventArgs.View.GetChildAt(0) != null ? scrollEventArgs.View.GetChildAt(0).GetY() : 0;			
		}

		private void ControlOnScrollStateChanged(object sender, AbsListView.ScrollStateChangedEventArgs scrollStateChangedEventArgs)
		{
			
		}
	}
}