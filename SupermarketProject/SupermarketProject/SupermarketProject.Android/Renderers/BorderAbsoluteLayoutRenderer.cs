using System.ComponentModel;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Views;
using SupermarketProject.Common;
using SupermarketProject.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BorderAbsoluteLayout), typeof(BorderAbsoluteLayoutRenderer))]

namespace SupermarketProject.Droid.Renderers
{
	public class BorderAbsoluteLayoutRenderer : VisualElementRenderer<BorderAbsoluteLayout>
	{
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			//HandlePropertyChanged (sender, e);
			BorderAbsoluteLayoutRendererVisual.UpdateBackground(Element, this.ViewGroup);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<BorderAbsoluteLayout> e)
		{
			base.OnElementChanged(e);
			BorderAbsoluteLayoutRendererVisual.UpdateBackground(Element, this.ViewGroup);
		}

		/*void HandlePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Content")
			{
				BorderRendererVisual.UpdateBackground (Element, this.ViewGroup);
			}
		}*/

		protected override void DispatchDraw(Canvas canvas)
		{
			if (Element.IsClippedToBorder)
			{
				canvas.Save(SaveFlags.Clip);
				BorderAbsoluteLayoutRendererVisual.SetClipPath(this, canvas);
				base.DispatchDraw(canvas);
				canvas.Restore();
			}
			else
			{
				base.DispatchDraw(canvas);
			}
		}
	}
}
