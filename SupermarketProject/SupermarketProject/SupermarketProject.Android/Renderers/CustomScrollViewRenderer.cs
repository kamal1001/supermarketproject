﻿using System;
using System.ComponentModel;
using SupermarketProject.Common.CustomRenderers;
using SupermarketProject.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using static Java.Util.ResourceBundle;

[assembly: ExportRenderer(typeof(CustomScrollView), typeof(CustomScrollViewRenderer))]

namespace SupermarketProject.Droid
{
    public class CustomScrollViewRenderer :ScrollViewRenderer
    {

        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || this.Element == null)
                return;
            var element = Element as CustomScrollView;

            this.HorizontalScrollBarEnabled = !element.HideScrollBar;
            this.VerticalScrollBarEnabled = !element.HideScrollBar;
        }

        protected void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            var element = Element as CustomScrollView;

            this.HorizontalScrollBarEnabled = !element.HideScrollBar;
            this.VerticalScrollBarEnabled = !element.HideScrollBar;



        }
    }
}
