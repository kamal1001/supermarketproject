﻿using System;
using Xamarin.Forms.Platform.Android;
using System.ComponentModel;
using Xamarin.Forms;
using SupermarketProject.Droid;
using SupermarketProject;
using Android.Graphics;

[assembly: ExportRenderer(typeof(ColorProgressBar), typeof(ColorProgressBarRenderer))]
namespace SupermarketProject.Droid
{
	public class ColorProgressBarRenderer : ProgressBarRenderer
	{
		public ColorProgressBarRenderer()
		{ }

		protected override void OnElementChanged(ElementChangedEventArgs<ProgressBar> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement == null)
				return;

			if (Control != null)
			{
				UpdateBarColor();
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == ColorProgressBar.BarColorProperty.PropertyName)
			{
				UpdateBarColor();
			}
		}

		private void UpdateBarColor()
		{
			var element = Element as ColorProgressBar;
			// http://stackoverflow.com/a/29199280
			Control.IndeterminateDrawable.SetColorFilter(element.BarColor.ToAndroid(), PorterDuff.Mode.SrcIn);
			Control.ProgressDrawable.SetColorFilter(element.BarColor.ToAndroid(), PorterDuff.Mode.SrcIn);
		}
	}
}
