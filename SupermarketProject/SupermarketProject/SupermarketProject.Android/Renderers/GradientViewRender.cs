﻿
using SupermarketProject;
using SupermarketProject.Droid.Renderers;
using System;
using System.Linq;
using Android.App;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Android.Content;

[assembly: Xamarin.Forms.ExportRenderer(typeof(GradientView), typeof(GradientViewRender))]
namespace SupermarketProject.Droid.Renderers
{
    public class GradientViewRender : ViewRenderer<GradientView,View>
    {
        LinearLayout layout;
        Xamarin.Forms.Color[] gradientColors;
        float cornerRadius;
        float viewHeight;
        float viewWidth;
        bool roundCorners;
        GradientDirection direction;
        public GradientViewRender(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<GradientView> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                layout = new LinearLayout(Application.Context);
                layout.SetBackgroundColor(Color.Transparent);

                gradientColors = e.NewElement.GradientColors;
                cornerRadius = e.NewElement.CornerRadius;
                viewWidth = (float)e.NewElement.Width;
                viewHeight = (float)e.NewElement.Height;
                roundCorners = e.NewElement.RoundCorners;
                direction = e.NewElement.Direction;
                CreateLayout();
            }

            if (e.OldElement != null)
            {
                // Unsubscribe from event handlers and cleanup any resources
            }

            if (e.NewElement != null)
            {
                // Configure the control and subscribe to event handlers
                gradientColors = e.NewElement.GradientColors;
                cornerRadius = e.NewElement.CornerRadius;
                viewWidth = (float)e.NewElement.Width;
                viewHeight = (float)e.NewElement.Height;
                roundCorners = e.NewElement.RoundCorners;
                direction = e.NewElement.Direction;
                CreateLayout();
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == GradientView.HeightProperty.PropertyName)
            {
                this.viewHeight = (float)Element.Height;
                CreateLayout();
            }
            else if (e.PropertyName == GradientView.WidthProperty.PropertyName)
            {
                this.viewWidth = (float)Element.Width;
                CreateLayout();
            }
            else if (e.PropertyName == GradientView.GradientColorsProperty.PropertyName)
            {
                this.gradientColors = Element.GradientColors;
                CreateLayout();
            }
            else if (e.PropertyName == GradientView.CornerRadiusProperty.PropertyName)
            {
                this.cornerRadius = Element.CornerRadius;
                CreateLayout();
            }
            else if (e.PropertyName == GradientView.RoundCornersProperty.PropertyName)
            {
                this.roundCorners = Element.RoundCorners;
                CreateLayout();
            }else if (e.PropertyName == GradientView.DirectionProperty.PropertyName)
            {
                this.direction = Element.Direction;
                CreateLayout();
            }
        }

        private void CreateLayout()
        {
            layout.SetMinimumWidth((int)viewWidth);
            layout.SetMinimumHeight((int)viewHeight);

            CreateGradient();

            SetNativeControl(layout);
        }

        public void CreateGradient()
        {
            //Need to convert the colors to Android Color objects
            int[] androidColors = new int[gradientColors.Count()];

            for (int i = 0; i < gradientColors.Count(); i++)
            {
                Xamarin.Forms.Color temp = gradientColors[i];
                androidColors[i] = temp.ToAndroid();
            }

            GradientDrawable gradient; 
            switch (direction) 
            {
 
                case GradientDirection.LeftToRight:
 
                    gradient = new GradientDrawable(GradientDrawable.Orientation.LeftRight, androidColors);
 
                    break;
 
                case GradientDirection.RightToLeft:
 
                    gradient = new GradientDrawable(GradientDrawable.Orientation.RightLeft, androidColors);
 
                    break;
 
                case GradientDirection.TopToBottom:
 
                    gradient = new GradientDrawable(GradientDrawable.Orientation.TopBottom, androidColors);
 
                    break;
 
                default:
 
                    gradient = new GradientDrawable(GradientDrawable.Orientation.BottomTop, androidColors);
 
                    break; 
            } 
            if (roundCorners)
                gradient.SetCornerRadii(new float[] { cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius, cornerRadius });

            layout.SetBackground(gradient);
        }
    }
}
