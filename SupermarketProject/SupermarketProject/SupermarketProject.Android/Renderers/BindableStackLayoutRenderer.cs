﻿using System;
using SupermarketProject;
using SupermarketProject.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(BindableStackLayout), typeof(BindableStackLayoutRenderer))]

namespace SupermarketProject.Droid
{
    public class BindableStackLayoutRenderer : VisualElementRenderer<StackLayout>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<StackLayout> e)
        {
            base.OnElementChanged(e);

            var element = e.NewElement as BindableStackLayout;
            element?.Render();
        }
    }
}
