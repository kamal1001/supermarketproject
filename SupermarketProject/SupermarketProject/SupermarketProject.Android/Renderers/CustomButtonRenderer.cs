﻿using SupermarketProject.Common.CustomRenderers;
using SupermarketProject.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]

namespace SupermarketProject.Droid.Renderers
{
    public class CustomButtonRenderer:ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);

            Control?.SetAllCaps(false);
        }
    }
}