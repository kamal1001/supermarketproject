﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using SupermarketProject.Common.CustomRenderers;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]

namespace SupermarketProject.Droid.Renderers
{
    class CustomPickerRenderer : PickerRenderer
    {
        public CustomPickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var element = Element as CustomPicker;

                Control.TextSize = (float)element.FontSize;
                Control.SetTextColor(element.CustomTextColor.ToAndroid());
                Control.SetHintTextColor(element.HintTextColor.ToAndroid());
                //Control.Hint = element.Hint;
                Control.SetPadding((int)MyDevice.GetReversedScaledSize(element.LeftPadding), Control.PaddingTop, Control.PaddingRight, Control.PaddingBottom);
                Typeface font = Typeface.CreateFromAsset(Context.Assets, $"fonts/{element.FontType}.ttf");
                Control.Typeface = font;
                Control.SetBackgroundColor(Android.Graphics.Color.Transparent);

            }
        }

    }
}