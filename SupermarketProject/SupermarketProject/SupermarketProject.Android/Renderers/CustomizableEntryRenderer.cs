﻿using System;
using System.ComponentModel;
using Android.Graphics;
using Android.Util;
using Android.Views;
using SupermarketProject.Common;
using SupermarketProject.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomizableEntry), typeof(CustomizableEntryRenderer))]
namespace SupermarketProject.Droid.Renderers
{
	public class CustomizableEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);
			if (e.OldElement != null || Element == null)
				return;
			var entryElement = Element as CustomizableEntry;
			if (entryElement == null) return;
			Control.SetPadding(entryElement.LeftPadding, Control.PaddingTop, Control.PaddingRight, Control.PaddingBottom);
			Control.Background.SetAlpha(entryElement.UnderlineEnabled ? 255 : 0);
			if (entryElement.ShouldCenterVertically)
				Control.Gravity = GravityFlags.CenterVertical;

			Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, $"fonts/{entryElement.FontType}.ttf");

			/*if (entryElement.FormattedText != null)
			{
				HandleFormattedText(entryElement, font);
				return;
			}*/

			Control.Typeface = font;
		}
	}
}