﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using Android.Util;
using Android.Views;
using SupermarketProject.Common;
using SupermarketProject.Common.CustomRenderers;
using SupermarketProject.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace SupermarketProject.Droid.Renderers
{
    public class CustomEditorRenderer : EditorRenderer
    {
        public CustomEditorRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            if (Element == null)
                return;

            var element = (CustomEditor)Element;

            Control.Hint = element.Placeholder;
            Control.SetHintTextColor(element.PlaceholderColor.ToAndroid());
            Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
            Typeface font = Typeface.CreateFromAsset(Forms.Context.Assets, $"fonts/{element.FontType}.ttf");

            Control.Typeface = font;
        }
    }
}
