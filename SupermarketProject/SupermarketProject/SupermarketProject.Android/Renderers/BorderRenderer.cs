using System.ComponentModel;
using Android.Graphics;
using Android.Views;
using SupermarketProject.Common;
using SupermarketProject.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Border), typeof(BorderRenderer))]

namespace SupermarketProject.Droid.Renderers
{
	public class BorderRenderer : VisualElementRenderer<Border>
	{
		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			//HandlePropertyChanged (sender, e);
			BorderRendererVisual.UpdateBackground(Element, this.ViewGroup);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Border> e)
		{
			base.OnElementChanged(e);
			BorderRendererVisual.UpdateBackground(Element, this.ViewGroup);
		}		

		/*void HandlePropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Content")
			{
				BorderRendererVisual.UpdateBackground (Element, this.ViewGroup);
			}
		}*/

		protected override void DispatchDraw(Canvas canvas)
		{
			if (Element.IsClippedToBorder)
			{
				canvas.Save(SaveFlags.Clip);
				BorderRendererVisual.SetClipPath(this, canvas);
				base.DispatchDraw(canvas);
				canvas.Restore();
			}
			else
			{
				base.DispatchDraw(canvas);
			}
		}
	}
}
