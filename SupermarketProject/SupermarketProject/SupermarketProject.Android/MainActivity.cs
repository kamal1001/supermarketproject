﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.OS;
using Android.Util;
using Android.Content.Res;
using SupermarketProject.Common.Utilities;
using TwinTechs.Droid.Controls;
using System.Threading;
using Plugin.Permissions;
using System.Net;
using Xamarin.Forms;
using FFImageLoading.Forms;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter;
using Firebase.Analytics;
using Bindings.CrashlyticsKit;
using Bindings.FabricSdk;
using Acr.UserDialogs;

namespace SupermarketProject.Droid
{
    [Activity(Label = "SupermarketProject", Icon = "@drawable/icon1", ConfigurationChanges = ConfigChanges.KeyboardHidden | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait, LaunchMode = LaunchMode.SingleTask)]
    public class MainActivity : Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        public static MainActivity CurrentActivity { get; private set; }
        public static FirebaseAnalytics firebaseAnalytics;

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            CurrentActivity = this;
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            Rg.Plugins.Popup.Popup.Init(this, bundle);
            adjustFontScale(Resources.Configuration);
            FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/Arial-Rounded-MT-Bold.ttf");
            global::Xamarin.Forms.Forms.SetFlags("FastRenderers_Experimental");
            global::Xamarin.Forms.Forms.Init(this, bundle);
            firebaseAnalytics = FirebaseAnalytics.GetInstance(this);
            Fabric.With(this, new Crashlytics());
            AppCenter.Start("1c4215af-0d32-421a-a546-3c8dd2c61bd6", typeof(Microsoft.AppCenter.Analytics.Analytics), typeof(Crashes));
            UserDialogs.Init(this);
            CachedImage.FixedOnMeasureBehavior = true;
            CachedImage.FixedAndroidMotionEventHandler = true;

            var config = new FFImageLoading.Config.Configuration
            {
                FadeAnimationForCachedImages = false,
                FadeAnimationEnabled = false
            };
            FFImageLoading.ImageService.Instance.Initialize(config);
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(enableFastRenderer: true);
            App.ScreenWidth = Resources.DisplayMetrics.WidthPixels / Resources.DisplayMetrics.Density; // real pixels
            App.ScreenHeight = (int)(Resources.DisplayMetrics.HeightPixels / Resources.DisplayMetrics.Density) - 20;

            DependencyService.Get<INativeMethods>().ChangeStatusBarColor(MyDevice.StatusBarColor);

            SQLitePCL.Batteries.Init();
            var result = SQLitePCL.raw.sqlite3_config(SQLitePCL.raw.SQLITE_CONFIG_SERIALIZED);
            if (result != SQLitePCL.raw.SQLITE_OK)
            {
                System.Diagnostics.Debug.WriteLine($"Unable to set SQLIte in Serialized mode (error code = {result})");
            }

            Plugin.LocalNotifications.LocalNotificationsImplementation.NotificationIconId = Resource.Drawable.ic_stat_onesignal_default;

            LoadApplication(new App());

            MyDevice.fastCellCache = FastCellCache.Instance;
            MyDevice.fastGridCellCache = FastGridCellCache.Instance;
        }
        public void adjustFontScale(Configuration configuration)
        {
            configuration.FontScale = 1f;
            DisplayMetrics metrics = Resources.DisplayMetrics;
            BaseContext.GetSystemService(Context.WindowService).JavaCast<IWindowManager>().DefaultDisplay.GetMetrics(metrics);
            metrics.ScaledDensity = configuration.FontScale * metrics.Density;
            BaseContext.Resources.UpdateConfiguration(configuration, metrics);
        }
    }
}