﻿using System;
using System.Collections.Generic;
using Android.OS;
using Java.Math;
using Bindings.AnswersKit;
using Xamarin.Forms;
using SupermarketProject.Droid;
using SupermarketProject.Analytics;

[assembly: Dependency(typeof(AnalyticsLog))]

namespace SupermarketProject.Droid
{
    class AnalyticsLog : IAnalytics
    {
        //Firebase Events
        public void LogFireBase(string key, Dictionary<string, object> LogData)
        {
            var bundle = new Bundle();
            foreach (var item in LogData) bundle.PutString(item.Key, item.Value as string);
            MainActivity.firebaseAnalytics.LogEvent(key, bundle);
        }

        //Fabric Events
        public void LogFabricPurchase(Decimal Price, string Currency, string ItemName, string ItemType, string ItemId, bool Success)
        {
            Answers.Instance.LogPurchase(new PurchaseEvent().PutItemPrice(BigDecimal.ValueOf((double)Price)).PutCurrency(Java.Util.Currency.GetInstance(Currency)).PutItemName(ItemName).PutItemType(ItemType).PutItemId(ItemId).PutSuccess(Success));
        }
        public void LogFabricAddCart(Decimal Price, string Currency, string ItemName, string ItemType, string ItemId)
        {
            Answers.Instance.LogAddToCart(new AddToCartEvent().PutItemPrice(BigDecimal.ValueOf((double)Price)).PutCurrency(Java.Util.Currency.GetInstance(Currency)).PutItemName(ItemName).PutItemType(ItemType).PutItemId(ItemId));
        }
        public void LogFabricStartCheckout(Decimal Price, string Currency, int ItemCount)
        {
            Answers.Instance.LogStartCheckout(new StartCheckoutEvent().PutTotalPrice(BigDecimal.ValueOf((double)Price)).PutCurrency(Java.Util.Currency.GetInstance(Currency)).PutItemCount(ItemCount));
        }
        public void LogFabricContentView(string ContentName, string ContentType, string ContentId)
        {
            Answers.Instance.LogContentView(new ContentViewEvent().PutContentName(ContentName).PutContentType(ContentType).PutContentId(ContentId));
        }
        public void LogFabricSearch(string Query)
        {
            Answers.Instance.LogSearch(new SearchEvent().PutQuery(Query));
        }
        public void LogFabricReatedContent(int Rating, string ContentName, string ContentType, string ContentId)
        {
            Answers.Instance.LogRating(new RatingEvent().PutRating(Rating).PutContentName(ContentName).PutContentType(ContentType).PutContentId(ContentId));
        }
        public void LogFabricSignUp(string Method, bool Success)
        {
            Answers.Instance.LogSignUp(new SignUpEvent().PutMethod(Method).PutSuccess(Success));
        }
        public void LogFabricLogin(string Method, bool Success)
        {
            Answers.Instance.LogLogin(new LoginEvent().PutMethod(Method).PutSuccess(Success));
        }
    }
}
