﻿using System;
using SupermarketProject.Droid;
using Java.Lang;
using Java.IO;
using Java.Net;
using Android.Net;
using System.Threading.Tasks;
using Android.Util;
using System.Linq;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using SupermarketProject.Common.Utilities;
using Plugin.CurrentActivity;
using Xamarin.Forms;
using Application = Android.App.Application;
using Android.App;
using Android.Views.InputMethods;
using Microsoft.AppCenter.Crashes;

[assembly: Xamarin.Forms.Dependency(typeof(NativeMethods))]
namespace SupermarketProject.Droid
{
    public class NativeMethods : INativeMethods
    {
        public bool IsKeyboardHidden()
        {
            return !((InputMethodManager)MainActivity.CurrentActivity.GetSystemService(Context.InputMethodService)).IsAcceptingText;
        }
        public void CloseApp()
        {
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }
        public bool FileExist(string image)
        {
            return true;
        }
        public string GetImageDownloadPath()
        {
            return Application.Context.GetExternalFilesDir(Android.OS.Environment.DataDirectory.AbsolutePath).AbsolutePath;
        }
        public void Logg(string logg)
        {
            Log.Warn("1001App", logg);
        }
        public string GetFileNameFromURL(string url)
        {
            return Android.Net.Uri.Parse(url).Path.Split('/').Last();
        }
        public void CloseKeyboard()
        {
            var context = Forms.Context;
            var inputMethodManager = context.GetSystemService(Context.InputMethodService) as InputMethodManager;
            if (inputMethodManager != null && context is Activity)
            {
                var activity = context as Activity;
                var token = activity.CurrentFocus?.WindowToken;
                inputMethodManager.HideSoftInputFromWindow(token, HideSoftInputFlags.None);

                activity.Window.DecorView.ClearFocus();
            }
        }
        public void DeleteCache()
        {

            try
            {
                var cachePath = System.IO.Path.GetTempPath();

                // If exist, delete the cache directory and everything in it recursivly
                if (System.IO.Directory.Exists(cachePath))
                    System.IO.Directory.Delete(cachePath, true);

                // If not exist, restore just the directory that was deleted
                if (!System.IO.Directory.Exists(cachePath))
                    System.IO.Directory.CreateDirectory(cachePath);

                var dataPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);


                // If exist, delete the cache directory and everything in it recursivly
                if (System.IO.Directory.Exists(dataPath))
                    System.IO.Directory.Delete(dataPath, true);

                // If not exist, restore just the directory that was deleted
                if (!System.IO.Directory.Exists(dataPath))
                    System.IO.Directory.CreateDirectory(dataPath);
            }
            catch (System.Exception e)
            {
                Crashes.TrackError(e);
                System.Console.WriteLine(e.ToString());
                var b = e;
            }
        }
        public bool DeleteDir(File dir)
        {
            if (dir != null && dir.IsDirectory)
            {
                string[] children = dir.List();
                for (int i = 0; i < children.Length; i++)
                {
                    bool success = DeleteDir(new File(dir, children[i]));
                    if (!success)
                    {
                        return false;
                    }
                }
                return dir.Delete();
            }
            else if (dir != null && dir.IsFile)
            {
                return dir.Delete();
            }
            else
            {
                return false;
            }
        }
        public void ChangeStatusBarColor(Color color)
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop && color != null)
            {
                Android.Graphics.Color c = new Android.Graphics.Color((int)(color.R * 255), (int)(color.G * 255), (int)(color.B * 255));
                CrossCurrentActivity.Current.Activity?.Window?.ClearFlags(WindowManagerFlags.TranslucentStatus);
                CrossCurrentActivity.Current.Activity?.Window?.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
                CrossCurrentActivity.Current.Activity?.Window?.SetStatusBarColor(c);
            }
        }
        public string GetTimeURL()
        {
            return "http://api.timezonedb.com/?zone=Asia%2FDubai&format=json&key=3KQP2BBMXURP";
        }

        public string GetVersionCode()
        {
            return Application.Context.PackageManager.GetPackageInfo(Application.Context.PackageName, 0).VersionCode.ToString();
        }

        public void share(string title, string content)
        {
            title = "Shopping App Recommendation";
            Intent intent = new Intent(Intent.ActionSend);
            intent.PutExtra(Intent.ExtraText, content);
            intent.SetType("text/plain");
            Forms.Context.StartActivity(Intent.CreateChooser(intent, title));
        }

        public void RateUs()
        {
        }
    }
}