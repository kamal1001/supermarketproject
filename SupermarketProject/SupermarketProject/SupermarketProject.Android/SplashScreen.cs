﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Content;
using Acr.UserDialogs;

namespace SupermarketProject.Droid
{
    [Activity(Label = "SupermarketProject", MainLauncher = true, NoHistory = true, Theme = "@style/Theme.Splash",
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class SplashScreen : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            UserDialogs.Init(this);
            StartActivity(new Intent(this, typeof(MainActivity)));
        }
    }
}