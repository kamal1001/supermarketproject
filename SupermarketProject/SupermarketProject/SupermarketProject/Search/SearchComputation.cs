﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using SupermarketProject.Models.Remote;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject
{
    static class SearchComputation
    {
        //The GetSuggestedWord function is used to get the closest match
        //That value represent the nearest word to the entry
        //Might return same value as entry
        public static string GetSuggestedWord(string inputWord, List<string> wordList)
        {
            float lowestWeight = Int16.MaxValue;
            string closestMatch = inputWord;
            foreach (string word in wordList)
            {
                float distanceValue = LevenshteinDistance.Compute(inputWord, word, true, true);
                if (distanceValue == 0)//we found the exact word
                    return word;

                //We shorten discrepancy for more accurate results
                int refacturedWordLength = inputWord.Replace(" ", string.Empty).Length;
                float shortenDiscrepancyStep1 = (float)Math.Max(refacturedWordLength, word.Length) /
                    Math.Min(refacturedWordLength, word.Length);
                float shortenDiscrepancyStep2 = Math.Max(distanceValue, shortenDiscrepancyStep1) /
                    Math.Min(distanceValue, shortenDiscrepancyStep1);

                if (lowestWeight > shortenDiscrepancyStep2)
                {
                    lowestWeight = shortenDiscrepancyStep2;
                    closestMatch = word;
                }
            }
            return closestMatch;
        }

        //The GetSuggestedWords function is used to get a list of the closest matches
        //That value represent the nearest words to the entry with their score sorted in ascending order
        //Might return same value as entry which would be placed at the begining of the list with a score of 0
        public static List<KeyValuePair<string, float>> GetSuggestedWords(string inputWord, List<string> wordList, int count = 1)
        {
            Dictionary<string, float> WordRanking = new Dictionary<string, float>();
            foreach (string word in wordList)
            {
                float distanceValue = LevenshteinDistance.Compute(inputWord, word, true, true);
                if (distanceValue == 0)//we found the exact word
                {
                    try
                    {
                        WordRanking.Add(word, distanceValue);
                    }
                    catch (Exception e) {
                        Crashes.TrackError(e);
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                    }
                    continue;
                }
                try
                {
                    bool contains = Regex.IsMatch(word.ToLower(), string.Format(@"\b{0}\b", Regex.Escape(inputWord.ToLower())));
                    Match m = Regex.Match(word.ToLower(), string.Format(@"\b{0}\b", Regex.Escape(inputWord.ToLower())));
                    //if (word.ToLower().IndexOf(inputWord.ToLower(), StringComparison.Ordinal) == 0 && contains)
                    //WordRanking.Add(word, distanceValue);
                    var product = ProductModel.ProductItem(word);
                    var category = CategoryModel.CategoryDictionary?.Where(x => x.Key == product.RealCategoryID).First();

                    if (category.Value.Value.Name.ToLower().ContainsString(inputWord, StringComparison.OrdinalIgnoreCase) == true)
                    {
                        WordRanking.Add(word, distanceValue);
                    }
                    else if (m.Success && m.Index == 0)
                    {
                        WordRanking.Add(word, distanceValue + 100);
                    }

                    else if (contains) 
                    { 
                        if (!WordRanking.ContainsKey(word)) 
                        { 
                            WordRanking.Add(word, distanceValue + 500); 
                        } 
                    }else{ 
                        if (!WordRanking.ContainsKey(word)) 
                        { 
                            WordRanking.Add(word, distanceValue + 1000); 
                        } 
                    } 

                }
                catch (Exception e) {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                }
                /*
                //We shorten discrepancy for more accurate results
                int refacturedWordLength = inputWord.Replace(" ", string.Empty).Length;
                float shortenDiscrepancyStep1 = (float)Math.Max(refacturedWordLength, word.Length) /
                    Math.Min(refacturedWordLength, word.Length);
                float shortenDiscrepancyStep2 = Math.Max(distanceValue, shortenDiscrepancyStep1) /
                    Math.Min(distanceValue, shortenDiscrepancyStep1);

                try
                {
                    WordRanking.Add(word, shortenDiscrepancyStep2);
                }
                catch (Exception e) { }*/
            }
            var SortedList = (from Entry in WordRanking
                              orderby Entry.Value
                              ascending
                              select Entry
                             ).Take(count).ToList();

            return SortedList;
        }
    }
}
