﻿using System;
using Xamarin.Forms;
using SupermarketProject.Common.ViewCells;
using XLabs.Forms.Controls;
using SupermarketProject.Models.Local;
using SupermarketProject.Common.Utilities;
using System.Collections.ObjectModel;
using SupermarketProject.Common.Objects;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;
using Com.OneSignal;
using Com.OneSignal.Abstractions;
using Rg.Plugins.Popup.Services;
using SQLite;
using TwinTechs.Controls;
using Xamarin.Forms.Xaml;
using Parse;
using SupermarketProject.Models.Remote;

namespace SupermarketProject
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class App : Application
    {
        public static double ScreenWidth;
        public static double ScreenHeight;
        public static bool IsIPhoneXOrLatest;

        public App()
        {
            InitializeComponent();
            MyDevice.Initialize();


            if (Device.OS == TargetPlatform.iOS)
            {
                App.Current.Resources = new ResourceDictionary();

                Style labelStyle = new Style(typeof(Label))
                {
                    Setters = {
                        new Setter { Property = Label.FontFamilyProperty, Value = "Arial Rounded MT Bold"  }
                    }
                };
                Style entryStyle = new Style(typeof(Entry))
                {
                    Setters = {
                        new Setter { Property = Entry.FontFamilyProperty, Value = "Arial Rounded MT Bold"  }
                    }
                };
                Style extendedStyle = new Style(typeof(ExtendedButton))
                {
                    Setters = {
                        new Setter { Property = ExtendedButton.FontFamilyProperty, Value = "Arial Rounded MT Bold"  }
                    }
                };
                App.Current.Resources.Add(labelStyle);
                App.Current.Resources.Add(entryStyle);
                App.Current.Resources.Add(extendedStyle);
            }

            MainPage = new LoadingPage();

#if Promotion
            PromotionDetailProductCell.PromotionDetailProductCellQueue = new Queue<PromotionDetailProductCell>();
            PromotionCell.PromotionPageCellQueue = new Queue<PromotionCell>();
#endif

            CartCellNew.LastSixCell = new Queue<CartCellNew>();
            ProductCellNew.LastSixCell = new Queue<ProductCellNew>();
            CategoryCellNew.LastSixCell = new Queue<CategoryCellNew>();
            StoreInfoBranchCell.StoreInfoCellQueue = new Queue<StoreInfoBranchCell>();

            Cart.ProductsInCart = new ObservableCollection<Product>();
#if Laundry
            Cart.CartLaundries = new ObservableCollection<LaundryClass>();
#endif
#if Stamp
            Cart.CartStamps = new ObservableCollection<StampRewardClass>();
#endif

            Assembly assembly = typeof(App).GetTypeInfo().Assembly;
            var fullImageNameList = assembly.GetManifestResourceNames().Where(name => name.Contains(ParseConstants.IMAGE_FOLDER_NAME)).ToList();
            string projectName = assembly.GetName().Name;
            MyDevice.EmbeddedResourceImageDirectoryPath = string.Concat(projectName, ".", ParseConstants.IMAGE_FOLDER_NAME, ".");
            foreach (var fullImageName in fullImageNameList)
            {
                MyDevice.EmbeddedResourceImageNameStrings.Add(fullImageName.Substring(MyDevice.EmbeddedResourceImageDirectoryPath.Length));
            }

            OneSignal.Current.StartInit("001fb351-82f2-44cc-b435-2a1c4d48628a").InFocusDisplaying(OSInFocusDisplayOption.None).HandleNotificationReceived(NotificationReceived).EndInit();
        }

        private static async void NotificationReceived(OSNotification notification)
        {
            OSNotificationPayload payload = notification.payload;

            //get subject
            if (payload.additionalData.ContainsKey("subject") && payload.additionalData["subject"].ToString() == "@service_order_ready")
            {
                Device.BeginInvokeOnMainThread(async () => await Current.MainPage.DisplayAlert("Laundry", "Your laundry order is ready to drop off!", "OK"));
                return;
            }

            if (payload.additionalData.ContainsKey("subject") && (payload.additionalData["subject"].ToString() == "@status_changed_to_delivered"))
            {
                //todo check if user has met maximum feedback prompts threshold
                //show dialog if it is false else dont show dialog

                if (ParseUser.CurrentUser != null && MyDevice.ShouldLogFeedbackPrompt())
                {
                    Device.BeginInvokeOnMainThread(() => MyDevice.mMenuLayout.OpenFeedbackForm(true));
                }
                return;
            }

            //unhandled notification
            if (Current.MainPage != null)
                Device.BeginInvokeOnMainThread(async () => await Current.MainPage.DisplayAlert(payload.title, payload.body, "OK"));
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
