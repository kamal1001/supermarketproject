﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using Parse;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;

namespace SupermarketProject.Models.Remote
{
    public static class ProductBranchInfoModel
    {
        public static Dictionary<string, ProductBranchInfoClass> ProductBranchInfoDictionary;

        static ProductBranchInfoModel()
        {
            ProductBranchInfoDictionary = new Dictionary<string, ProductBranchInfoClass>();
        }

        public static async Task FetchAllProductBranchInfos()
        {
            var query = ParseObject.GetQuery(ParseConstants.PRODUCTBRANCHINFOS_CLASS_NAME);

            var allObjects = await FetchProductBranchInfos(query);

            AddFetchedObjectsToLocal(allObjects);
        }

        public static async Task FetchProductBranchInfosByCategory(string parentCategoryId)
        {
            int currentBranchIndex = UserClassFunctions.GetUser().ActiveBranch;
            DateTime? localUpdate = ProductBranchInfoDictionary.Values.Where(x => x.CategoryId == parentCategoryId).OrderByDescending(x => x.UpdatedAt).FirstOrDefault()?.UpdatedAt;
            if (localUpdate == null)
                localUpdate = DateTime.MinValue;

            var query = ParseObject.GetQuery(ParseConstants.PRODUCTBRANCHINFOS_CLASS_NAME).WhereEqualTo(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_BRANCHINDEX, currentBranchIndex).WhereEqualTo(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_CATEGORYID, parentCategoryId).
                WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate);

            var allObjects = await FetchProductBranchInfos(query);
            AddFetchedObjectsToLocal(allObjects);


        }

        public static async Task FetchProductBranchInfosByProductList(List<string> idList)
        {
            if (idList == null || !idList.Any())
                return;

            int currentBranchIndex = UserClassFunctions.GetUser().ActiveBranch;

            var query = ParseObject.GetQuery(ParseConstants.PRODUCTBRANCHINFOS_CLASS_NAME).WhereEqualTo(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_BRANCHINDEX, currentBranchIndex).WhereContainedIn(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_PRODUCTID, idList);

            var allObjects = await FetchProductBranchInfos(query);

            AddFetchedObjectsToLocal(allObjects, false);
        }

        private static async Task<List<ParseObject>> FetchProductBranchInfos(ParseQuery<ParseObject> productBranchInfoQuery)
        {
            int queryLimit = 1000;

            List<ParseObject> allObjects = new List<ParseObject>();

            if (CrossConnectivity.Current.IsConnected)
            {

                int i = 0;

                while (true)
                {
                    System.Diagnostics.Debug.WriteLine("before: " + allObjects.Count);
                    IEnumerable<ParseObject> objects = null;
                    for (int t = 0; t < 3; t++)
                    {
                        System.Diagnostics.Debug.WriteLine("Inner: " + allObjects.Count);
                        int cancelTime = 15000;
                        try
                        {
                            System.Diagnostics.Debug.WriteLine("Start of Try: " + allObjects.Count);

                            System.Threading.CancellationTokenSource cancellation =
                                new System.Threading.CancellationTokenSource(cancelTime);
                            objects = await productBranchInfoQuery.Limit(queryLimit).Skip(i).FindAsync(cancellation.Token);

                            System.Diagnostics.Debug.WriteLine("End of Try: " + allObjects.Count);
                            break;
                        }
                        catch (Exception e)
                        {
                            System.Diagnostics.Debug.WriteLine(e.ToString());
                            Crashes.TrackError(e);
                            string message = MyDevice.InternetErrorMessage1;
                            Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message)
                                .SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White)
                                .SetDuration(cancelTime));
                            System.Diagnostics.Debug.WriteLine("Toast from: FetchProducts " + e.Message);
                        }
                    }
                    if (objects == null)
                        break;

                    var collection = objects as ParseObject[] ?? objects.ToArray();

                    allObjects.AddRange(collection);

                    System.Diagnostics.Debug.WriteLine("After: " + allObjects.Count);
                    if (collection.Count() < queryLimit)
                        break;

                    i += queryLimit;
                }
            }

            return allObjects;
        }

        public static void PopulateProductBranchInfoDictionary(List<ProductBranchInfoClass> syncedProductBranchInfos = null)
        {
            bool addNewProducts = syncedProductBranchInfos != null;
            int currentBranchIndex = UserClassFunctions.GetUser().ActiveBranch;


            List<ProductBranchInfoClass> productBranchInfos = new List<ProductBranchInfoClass>();

            if (!addNewProducts)
            {
                ProductBranchInfoDictionary.Clear();
                productBranchInfos = ProductBranchInfoClassFunctions.GetProductBranchInfosByBranch(currentBranchIndex);
            }
            else
                productBranchInfos = syncedProductBranchInfos.Where(x => x.BranchIndex == currentBranchIndex && x.IsDeleted == false).ToList();

            if (productBranchInfos.Count == 0)
                return;

            foreach (ProductBranchInfoClass productBranchInfoClass in productBranchInfos)
            {
                if (addNewProducts)
                {
                    if (ProductBranchInfoDictionary.ContainsKey(productBranchInfoClass.ProductId))
                        ProductBranchInfoDictionary[productBranchInfoClass.ProductId] = productBranchInfoClass;

                    else
                        ProductBranchInfoDictionary.Add(productBranchInfoClass.ProductId, productBranchInfoClass);
                }
                else
                {
                    //todo multiple product check can be removed when the data is fixed
                    if (ProductBranchInfoDictionary.ContainsKey(productBranchInfoClass.ProductId))
                        ProductBranchInfoDictionary[productBranchInfoClass.ProductId] = productBranchInfoClass;

                    else
                        ProductBranchInfoDictionary.Add(productBranchInfoClass.ProductId, productBranchInfoClass);
                }
            }
        }

        private static void AddFetchedObjectsToLocal(List<ParseObject> objects, bool updateDate = true)
        {
            if (objects == null || !objects.Any())
                return;

            List<ProductBranchInfoClass> tempList = new List<ProductBranchInfoClass>();

            foreach (var productBranchInfoObject in objects)
            {
                ProductBranchInfoClass productBranchInfoClass = new ProductBranchInfoClass();

                productBranchInfoClass.objectId = productBranchInfoObject.ObjectId;
                productBranchInfoClass.CategoryId = productBranchInfoObject.Get<string>(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_CATEGORYID);
                productBranchInfoClass.ProductId = productBranchInfoObject.Get<string>(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_PRODUCTID);
                productBranchInfoClass.BranchIndex = productBranchInfoObject.Get<int>(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_BRANCHINDEX);

                productBranchInfoClass.IsInStock = productBranchInfoObject.Get<bool>(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_ISINSTOCK);
                productBranchInfoClass.IsInStore = productBranchInfoObject.Get<bool>(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_ISINSTORE);
                productBranchInfoClass.Price = (decimal)productBranchInfoObject.Get<double>(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_PRICE);

                productBranchInfoClass.IsDeleted = productBranchInfoObject.ContainsKey(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_ISDELETED) && productBranchInfoObject.Get<bool>(ParseConstants.PRODUCTBRANCHINFOS_ATTRIBUTE_ISDELETED);
                productBranchInfoClass.UpdatedAt = updateDate ? productBranchInfoObject.UpdatedAt : DateTime.MinValue;

                tempList.Add(productBranchInfoClass);
            }

            if (tempList.Count > 0)
            {
                ProductBranchInfoClassFunctions.AddProductBranchInfos(tempList);
                PopulateProductBranchInfoDictionary(tempList);
            }
        }

    }
}
