﻿using System;
using System.Collections.Generic;
using Parse;
using SupermarketProject.Common.Utilities;
using System.Threading.Tasks;
using SupermarketProject.Common.Objects;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject
{
    public static class SettingsModel
    {
        public static List<ActiveHours> ActiveHoursList = new List<ActiveHours>();

        public static string StampPin = string.Empty;

        public static async Task<int> GetVersionFromRemote()
        {
            if (!await MyDevice.GetNetworkStatus())
            {
                return -1;
            }
            int result = -1;
            for (int i = 0; i < 3; i++)
            {
                int cancelTime = 5000;
                try
                {
                    System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                    ParseQuery<ParseObject> query = ParseObject.GetQuery(ParseConstants.SETTINGS_CLASS_NAME);
                    ParseObject parseObject = await query.FirstAsync(cancellation.Token);

                    var activeHourString = parseObject.Get<string>(ParseConstants.SETTINGS_ACTIVEHOURS);
                    MyDevice.FeedbackPromptsCount = (int)parseObject.Get<double>(ParseConstants.SETTINGS_NUM_OF_FEEDBACK_PROMPTS_COUNT);
                    MyDevice.FeedbackPromptsOffset= (int)parseObject.Get<double>(ParseConstants.SETTINGS_FEEDBACK_PROMPTS_OFFSET);

                    ActiveHoursList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ActiveHours>>(activeHourString);
                    ActiveHoursList.Sort((activehour1, activehour2) => activehour1.DayofWeek.CompareTo(activehour2.DayofWeek));

                    result = parseObject.Get<int>(ParseConstants.SETTINGS_VERSION);

                    bool isMaintenance = parseObject.Get<bool>(ParseConstants.SETTINGS_MAINTENANCE);
                    #if Stamp
                    StampPin = parseObject.Get<string>(ParseConstants.SETTINGS_STAMPPIN);
#endif
                    if (isMaintenance)
                        result = -2;
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                    System.Diagnostics.Debug.WriteLine("Toast from: GetVersionFromRemote " + e.Message);
                }
            }
            return result;
        }
    }
}

