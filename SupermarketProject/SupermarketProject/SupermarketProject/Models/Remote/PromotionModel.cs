﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using SupermarketProject.Common.Objects;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using Microsoft.AppCenter.Crashes;
using Parse;
using Plugin.Connectivity;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace SupermarketProject.Models.Remote
{
    [Preserve(AllMembers = true)]
    public class PromotionEffect
    {
        public PromotionClass promotion;
        public decimal discount;
        public decimal newPrice;
        public string requiredProductId;
        public string discountedProductId;
        public int requiredProductQuantity;
        public int discountedProductQuantity;

        public PromotionEffect(PromotionClass promotion, decimal discount, decimal newPrice, string requiredProductId = null, string discountedProductId = null, int requiredProductQuantity = 0, int discountedProductQuantity = 0)
        {
            this.promotion = promotion;
            this.discount = discount;
            this.newPrice = newPrice;
            this.requiredProductId = requiredProductId;
            this.discountedProductId = discountedProductId;
            this.requiredProductQuantity = requiredProductQuantity;
            this.discountedProductQuantity = discountedProductQuantity;
        }
    }

    public class PromotionVariables
    {
        public string ProductID;
        public string NewPrice;
        public string OutProductID;
        public string OutNewValue;
    }

    public static class PromotionModel
    {
        // Type 1 => Categories %discount    InCategories    SaleValue
        // Type 2 => Products %discount   InProducts   SaleValue
        // Type 3 => Products Fixed price discount   InProducts   SaleValue
        // Type 4 => Products new Price InProducts   SaleValue
        // Type 5 => Products new Price product by product  InProducts  SaleValue
        // Type 6 => Categories fixed price discount    InCategories    SaleValue
        // Type 7 => Category without discount
        // Type 8 => Products without discount
        // Type 9 => Relational free product (ie buy one get one free)
        public static Dictionary<string, PromotionEffect> productPromotionDictionary =
            new Dictionary<string, PromotionEffect>();

        public static Dictionary<string, PromotionEffect> categoryPromotionDictionary =
            new Dictionary<string, PromotionEffect>();

        private static bool _fetchSucceeded = true;

        private static void ClearContainers()
        {
            MyDevice.PromotionList.Clear();
            productPromotionDictionary.Clear();
            categoryPromotionDictionary.Clear();
        }

        public static void RemoveRelatedPromotion(Promotion promotion)
        {
            if (promotion.ProductList != null)
            {
                foreach (Product product in promotion.ProductList)
                {
                    Product cartProduct = Cart.ProductsInCart.FirstOrDefault((arg) => arg.ProductID == product.ProductID);
                    if (cartProduct != null)
                        cartProduct.IsInPromotion = false;

                    if (productPromotionDictionary.ContainsKey(product.ProductID))
                        productPromotionDictionary.Remove(product.ProductID);
                }
            }

            MyDevice.PromotionList.Remove(promotion);
        }

        public static void PopulatePromotionDictionaries()
        {
            if (!_fetchSucceeded)
                return;

            ClearContainers();
            int store = UserClassFunctions.GetActiveBranchFromUser();
            Product product;
            List<string> promotionVariableList;
            string productId;
            string[] promotionVariableArray;
            List<Promotion> newPromotionList = new List<Promotion>();
            foreach (PromotionClass promotion in PromotionClassFunctions.GetPromotions())
            {
                if (promotion.Branch != "N/A")
                {
                    var branchList = promotion.Branch.Split(',').Select(int.Parse).ToArray();
                    if (!branchList.Contains(store))
                        continue;
                }

                Promotion tempPromotion = new Promotion
                {
                    PromotionId = promotion.objectId,
                    Description = promotion.Description,
                    EndDate = promotion.EndDate,
                    ImageId = promotion.ImageId,
                    StartDate = promotion.StartDate,
                    Subdescription = promotion.SubDescription,
                    Subtitle = promotion.SubTitle,
                    Title = promotion.Title,
                    DiscountPercentage = promotion.SaleValue,
                    ProductList = new List<Product>()

                };

                //care about the order
                switch (promotion.PromotionType)
                {
                    case "9":
                        promotionVariableList = promotion.PromotionVariables.Split(',').ToList();
                        foreach (string promotionVariable in promotionVariableList)
                        {
                            promotionVariableArray = promotionVariable.Split('-');
                            if (promotionVariableArray.Length != 4)
                                continue;

                            productId = promotionVariableArray[0];
                            int requiredProductQuantity = Convert.ToInt32(promotionVariableArray[1]);
                            string discountedProductId = promotionVariableArray[2];
                            int discountedProductQuantity = Convert.ToInt32(promotionVariableArray[3]);

                            PromotionEffect promotionEffect = new PromotionEffect(promotion, -1, -1, productId, discountedProductId, requiredProductQuantity, discountedProductQuantity);
                            if (!productPromotionDictionary.ContainsKey(discountedProductId))
                                productPromotionDictionary.Add(discountedProductId, promotionEffect);
                            if (!productPromotionDictionary.ContainsKey(productId))
                                productPromotionDictionary.Add(productId, promotionEffect);

                            product = GetProduct(discountedProductId, store);
                            if (product == null)
                                continue;
                            var requiredProduct = GetProduct(productId, store);
                            if (requiredProduct == null)
                                continue;
                            tempPromotion.ProductList.Add(requiredProduct);
                            tempPromotion.ProductList.Add(product);

                            product.DiscountedPrice = product.Price;
                            product.IsInPromotion = true;

                            requiredProduct.DiscountedPrice = requiredProduct.Price;
                            requiredProduct.IsInPromotion = true;
                        }
                        break;
                    case "1":
                        foreach (var categoryID in promotion.InCategories.Split(','))
                        {
                            if (!categoryPromotionDictionary.ContainsKey(categoryID))
                                categoryPromotionDictionary.Add(categoryID,
                                    new PromotionEffect(promotion, promotion.SaleValue, -1));

                            if (ProductModel.mProductCategoryIDDictionary.ContainsKey(categoryID))
                            {
                                foreach (string productID in ProductModel.mProductCategoryIDDictionary[categoryID])
                                {
                                    product = GetProduct(productID, store);
                                    if (product == null)
                                        continue;
                                    tempPromotion.ProductList.Add(product);
                                    product.IsInPromotion = true;
                                    product.DiscountedPrice = product.Price * (100 - promotion.SaleValue) / 100;
                                    if (product.DiscountedPrice < 0)
                                        product.DiscountedPrice = 0;
                                }
                            }
                        }
                        break;
                    case "2":
                        promotionVariableList = promotion.PromotionVariables.Split(',').ToList();
                        foreach (string promotionVariable in promotionVariableList)
                        {
                            promotionVariableArray = promotionVariable.Split('-');
                            if (promotionVariableArray.Length < 2)
                                continue;

                            productId = promotionVariableArray[0];

                            if (!productPromotionDictionary.ContainsKey(productId))
                                productPromotionDictionary.Add(productId, new PromotionEffect(promotion, promotion.SaleValue, -1));

                            product = GetProduct(productId, store);
                            if (product == null)
                                continue;
                            tempPromotion.ProductList.Add(product);
                            product.IsInPromotion = true;
                            product.DiscountedPrice = product.Price * (100 - promotion.SaleValue) / 100;
                            if (product.DiscountedPrice < 0)
                                product.DiscountedPrice = 0;
                        }
                        break;
                    case "3":
                        promotionVariableList = promotion.PromotionVariables.Split(',').ToList();
                        foreach (string promotionVariable in promotionVariableList)
                        {
                            promotionVariableArray = promotionVariable.Split('-');
                            if (promotionVariableArray.Length < 2)
                                continue;

                            productId = promotionVariableArray[0];

                            if (!productPromotionDictionary.ContainsKey(productId))
                                productPromotionDictionary.Add(productId, new PromotionEffect(promotion, -1, promotion.SaleValue));

                            product = GetProduct(productId, store);
                            if (product == null)
                                continue;

                            tempPromotion.ProductList.Add(product);
                            product.IsInPromotion = true;
                            product.DiscountedPrice = product.Price - promotion.SaleValue;
                            if (product.DiscountedPrice < 0)
                                product.DiscountedPrice = 0;
                        }
                        break;
                    case "4":
                        promotionVariableList = promotion.PromotionVariables.Split(',').ToList();
                        foreach (string promotionVariable in promotionVariableList)
                        {
                            promotionVariableArray = promotionVariable.Split('-');
                            if (promotionVariableArray.Length < 2)
                                continue;

                            productId = promotionVariableArray[0];

                            if (!productPromotionDictionary.ContainsKey(productId))
                                productPromotionDictionary.Add(productId, new PromotionEffect(promotion, -1, promotion.SaleValue));

                            product = GetProduct(productId, store);
                            if (product == null)
                                continue;

                            tempPromotion.ProductList.Add(product);
                            product.IsInPromotion = true;
                            product.DiscountedPrice = promotion.SaleValue;
                            if (product.DiscountedPrice < 0)
                                product.DiscountedPrice = 0;
                        }
                        break;
                    case "5":
                        promotionVariableList = promotion.PromotionVariables.Split(',').ToList();
                        foreach (string promotionVariable in promotionVariableList)
                        {
                            promotionVariableArray = promotionVariable.Split('-');
                            if (promotionVariableArray.Length < 2)
                                continue;

                            productId = promotionVariableArray[0];
                            decimal discountedPrice = decimal.Parse(promotionVariableArray[1]);

                            if (!productPromotionDictionary.ContainsKey(productId))
                                productPromotionDictionary.Add(productId, new PromotionEffect(promotion, -1, discountedPrice));

                            product = GetProduct(productId, store);
                            if (product == null)
                                continue;

                            tempPromotion.ProductList.Add(product);
                            product.IsInPromotion = true;
                            product.DiscountedPrice = discountedPrice;
                            if (product.DiscountedPrice < 0)
                                product.DiscountedPrice = 0;
                        }
                        break;
                    case "6":
                        foreach (var categoryID in promotion.InCategories.Split(','))
                        {
                            if (!categoryPromotionDictionary.ContainsKey(categoryID))
                                categoryPromotionDictionary.Add(categoryID, new PromotionEffect(promotion, -1, promotion.SaleValue));

                            if (ProductModel.mProductCategoryIDDictionary.ContainsKey(categoryID))
                            {
                                foreach (string productID in ProductModel.mProductCategoryIDDictionary[categoryID])
                                {
                                    product = GetProduct(productID, store);
                                    if (product == null)
                                        continue;
                                    tempPromotion.ProductList.Add(product);
                                    product.IsInPromotion = true;
                                    product.DiscountedPrice = product.Price - promotion.SaleValue;
                                    if (product.DiscountedPrice < 0)
                                        product.DiscountedPrice = 0;
                                }
                            }
                        }
                        break;
                    case "7":
                        foreach (var categoryID in promotion.InCategories.Split(','))
                        {
                            if (!categoryPromotionDictionary.ContainsKey(categoryID))
                                categoryPromotionDictionary.Add(categoryID,
                                    new PromotionEffect(promotion, promotion.SaleValue, -1));

                            if (ProductModel.mProductCategoryIDDictionary.ContainsKey(categoryID))
                            {
                                foreach (string productID in ProductModel.mProductCategoryIDDictionary[categoryID])
                                {
                                    product = GetProduct(productID, store);
                                    if (product == null)
                                        continue;
                                    tempPromotion.ProductList.Add(product);
                                    product.IsInPromotion = true;
                                    product.DiscountedPrice = product.Price;
                                }
                            }
                        }
                        break;
                    case "8":
                        promotionVariableList = promotion.PromotionVariables.Split(',').ToList();
                        foreach (string promotionVariable in promotionVariableList)
                        {
                            promotionVariableArray = promotionVariable.Split('-');
                            if (promotionVariableArray.Length < 2)
                                continue;

                            productId = promotionVariableArray[0];

                            if (!productPromotionDictionary.ContainsKey(productId))
                                productPromotionDictionary.Add(productId, new PromotionEffect(promotion, promotion.SaleValue, -1));

                            product = GetProduct(productId, store);
                            if (product == null)
                                continue;
                            tempPromotion.ProductList.Add(product);
                            product.IsInPromotion = true;
                            product.DiscountedPrice = product.Price;
                        }
                        break;
                }
                short typeId;
                short.TryParse(promotion.PromotionType, out typeId);
                tempPromotion.PromotionTypeId = typeId;
                if ((tempPromotion.EndDate - MyDevice.DubaiDateTimeNow).Value.TotalMilliseconds > 0 && (MyDevice.DubaiDateTimeNow - tempPromotion.StartDate).Value.TotalMilliseconds > 0)
                {
                    newPromotionList.Add(tempPromotion);
                }
            }

            foreach (var newPromotion in newPromotionList)
            {
                var promotion = MyDevice.PromotionList.FirstOrDefault(x => x.ImageId == newPromotion.ImageId);
                if (promotion == null)
                {
                    MyDevice.PromotionList.Add(newPromotion);
                }
                else
                {
                    MyDevice.PromotionList[MyDevice.PromotionList.IndexOf(promotion)] = promotion;
                }
            }
            //Device.BeginInvokeOnMainThread(() => {
            for (int e = MyDevice.PromotionList.Count - 1; e >= 0; e--)
            {
                Debug.WriteLine("Counter: " + e);
                Debug.WriteLine("List Count: " + MyDevice.PromotionList.Count);
                var promotionToBeDeleted = newPromotionList.FirstOrDefault(x => x.ImageId == MyDevice.PromotionList[e].ImageId);
                if (promotionToBeDeleted == null)
                {
                    MyDevice.PromotionList.Remove(MyDevice.PromotionList[e]);



                }

            }
            //    });
        }

        private static Product GetProduct(string productID, int store)
        {
            if (!ProductModel.ProductDictionary.ContainsKey(productID))
                return null;
            //if (ProductBranchInfoModel.ProductBranchInfoDictionary.ContainsKey(productID) && !ProductModel.ProductDictionary[productID].IsInStore)
            //    return null;
            //if (!ProductModel.ProductDictionary[productID].IsInStore)
            //    return null;
            var tempProduct = ProductModel.ProductDictionary[productID];

            return tempProduct;
        }

        public static PromotionEffect GetPromotion(string productID, string realCategoryID, string parentCategoryID)
        {
            PromotionEffect promotionEffect = null;
            if (productPromotionDictionary.ContainsKey(productID))
                promotionEffect = productPromotionDictionary[productID];
            else if (categoryPromotionDictionary.ContainsKey(realCategoryID))
                promotionEffect = categoryPromotionDictionary[realCategoryID];
            else if (categoryPromotionDictionary.ContainsKey(parentCategoryID))
                promotionEffect = categoryPromotionDictionary[parentCategoryID];
            return promotionEffect;
        }

        public static async Task<bool> FetchPromotions()
        {
            bool isNewEntry = false;
            bool isDeletedExists = false;

            DateTime? localUpdate = UserClassFunctions.GetPromotionsUpdatedDateFromUser();
            DateTime? remoteUpdate = null;

            var productQuery =
                ParseObject.GetQuery(ParseConstants.PROMOTIONS_CLASS_NAME)
                    .WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate);

            int queryLimit = 1000;

            List<PromotionClass> tempList = new List<PromotionClass>();
            int i = 0;
            while (true)
            {
                IEnumerable<ParseObject> promotionObjects = null;
                if (CrossConnectivity.Current.IsConnected)
                {
                    for (int t = 0; t < 3; t++)
                    {
                        int cancelTime = 5000;
                        try
                        {
                            if (productQuery != null)
                            {
                                _fetchSucceeded = true;
                                System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                                promotionObjects = await productQuery.Limit(queryLimit).Skip(i).FindAsync(cancellation.Token);
                            }
                            break;
                        }
                        catch (Exception e)
                        {
                            Crashes.TrackError(e);
                            System.Diagnostics.Debug.WriteLine(e.ToString());
                            string message = MyDevice.InternetErrorMessage1;
                            System.Diagnostics.Debug.WriteLine("Toast from: FetchPromotions " + e.Message);
                            if (t == 2)
                                _fetchSucceeded = false;

                        }
                    }
                }

                if (promotionObjects == null || promotionObjects.Count() == 0)
                    break;

                foreach (var promotionObject in promotionObjects)
                {
                    isNewEntry = true;
                    bool isDeleted = promotionObject.Get<bool>("IsDeleted");
                    if (isDeleted)
                    {
                        isDeletedExists = PromotionClassFunctions.RemovePromotion(promotionObject.ObjectId);

                        var promotion = MyDevice.PromotionList.FirstOrDefault(x => x.PromotionId == promotionObject.ObjectId);
                        if (promotion != null && promotion.ProductList != null)
                        {
                            foreach (Product product in promotion?.ProductList)
                            {
                                product.DiscountedPrice = product != null ? product.Price : 0;
                                product.IsInPromotion = false;
                            }
                            MyDevice.PromotionList?.Remove(promotion);
                        }
                    }
                    else
                    {
                        PromotionClass tempPromotion = new PromotionClass();
                        tempPromotion.objectId = promotionObject.ObjectId;

                        tempPromotion.PromotionType = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_TYPE) ? promotionObject.Get<string>(ParseConstants.PROMOTION_ATTRIBUTE_TYPE) : "N/A";
                        tempPromotion.Title = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_TITLE) ? promotionObject.Get<string>(ParseConstants.PROMOTION_ATTRIBUTE_TITLE) : "N/A";
                        tempPromotion.SubTitle = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_SUBTITLE) ? promotionObject.Get<string>(ParseConstants.PROMOTION_ATTRIBUTE_SUBTITLE) : "N/A";
                        tempPromotion.Description = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_DESCRIPTION) ? promotionObject.Get<string>(ParseConstants.PROMOTION_ATTRIBUTE_DESCRIPTION) : "N/A";
                        tempPromotion.SubDescription = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_SUBDESCRIPTION) ? promotionObject.Get<string>(ParseConstants.PROMOTION_ATTRIBUTE_SUBDESCRIPTION) : "N/A";
                        tempPromotion.ImageId = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_IMAGEID) ? promotionObject.Get<string>(ParseConstants.PROMOTION_ATTRIBUTE_IMAGEID) : "N/A";
                        if (promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_BRANCH))
                        {
                            var branchList = promotionObject.Get<IEnumerable<object>>(ParseConstants.PROMOTION_ATTRIBUTE_BRANCH).Cast<Int64>().ToList();
                            foreach (Int64 store in branchList)
                            {
                                tempPromotion.Branch += store.ToString();

                                if (store != branchList.Last())
                                    tempPromotion.Branch += ",";
                            }
                        }
                        else { tempPromotion.Branch = "N/A"; }

                        tempPromotion.StartDate = promotionObject.Get<DateTime>(ParseConstants.PROMOTION_ATTRIBUTE_STARTDATE);
                        tempPromotion.EndDate = promotionObject.Get<DateTime>(ParseConstants.PROMOTION_ATTRIBUTE_ENDDATE);
                        tempPromotion.Priority = promotionObject.Get<int>(ParseConstants.PROMOTION_ATTRIBUTE_PRIORITY);
                        tempPromotion.CreatedDate = promotionObject.CreatedAt;
                        try
                        {
                            var promotionVariablesString = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_PROMOTIONVARIABLES) ? promotionObject.Get<string>(ParseConstants.PROMOTION_ATTRIBUTE_PROMOTIONVARIABLES) : "N/A";
                            var inProductObjectList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PromotionVariables>>(promotionVariablesString);
                            foreach (PromotionVariables inProductsObject in inProductObjectList)
                            {
                                tempPromotion.PromotionVariables += inProductsObject?.ProductID + "-" + inProductsObject?.NewPrice;

                                if (inProductsObject.OutProductID != null)
                                    tempPromotion.PromotionVariables += "-" + inProductsObject?.OutProductID;
                                if (inProductsObject.OutNewValue != null)
                                    tempPromotion.PromotionVariables += "-" + inProductsObject?.OutNewValue;

                                if (inProductObjectList?.Last().ProductID != inProductsObject?.ProductID)
                                    tempPromotion.PromotionVariables += ",";
                            }
                        }
                        catch (Exception e)
                        {
                            Crashes.TrackError(e);
                            System.Diagnostics.Debug.WriteLine(e.ToString());
                            tempPromotion.PromotionVariables = "";
                        }

                        //tempPromotion.PromotionVariables = MyDevice.GetArrayAsString(promotionObject.Get<IEnumerable<object>>(ParseConstants.PROMOTION_ATTRIBUTE_PROMOTIONVARIABLES).Cast<string>().ToList());
                        tempPromotion.InCategories = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_INCATEGORIES) ? MyDevice.GetArrayAsString(promotionObject.Get<IEnumerable<object>>(ParseConstants.PROMOTION_ATTRIBUTE_INCATEGORIES).Cast<string>()?.ToList()) : "N/A";
                        tempPromotion.SaleValue = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_SALEVALUE) ? new Decimal(promotionObject.Get<double>(ParseConstants.PROMOTION_ATTRIBUTE_SALEVALUE)) : 0;
                        tempPromotion.InProductCount = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_INPRODUCTCOUNT) ? promotionObject.Get<int>(ParseConstants.PROMOTION_ATTRIBUTE_INPRODUCTCOUNT) : 0;
                        tempPromotion.OutProducts = promotionObject.ContainsKey(ParseConstants.PROMOTION_ATTRIBUTE_OUTPRODUCTS) ? MyDevice.GetArrayAsString(promotionObject.Get<IEnumerable<object>>(ParseConstants.PROMOTION_ATTRIBUTE_OUTPRODUCTS).Cast<string>()?.ToList()) : "N/A";


                        tempList.Add(tempPromotion);
                    }

                    if (remoteUpdate == null || promotionObject.UpdatedAt > remoteUpdate)
                    {
                        remoteUpdate = promotionObject.UpdatedAt;
                    }
                }
                if (promotionObjects.Count() < queryLimit)
                    break;
                else
                    i += queryLimit;
            }

            if (_fetchSucceeded && remoteUpdate != null)
                UserClassFunctions.AddPromotionsUpdateDateToUser(remoteUpdate);


            if (tempList.Count > 0)
            {
                PromotionClassFunctions.AddPromotion(tempList);
            }
            else if (!isDeletedExists)
                isNewEntry = false;

            return isNewEntry;
        }

        public static void ResetProductsInPromotion()
        {
            foreach (var product in ProductModel.ProductDictionary.Values)
            {
                product.IsInPromotion = false;
            }
        }
    }
}
