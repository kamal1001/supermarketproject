﻿using System;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using SupermarketProject.Common.Objects;
using System.Collections.Generic;
using Parse;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Models.Remote
{
    public static class SuggestModel
    {
        public static async Task<bool> SendSuggestionToRemote(string text, int type)
        {
            ParseObject suggestion = new ParseObject(ParseConstants.SUGGESTIONS_CLASS_NAME);

            suggestion[ParseConstants.SUGGESTIONS_ATTRIBUTE_SUGGESTION] = text;
            suggestion[ParseConstants.SUGGESTIONS_ATTRIBUTE_STATE] = 0;
            suggestion[ParseConstants.SUGGESTIONS_ATTRIBUTE_USER] = Parse.ParseUser.CurrentUser;
            suggestion[ParseConstants.SUGGESTIONS_ATTRIBUTE_TYPE] = type;
            bool test = false;
            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 10000;
                try
                {
                    if (suggestion != null)
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        await suggestion.SaveAsync(cancellation.Token);
                        test = true;
                    }
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    //string message = MyDevice.InternetErrorMessage1;
                    //Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                }
            }
            /*if (!test) 
            {
                int cancelTime = 3000;
                string message = "We couldn't send your suggestion. Please check your internet connection.";
                Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));

            }*/
            return test;
        }
    }
}
