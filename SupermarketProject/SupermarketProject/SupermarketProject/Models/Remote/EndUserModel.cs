﻿#if Loyalty
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupermarketProject.Common.Objects;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using Parse;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;

namespace SupermarketProject.Models.Remote
{
    public static class EndUserModel
    {
        public static async Task<bool> SetEndUser()
        {
            bool isSuccess = false;

            if (CrossConnectivity.Current.IsConnected) 
                {
                try
                {
                    if (ParseUser.CurrentUser != null) 
                    { 
                        await ParseUser.CurrentUser.FetchAsync();
                        EndUser.DefaultInstance.UserPoints = ParseUser.CurrentUser.Get<int>("Points");
                    }
                    isSuccess = true;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    EndUser.DefaultInstance.UserPoints = ParseUser.CurrentUser.Get<int>("Points");
                }
            }

            return isSuccess;
        }


        public static async Task<bool> UpdateEndUser()
        {
            bool isSuccess = false;

            try
            {
                if (ParseUser.CurrentUser != null) 
                { 
                    ParseUser.CurrentUser["Points"] = EndUser.DefaultInstance.UserPoints;
                    await ParseUser.CurrentUser.SaveAsync();
                }
                isSuccess = true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                Crashes.TrackError(e);
                isSuccess = false;
            }            

            return isSuccess;
        }        
    }
}
#endif