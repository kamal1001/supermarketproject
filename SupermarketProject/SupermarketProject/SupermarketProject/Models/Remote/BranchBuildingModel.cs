﻿using System;
using SupermarketProject.Common.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Parse;
using SupermarketProject.Models.Local;
using System.Linq;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;

namespace SupermarketProject.Models.Remote
{
    public class BranchBuildingModel
    {
        public static List<BranchBuildingClass> BranchBuildingList;
        public static async Task FetchBranchBuildings()
        {
            DateTime? localUpdate = UserClassFunctions.GetBranchBuildingUpdatedDateFromUser();
            DateTime? remoteUpdate = null;
            int queryLimit = 1000;
            var buildingQuery = ParseObject.GetQuery("BranchBuilding").WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate).Limit(queryLimit);

            IEnumerable<ParseObject> buildingObjects = null;
            if (CrossConnectivity.Current.IsConnected)
            {
                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 5000;
                    try
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        buildingObjects = await buildingQuery.FindAsync(cancellation.Token);
                        break;
                    }
                    catch (Exception e)
                    {
                        string message = MyDevice.InternetErrorMessage1;
                        Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                        System.Diagnostics.Debug.WriteLine("Toast from: Fetch BranchBuilding Relations " + e.Message);

                        Crashes.TrackError(e);
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                    }
                }
            }
            if (buildingObjects != null)
            {
                var enumerable = buildingObjects as ParseObject[] ?? buildingObjects.ToArray();
                if (!enumerable.Any())
                {
                    BranchBuildingList = BranchBuildingClassFunctions.GetBranchBuilding();
                    return;
                }

                List<BranchBuildingClass> branchbuildingList = new List<BranchBuildingClass>();
                foreach (var buildingObject in enumerable)
                {
                    BranchBuildingClass building = new BranchBuildingClass
                    {
                        objectId = buildingObject.ObjectId,
                        BranchId = buildingObject.Get<int>("BranchId"),
                        BuildingId = buildingObject.Get<string>("BuildingId"),
                        MinOrder = buildingObject.Get<int>("MinOrder"),
                        MinOrderWithout = buildingObject.Get<int>("MinOrderWithout"),
                        IsDeleted = buildingObject.ContainsKey("IsDeleted") && buildingObject.Get<bool>("IsDeleted")
                    };

                    branchbuildingList.Add(building);

                    if (remoteUpdate == null || buildingObject.UpdatedAt > remoteUpdate)
                        remoteUpdate = buildingObject.UpdatedAt;
                }
                BranchBuildingClassFunctions.AddBranchBuilding(branchbuildingList);
            }
            if (remoteUpdate != null)
                UserClassFunctions.AddBranchBuildingUpdateDateToUser(remoteUpdate);
            BranchBuildingList = BranchBuildingClassFunctions.GetBranchBuilding();
        }

    }
}
