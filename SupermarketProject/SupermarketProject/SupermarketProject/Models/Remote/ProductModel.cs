﻿using System;
using PCLStorage;
using System.Collections.Generic;
using SupermarketProject.Common.Utilities;
using Parse;
using System.Net.Http;
using System.Linq;
using SupermarketProject.Models.Local;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Globalization;
using System.Linq.Expressions;
using SupermarketProject.Common.Objects;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;

namespace SupermarketProject.Models.Remote
{
    public static class ProductModel
    {
        public static IFolder mRootFolder;
        public static string mRootFolderPath;
        public static Dictionary<string, Product> ProductDictionary;
        public static List<Product> mSearchProductList;
        public static Dictionary<string, HashSet<string>> mProductCategoryIDDictionary;

        static ProductModel()
        {
            InitializeMemberVariables();
        }

        private static void InitializeMemberVariables()
        {

            mRootFolder = FileSystem.Current.LocalStorage;
            mRootFolderPath = mRootFolder.Path;
            ProductDictionary = new Dictionary<string, Product>(StringComparer.Ordinal);
            mSearchProductList = new List<Product>();
            mProductCategoryIDDictionary = new Dictionary<string, HashSet<string>>(StringComparer.Ordinal);

        }

        private static void ClearContainers()
        {
            ProductDictionary.Clear();
            mSearchProductList.Clear();
            mProductCategoryIDDictionary.Clear();
        }

        public static async Task FetchFavoriteAndCartProducts()
        {
            if (ParseUser.CurrentUser == null)
                return;

            List<string> productIds = new List<string>();
            var cartProducts = CartClassFunctions.GetProductList()?.Select(x => x.ProductID).ToList();
            var favoriteProducts = ParseUser.CurrentUser.ContainsKey("Favorites") ? ParseUser.CurrentUser.Get<string>("Favorites").Split(',').ToList() : null;

            if (cartProducts != null && cartProducts.Any())
                productIds.AddRange(cartProducts);
            if (favoriteProducts != null && favoriteProducts.Any())
                productIds.AddRange(favoriteProducts);

            if (productIds.Any())
                await FetchProductsByProductList(productIds);
        }

        public static async Task FetchProductsByProductList(List<string> productIdList)
        {
            if (productIdList == null || !productIdList.Any())
                return;
            await ProductBranchInfoModel.FetchProductBranchInfosByProductList(productIdList);
        }

        public static async Task FetchProductsByCategory(string parentCategoryId)
        {
            await ProductBranchInfoModel.FetchProductBranchInfosByCategory(parentCategoryId);
            Cart.Initialize();
        }
        public static async Task FetchAllProducts()
        {
            await ProductBranchInfoModel.FetchAllProductBranchInfos();

            if (CrossConnectivity.Current.IsConnected)
            {

                var productQuery = ParseObject.GetQuery(ParseConstants.PRODUCTS_CLASS_NAME);

                var allObjects = await FetchProducts(productQuery);

                if (allObjects.Any())
                {
                    AddFetchedProductsToLocal(allObjects);
                    DateTime? remoteUpdate = allObjects.Max(x => x.UpdatedAt);

                    if (remoteUpdate != null)
                        UserClassFunctions.AddProductsUpdateDateToUser(remoteUpdate);
                }
            }
        }
        public static async Task FetchUpdatedProducts()
        {
            DateTime? localUpdate = UserClassFunctions.GetProductsUpdatedDateFromUser();

            if (CrossConnectivity.Current.IsConnected)
            {
            var productQuery = ParseObject.GetQuery(ParseConstants.PRODUCTS_CLASS_NAME).
                WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate);

                var allObjects = await FetchProducts(productQuery);

                if (allObjects.Any())
                {
                    AddFetchedProductsToLocal(allObjects);
                    DateTime? remoteUpdate = allObjects.Max(x => x.UpdatedAt);

                    if (remoteUpdate != null)
                        UserClassFunctions.AddProductsUpdateDateToUser(remoteUpdate);
                }
            }
        }

        private static async Task<List<ParseObject>> FetchProducts(ParseQuery<ParseObject> productQuery)
        {
            int queryLimit = 1000;

            List<ParseObject> allObjects = new List<ParseObject>();
            int i = 0;
            productQuery = productQuery.Select(ParseConstants.PRODUCT_ATTRIBUTE_NAME)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_IMAGEID)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_CATEGORYID)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_QUANTITY)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_PRIORITY)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_DELETED)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_VAT)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_BARCODE)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_ISGRAMBASED)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_PARENTCATEGORY)
                                       .Select(ParseConstants.PRODUCT_ATTRIBUTE_KEYWORDS);
            while (true)
            {
                IEnumerable<ParseObject> productObjects = null;
                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 15000;
                    try
                    {
                        if (productQuery != null)
                        {
                            System.Threading.CancellationTokenSource cancellation =
                                new System.Threading.CancellationTokenSource(cancelTime);
                            productObjects = await productQuery.Limit(queryLimit).Skip(i).FindAsync(cancellation.Token);
                        }
                        break;
                    }
                    catch (Exception e)
                    {
                        Crashes.TrackError(e);
                        System.Diagnostics.Debug.WriteLine(e.ToString());

                        string message = MyDevice.InternetErrorMessage1;
                        Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message)
                            .SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White)
                            .SetDuration(cancelTime));
                        System.Diagnostics.Debug.WriteLine("Toast from: FetchProducts " + e.Message);
                    }
                }

                if (productObjects == null)
                    break;

                var collection = productObjects as ParseObject[] ?? productObjects.ToArray();

                allObjects.AddRange(collection);

                if (collection?.Count() < queryLimit)
                    break;

                i += queryLimit;
            }
            return allObjects;
        }

        private static void AddFetchedProductsToLocal(List<ParseObject> productObjects, bool updateDate = true)
        {
            if (productObjects == null || !productObjects.Any())
                return;

            List<ProductClass> tempList = new List<ProductClass>();

            foreach (var productObject in productObjects)
            {
                ProductClass tempProduct = new ProductClass();

                tempProduct.objectId = productObject.ObjectId;
                tempProduct.Name = productObject.Get<string>(ParseConstants.PRODUCT_ATTRIBUTE_NAME);
                tempProduct.ImageID = productObject.Get<string>(ParseConstants.PRODUCT_ATTRIBUTE_IMAGEID);
                tempProduct.CategoryId = productObject.Get<string>(ParseConstants.PRODUCT_ATTRIBUTE_CATEGORYID);
                tempProduct.Quantity = productObject.Get<string>(ParseConstants.PRODUCT_ATTRIBUTE_QUANTITY);
                tempProduct.Priority = productObject.Get<int>(ParseConstants.PRODUCT_ATTRIBUTE_PRIORITY);
                tempProduct.IsDeleted = productObject.ContainsKey(ParseConstants.PRODUCT_ATTRIBUTE_DELETED) && productObject.Get<bool>(ParseConstants.PRODUCT_ATTRIBUTE_DELETED);
                tempProduct.Vat = productObject.ContainsKey(ParseConstants.PRODUCT_ATTRIBUTE_VAT) ? productObject.Get<double>(ParseConstants.PRODUCT_ATTRIBUTE_VAT) : 0;
                tempProduct.UpdatedAt = updateDate ? productObject.UpdatedAt : DateTime.MinValue;
#if Loyalty
                    tempProduct.LoyaltyPoints = productObject.ContainsKey(ParseConstants.PRODUCT_ATTRIBUTE_LOYALTYRATIO)
				        ? productObject.Get<int>(ParseConstants.PRODUCT_ATTRIBUTE_LOYALTYRATIO) : -1;
#endif
                string barcode;
                productObject.TryGetValue(ParseConstants.PRODUCT_ATTRIBUTE_BARCODE, out barcode);
                if (barcode == null)
                    barcode = string.Empty;
                tempProduct.Barcode = barcode;
#if SelectionByBrandActivated
					bool IsSelectedByBrand = false;
					productObject.TryGetValue<bool>(ParseConstants.PRODUCT_ATTRIBUTE_ISSELECTEDBYBRAND, out IsSelectedByBrand);
#else
                bool IsSelectedByBrand = true;
#endif
                tempProduct.IsSelectedByBrand = IsSelectedByBrand;

                bool tempOut = false;
                productObject.TryGetValue<bool>(ParseConstants.PRODUCT_ATTRIBUTE_ISGRAMBASED, out tempOut);
                tempProduct.IsGramBased = tempOut;
                tempProduct.ParentCategory = productObject.Get<string>(ParseConstants.PRODUCT_ATTRIBUTE_PARENTCATEGORY);
                int listCount = 1;

                IEnumerable<object> keywordIEnumarable;
                productObject.TryGetValue<IEnumerable<object>>(ParseConstants.PRODUCT_ATTRIBUTE_KEYWORDS, out keywordIEnumarable);

                if (keywordIEnumarable == null || keywordIEnumarable.Count<object>() == 0)
                {
                    tempProduct.Keywords = "";
                }
                else
                {
                    var keywordList = keywordIEnumarable.Cast<string>();
                    listCount = 1;
                    foreach (string keyword in keywordList)
                    {
                        tempProduct.Keywords += keyword;
                        if (listCount != keywordList.Count())
                            tempProduct.Keywords += ",";
                        listCount++;
                    }
                }
                tempList?.Add(tempProduct);

            }

            if (tempList.Count > 0)
            {
                ProductClassFunctions.AddProduct(tempList);
                PopulateProductDictionaries(tempList);
            }

        }

        public static void PopulateProductDictionaries(List<ProductClass> syncedProducts = null)
        {
            bool addNewProducts = syncedProducts != null;

            List<ProductClass> products;

            if (!addNewProducts)
            {
                ClearContainers();
                products = ProductClassFunctions.GetProducts();
            }
            else
                products = syncedProducts;


            foreach (ProductClass product in products)
            {
                if (mProductCategoryIDDictionary.ContainsKey(product.CategoryId))
                {
                    if (addNewProducts)
                    {
                        if (!mProductCategoryIDDictionary[product.CategoryId].Contains(product.objectId))
                            mProductCategoryIDDictionary[product.CategoryId].Add(product.objectId);
                    }
                    else
                        mProductCategoryIDDictionary[product.CategoryId].Add(product.objectId);
                }
                else
                {
                    mProductCategoryIDDictionary.Add(product.CategoryId, new HashSet<string>(StringComparer.Ordinal));
                    mProductCategoryIDDictionary[product.CategoryId].Add(product.objectId);
                }


                string parentCategoryID = product.ParentCategory;
                string ImageName = product.ImageID + ".jpg";
                string ImagePath = mRootFolderPath + "/" + ParseConstants.IMAGE_FOLDER_NAME + "/" + ImageName;
                string EnviorementImagePath = MyDevice.BrandFolderName + ".SavedImages." + ImageName;
                string ProductName = product.Name;
                string quantity = product.Quantity;
                bool isGramBased = product.IsGramBased;
                string ImageID = product.ImageID;
                string barcode = product.Barcode;
                double vat = product.Vat;
                int initialLoyaltyPoints = 0;
                DateTime? updatedAt = product.UpdatedAt;
                if (updatedAt == null)
                    updatedAt = DateTime.MinValue;
#if Loyalty
                //calculation according to category loyalty ratios needs to be done when the price is set
			    initialLoyaltyPoints = product.LoyaltyPoints;
#endif
                var tempProduct = new Product(product.objectId, ProductName, ImageName, ImagePath, EnviorementImagePath, parentCategoryID, quantity, isGramBased, ImageID, barcode, product.Keywords, product.CategoryId, vat, updatedAt, 0, initialLoyaltyPoints);

                if (addNewProducts)
                {
                    if (ProductDictionary.ContainsKey(tempProduct.ProductID))
                        ProductDictionary[tempProduct.ProductID] = tempProduct;
                    else
                        ProductDictionary.Add(tempProduct.ProductID, tempProduct);
                }
                else
                    ProductDictionary.Add(tempProduct.ProductID, tempProduct);
            }
        }

        public static async Task PopulateSearchProductList(string searchString)
        {
            mSearchProductList.Clear();
            if (ProductDictionary?.Count > 0)
            {
                foreach (var product in ProductDictionary.Values)
                {
                    if (ContainsString(product.Name, searchString, StringComparison.OrdinalIgnoreCase))
                    {
                        mSearchProductList?.Add(product);
                    }
                    else
                    {
                        if (product.Keywords == "")
                            continue;
                        var keywords = product.Keywords.Split(',');
                        foreach (string keyword in keywords)
                        {
                            if (keyword.ToLower() == searchString.ToLower())
                            {
                                mSearchProductList?.Add(product);
                            }
                        }
                    }
                    if (mSearchProductList?.Count >= 400)
                        break;
                }

                await ProductBranchInfoModel.FetchProductBranchInfosByProductList(mSearchProductList?.Select(x => x.ProductID)?.ToList());
                mSearchProductList = mSearchProductList?.Where(x => x.IsInStore).ToList();

                if (mSearchProductList.Count > 0)
                {
                    var ProductItems = mSearchProductList?.Select(s => s.Name).ToList();
                    var listofitems = SearchComputation.GetSuggestedWords(searchString, ProductItems, ProductItems.Count);
                    var listofitems2 = listofitems?.ToDictionary(x => x.Key, x => x.Value).Keys.ToList();
                    mSearchProductList = mSearchProductList?.OrderBy(s => listofitems2.IndexOf(s.Name))?.ToList();
                }
            }
        }
        public static bool ContainsString(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }

        public static Product ProductItem(string ProductName)
        {
            return ProductDictionary.Values?.Where(x => ProductName == x.Name).First();
        }
    }
}