﻿using System;
using PCLStorage;
using System.Linq;
using Parse;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;
using Acr.UserDialogs;

namespace SupermarketProject.Models.Remote
{
    public static class ImageModel
    {
        public static IFolder mRootFolder = FileSystem.Current.LocalStorage;
        public static string mRootFolderPath = mRootFolder.Path;

        private static Dictionary<string, ImageClass> _imageDictionary;

        private static Dictionary<ImageClass, Tuple<string, View>> _waitingImageDictionary = new Dictionary<ImageClass, Tuple<string, View>>();

        public static async Task GetImagesFromRemote(LoadingPage loadingPage = null)
        {
            try
            {
                DateTime? localUpdate = UserClassFunctions.GetImageUpdatedDateFromUser();

                var imageQuery = ParseObject.GetQuery(ParseConstants.IMAGES_CLASS_NAME).
                    WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate);

                imageQuery = imageQuery.Select(ParseConstants.IMAGE_ATTRIBUTE_IMAGEFILE);


                DateTime? remoteUpdate = null;
                List<ImageClass> images = new List<ImageClass>();
                int i = 0;
                int queryLimit = 1000;
                int j = 0;
                bool isAnyErrorOccured = false;

                while (true)
                {

                    IEnumerable<ParseObject> imageObjects = null;
                    if (CrossConnectivity.Current.IsConnected)
                    {
                        for (int t = 0; t < 3; t++)
                        {
                            int cancelTime = 15000;
                            try
                            {
                                if (imageQuery != null)
                                {
                                    System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                                    imageObjects = await imageQuery.Limit(queryLimit).Skip(i).FindAsync(cancellation.Token);
                                }
                                break;
                            }
                            catch (Exception e)
                            {
                                Crashes.TrackError(e);
                                string message = MyDevice.InternetErrorMessage1;
                                ShowToast(cancelTime, message);

                                System.Diagnostics.Debug.WriteLine(e.ToString());
                                System.Diagnostics.Debug.WriteLine("Toast from: GetImagesFromRemote " + e.Message);
                                if (t == 2)
                                    isAnyErrorOccured = true;
                            }
                        }
                    }

                    if (imageObjects == null || imageObjects.Count() == 0)
                        break;
                    foreach (ParseObject imageObject in imageObjects)
                    {
                        ParseFile img = imageObject.Get<ParseFile>(ParseConstants.IMAGE_ATTRIBUTE_IMAGEFILE);

                        ImageClass image = new ImageClass();
                        image.objectId = imageObject.ObjectId;
                        image.Url = img.Url.ToString();
                        images.Add(image);
                        if (remoteUpdate == null || imageObject.UpdatedAt > remoteUpdate)
                            remoteUpdate = imageObject.UpdatedAt;
                        j++;
                        if (loadingPage != null && i % 300 == 0)
                        {
                            double scrollPos = Decimal.ToDouble(Decimal.Add(Decimal.Multiply(Decimal.Multiply(Decimal.Divide((Decimal.Divide(1, 1000)), 10), 2), j), new decimal(0.3f)));
                            loadingPage.ProgressBar1.Progress = scrollPos;
                        }
                    }
                    if (imageObjects.Count() < queryLimit)
                        break;
                    else
                        i += queryLimit;

                }
                if (images.Count > 0)
                {
                    ImageClassFunctions.AddImage(images);
                    if (!isAnyErrorOccured && remoteUpdate != null)
                        UserClassFunctions.AddImagesUpdateDateToUser(remoteUpdate);

                    foreach (ImageClass imageClass in images)
                    {
                        if (_imageDictionary.ContainsKey(imageClass.objectId))
                            _imageDictionary[imageClass.objectId] = imageClass;
                        else
                            _imageDictionary.Add(imageClass.objectId, imageClass);
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
            }

        }

        private static void ShowToast(int cancelTime, string message)
        {
            try
            {
                UserDialogs.Instance?.Toast(new ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }

        public static async Task InitializeImageModel(LoadingPage loadingPage)
        {
            _imageDictionary = new Dictionary<string, ImageClass>();
            var images = ImageClassFunctions.GetImages();
            foreach (var image in images)
            {
                _imageDictionary.Add(image.objectId, image);
            }

            if (CrossConnectivity.Current.IsConnected)
            {
                await GetImagesFromRemote(loadingPage);
            }
        }

        public static void SetImageSource(View imageView, string imageName, string imagePath, string imageEnvironmentPath)
        {
            ImageSource imageSource = null;
            if (_imageDictionary.ContainsKey(imageName.Split('.')[0]))
            {
                var image = _imageDictionary[imageName.Split('.')[0]];
                imageSource = ImageSource.FromUri(new Uri(image.Url.Replace(" ", "%20")));
            }
            else if (MyDevice.EmbeddedResourceImageNameStrings.Contains(imageName))
            {
                imageSource = ImageSource.FromResource(imageEnvironmentPath);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Missing Image : " + imageName);
                string enviorementImagePath = MyDevice.BrandFolderName + ".SavedImages." + "missing.jpg";
                imageSource = ImageSource.FromResource(enviorementImagePath);
            }

            SetViewImage(imageView, imageSource);
        }

        private static void SetViewImage(View imageView, ImageSource imageSource)
        {
            if (imageView is Image)
                ((Image)imageView).Source = imageSource;
            else if (imageView is CachedImage)
                ((CachedImage)imageView).Source = imageSource;
            else
            {
                System.Diagnostics.Debug.WriteLine("Incompatible view type!");
            }
        }
    }

}