﻿using System;
using SupermarketProject.Common.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Diagnostics;
using Parse;
using SupermarketProject.Models.Local;
using System.Linq;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Models.Remote
{
    public class UserStampModel
    {
        public static List<UserStampClass> UserStampList = new List<UserStampClass>();
        public static async Task FetchUserStamps()
        {
            DateTime? localUpdate = UserClassFunctions.GetUserStampsUpdatedDateFromUser();
            DateTime? remoteUpdate = null;
            int queryLimit = 1000;
            var userStampQuery = ParseObject.GetQuery(ParseConstants.USERSTAMPS_CLASS_NAME).WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate).WhereEqualTo(ParseConstants.USERSTAMPS_ATTRIBUTE_USERID, ParseUser.CurrentUser.ObjectId).Limit(queryLimit);

            IEnumerable<ParseObject> userStampObjects = null;

            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 5000;
                try
                {
                    System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                    userStampObjects = await userStampQuery.FindAsync(cancellation.Token);
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                    Debug.WriteLine("Toast from: FetchUserStamps " + e.Message);
                }
            }

            if (userStampObjects != null)
            {
                var enumerable = userStampObjects as ParseObject[] ?? userStampObjects.ToArray();
                if (!enumerable.Any())
                {
                    UserStampList = UserStampClassFunctions.GetUserStamps();
                    return;
                }

                List<UserStampClass> userStamps = new List<UserStampClass>();
                foreach (var userStampObject in enumerable)
                {
                    UserStampClass userStamp = new UserStampClass();
                    userStamp.objectId = userStampObject.ObjectId;
                    userStamp.UserId = userStampObject.Get<string>(ParseConstants.USERSTAMPS_ATTRIBUTE_USERID);
                    userStamp.StampId = userStampObject.Get<string>(ParseConstants.USERSTAMPS_ATTRIBUTE_STAMPID);
                    userStamp.Count = userStampObject.Get<int>(ParseConstants.USERSTAMPS_ATTRIBUTE_COUNT);

                    userStamps.Add(userStamp);
                    if (remoteUpdate == null || userStampObject.UpdatedAt > remoteUpdate)
                        remoteUpdate = userStampObject.UpdatedAt;
                }
                UserStampClassFunctions.AddUserStampList(userStamps);
            }
            UserClassFunctions.AddUserStampsUpdateDateToUser(remoteUpdate);
            UserStampList = UserStampClassFunctions.GetUserStamps();
        }

        public static async Task<bool> AddUserStamp(string stampId, int count)
        {
            bool isSucceed = false;
            int cancelTime = 10000;
            System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);

            var userStamp = UserStampList.Find(x => x.StampId == stampId);

            for (int t = 0; t < 3; t++)
            {
                try
                {
                    ParseObject userStamParseObject;

                    if (userStamp == null)
                        userStamParseObject = new ParseObject(ParseConstants.USERSTAMPS_CLASS_NAME);
                    else
                        userStamParseObject = await ParseObject.GetQuery(ParseConstants.USERSTAMPS_CLASS_NAME).WhereEqualTo(ParseConstants.ObjectId, userStamp.objectId).FirstOrDefaultAsync(cancellation.Token);

                    userStamParseObject[ParseConstants.USERSTAMPS_ATTRIBUTE_STAMPID] = stampId;
                    userStamParseObject[ParseConstants.USERSTAMPS_ATTRIBUTE_USERID] = ParseUser.CurrentUser.ObjectId;
                    userStamParseObject[ParseConstants.USERSTAMPS_ATTRIBUTE_COUNT] = count;

                    await userStamParseObject.SaveAsync(cancellation.Token);

                    if (userStamp == null)
                    {
                        userStamp = new UserStampClass
                        {
                            Count = count,
                            objectId = userStamParseObject.ObjectId,
                            StampId = stampId,
                            UserId = ParseUser.CurrentUser.ObjectId
                        };
                        UserStampClassFunctions.AddUserStamp(userStamp);
                        UserStampList.Add(userStamp);
                    }
                    else
                    {
                        var oldStamp = UserStampList.Find(x => x.objectId == userStamp.objectId);
                        userStamp.Count = count;
                        UserStampList.Remove(oldStamp);
                        UserStampList.Add(userStamp);
                        UserStampClassFunctions.UpdateUserStamp(userStamp);
                    }

                    isSucceed = true;
                    break;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    Crashes.TrackError(e);
                    Debug.WriteLine("Error sending stamp : " + e.Message);
                }
            }

            return isSucceed;
        }

        public static UserStampClass GetUserStamp(string userStampId)
        {
            return UserStampList.Find(x => x.objectId == userStampId);
        }

    }
}
