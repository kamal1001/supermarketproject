﻿using System;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using SupermarketProject.Common.Objects;
using System.Collections.Generic;
using Parse;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using Xamarin.Forms;
using System.Diagnostics;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Models.Remote
{
    public static class OrderModel
    {
        public enum OrderStatus { WAITING_CONFIRMATION, CONFIRMED, IN_TRANSIT, COMPLETED, CANCELED };
        private struct OrderObject
        {
            public string ProductId;
            public string ImageId;
            public string Quantity;
            public string Product;
            public string Description;
            public string Price;
            public string FormattedPrice;
            public bool IsAvailable;
            public string Barcode;
            public string PromotionId;
            public string PromotionTitle;
            public string PromotionType;
            public string PricePerItem;
            public string DiscountedPricePerItem;
            public string Vat;
            public bool IsCanceled;
#if Stamp
		    public bool IsStampRelated;
#endif
#if Loyalty
            public int PointPerItem;
#endif
        };

        public static async Task<bool> CancelOrder(string objectId, string cancelReason)
        {
            bool bSuccess = false;

            if (string.IsNullOrWhiteSpace(objectId))
                return bSuccess;

            var orderObject = await GetOrderFromRemote(objectId);

            if (orderObject == null || orderObject.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_STATUS) > 1)
            {
                if (orderObject.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_STATUS) > 1 && orderObject.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_STATUS) != 4) { await Application.Current.MainPage.DisplayAlert("Attention!", "The order cancellation is not possible as it has already gone for delivery.", "Ok"); }
                else if (orderObject.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_STATUS) == 4) { await Application.Current.MainPage.DisplayAlert("Attention!", "The order is cancelled.", "Ok"); }
                return bSuccess;
            }

            //var finishDate = await Time.GetTime();
            var finishDate = DateTime.UtcNow.AddHours(1);

            orderObject[ParseConstants.ORDERS_ATTRIBUTE_STATUS] = 4;
            orderObject[ParseConstants.ORDERS_ATTRIBUTE_CANCELREASON] = /*"CancelledByUser"*/ cancelReason;
            orderObject[ParseConstants.ORDERS_FINISH_DATE] = finishDate;
            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 15000;
                try
                {
                    if (orderObject != null)
                    {
                        var cancellation = new CancellationTokenSource(cancelTime);
                        await orderObject.SaveAsync(cancellation.Token);
                        bSuccess = true;
                    }
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    bSuccess = false;
                }
            }

            if (bSuccess)
            {
#if RewardNewUser
                //check if new reward is used in that order
                if (orderObject.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_DISCOUNTFORNEWUSER) &&
                    orderObject.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_DISCOUNTFORNEWUSER) > 0)
                {
                    //return new user reward
                    int cancelTime = 15000;
                    CancellationTokenSource userCancellation = new CancellationTokenSource(cancelTime);
                    ParseUser.CurrentUser["NewRewardUsed"] = false;
                    await ParseUser.CurrentUser.SaveAsync(userCancellation.Token);
                }
#endif
                if (orderObject.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_FIRSTTIMEORDER) && orderObject.Get<bool>(ParseConstants.ORDERS_ATTRIBUTE_FIRSTTIMEORDER))
                {
                    int cancelTime = 15000;
                    CancellationTokenSource userCancellation = new CancellationTokenSource(cancelTime);
                    ParseUser.CurrentUser["NewRewardUsed"] = false;
                    await ParseUser.CurrentUser.SaveAsync(userCancellation.Token);
                }

#if Loyalty
                //return used points
                if (orderObject.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_USEDPOINTS) &&
                    orderObject.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_USEDPOINTS) > 0)
                {
                    EndUser.DefaultInstance.UserPoints += orderObject.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_USEDPOINTS);
                    await EndUserModel.UpdateEndUser();
                    MyDevice.Footer.UpdatePoints();
                }
#endif

            }

            return bSuccess;
        }

        private static async Task<ParseObject> GetOrderFromRemote(string objectId)
        {
            if (objectId == null)
                return null;

            var parseQuery = ParseObject.GetQuery(ParseConstants.ORDERS_CLASS_NAME)
                .WhereEqualTo(ParseConstants.ObjectId, objectId);

            ParseObject orderObject = null;
            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 15000;
                try
                {
                    if (parseQuery != null)
                    {
                        var cancellation = new CancellationTokenSource(cancelTime);
                        orderObject = await parseQuery.FirstAsync(cancellation.Token);
                    }
                    break;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    Crashes.TrackError(e);
                    return null;
                }
            }

            return orderObject;
        }

        public static async Task<bool> SendOrderToRemote(UserClass user, int change, bool bIsCash = false, bool shouldCall = true)
        {
#if Laundry
		   //send laundry orders first
		   var laundries =  await LaundryModel.SendLaundryOrdersToRemote(user);
#endif
#if Stamp
		    var stamps = await StampRewardModel.SendStampRewardsToRemote();
#endif

            AddressClass address = AddressClassFunctions.GetActiveAddress();
            List<OrderObject> orderList = new List<OrderObject>();

            bool stampPopupShown = false;
            foreach (Product product in Cart.ProductsInCart)
            {
#if Stamp
                //check for stamps
                if (product.IsInStamp && !stampPopupShown)
                {
                    AlertClass alert = AlertModel.GetAlert(AlertType.Stamp_Order_Contains_Stamp);
                    await Application.Current.MainPage.DisplayAlert(alert.Title, alert.Message, alert.OkText);
                    stampPopupShown = true;
                }
#endif

                int quantity = product.ProductNumberInCart;
                string productName = product.Name;
                string description = product.Quantity;

                PromotionEffect promotionEffect = PromotionModel.GetPromotion(product.ProductID, product.RealCategoryID, product.ParentCategory);
                string promotionId = string.Empty;
                string promotionTitle = string.Empty;
                string promotionType = string.Empty;
                if (promotionEffect != null)
                {
                    promotionId = promotionEffect.promotion.objectId;
                    promotionTitle = promotionEffect.promotion.Title;
                    promotionType = promotionEffect.promotion.PromotionType;
                }

                //seperate cart based promotions to another order object
                if (promotionType == "9" && promotionEffect.requiredProductId != product.ProductID)
                {
                    //get discounted product count
                    int discountedProductCount = Cart.Instance.AppliedPromotionDictionary.ContainsKey(product.ProductID) ? Cart.Instance.AppliedPromotionDictionary[product.ProductID].DiscountedProductCount : 0;

                    if (discountedProductCount != 0)
                    {
                        //create separate order object
                        OrderObject discountedOrderObject;
                        discountedOrderObject.FormattedPrice = null;
                        discountedOrderObject.ProductId = product.ProductID;
                        discountedOrderObject.ImageId = product.ImageID;
                        //quantity is discountedProductCount
                        discountedOrderObject.Quantity = discountedProductCount.ToString();
                        discountedOrderObject.Product = productName;
                        discountedOrderObject.Description = description;
                        //cost is 0
                        discountedOrderObject.Price = "0";
                        discountedOrderObject.IsAvailable = ReleaseConfig.DefaultOrderStockAvailability;
                        discountedOrderObject.Barcode = product.Barcode;
                        discountedOrderObject.PricePerItem = product.Price.ToString().Replace(',', '.');
                        discountedOrderObject.DiscountedPricePerItem = "0";
                        discountedOrderObject.PromotionId = promotionId;
                        discountedOrderObject.PromotionTitle = promotionTitle;
                        discountedOrderObject.PromotionType = promotionType;
                        discountedOrderObject.Vat = product.Vat.ToString();
#if Stamp
                        discountedOrderObject.IsStampRelated = product.IsInStamp;
#endif
#if Loyalty
                    discountedOrderObject.PointPerItem = 0;
#endif
                        discountedOrderObject.IsCanceled = false;
                        orderList.Add(discountedOrderObject);

                        //reset promotion variables for the remaining products
                        promotionId = string.Empty;
                        promotionTitle = string.Empty;
                        promotionType = string.Empty;

                        //reduce quantity
                        quantity -= discountedProductCount;

                        if (quantity == 0)
                            continue;
                    }
                }

                var productPrice = quantity * (product.IsInPromotion
                                       ? product.DiscountedPrice * (decimal)((100) / 100)
                                       : product.Price * (decimal)((100) / 100));
#if RewardNewUser
                //apply extra discount
				if (MyDevice.IsProductSuitableForNewUserPromotion(product) && bIsCash)
			        productPrice = productPrice * (100 - MyDevice.NewUserRewardPercentage) / 100; 
#endif

                string cost = productPrice.ToString().Replace(',', '.');

                OrderObject orderObject;
                orderObject.FormattedPrice = null;
                orderObject.ProductId = product.ProductID;
                orderObject.ImageId = product.ImageID;
                orderObject.Quantity = quantity.ToString();
                orderObject.Product = productName;
                orderObject.Description = description;
                orderObject.Price = cost;
                orderObject.IsAvailable = ReleaseConfig.DefaultOrderStockAvailability;
                orderObject.Barcode = product.Barcode;
                orderObject.PricePerItem = product.Price.ToString().Replace(',', '.');
                orderObject.DiscountedPricePerItem = product.DiscountedPrice.ToString().Replace(',', '.');
                orderObject.PromotionId = promotionId;
                orderObject.PromotionTitle = promotionTitle;
                orderObject.PromotionType = promotionType;
                orderObject.Vat = product.Vat.ToString();
#if Stamp
                orderObject.IsStampRelated = product.IsInStamp;
#endif
#if Loyalty
                orderObject.PointPerItem = product.IsInPromotion? 0 : product.LoyaltyPoints;
#endif
                orderObject.IsCanceled = false;
                orderList.Add(orderObject);
            }


            ParseObject order = new ParseObject(ParseConstants.ORDERS_CLASS_NAME);
            int earnedPoints = 0;
            int usedPoints = 0;
#if Loyalty
            //will be calculated from the panel
            earnedPoints = Cart.CalculateEarnedPoints();
            if (Cart.Instance.PointsUsed)
                usedPoints = Cart.Instance.UsedPoints;
#endif

            order[ParseConstants.ORDERS_ATTRIBUTE_ADDRESS] = address.Address;
            order[ParseConstants.ORDERS_ATTRIBUTE_ADDRESSDESC] = address.AddressDescription;
            order[ParseConstants.ORDERS_ATTRIBUTE_ADDRESSLINE3] = address.AddressLine3;
            order[ParseConstants.ORDERS_ATTRIBUTE_ADDRESSNOTE] = address.Note ?? "";
            order[ParseConstants.ORDERS_ATTRIBUTE_BRANCH] = BranchHelper.GetBranch(user.ActiveBranch).Name;
            order[ParseConstants.ORDERS_ATTRIBUTE_USERNAME] = ParseUser.CurrentUser.Get<string>("FirstName");
            order[ParseConstants.ORDERS_ATTRIBUTE_SURNAME] = ParseUser.CurrentUser.Get<string>("LastName");
            order[ParseConstants.ORDERS_ATTRIBUTE_PHONE] = address.PhoneNumber;
            order[ParseConstants.ORDERS_ATTRIBUTE_STORE] = user.ActiveBranch;
            order[ParseConstants.ORDERS_ATTRIBUTE_STATUS] = (int)OrderStatus.WAITING_CONFIRMATION;
            order[ParseConstants.ORDERS_ATTRIBUTE_USERID] = MyDevice.DeviceID;
            order[ParseConstants.ORDERS_ATTRIBUTE_ENDUSERID] = ParseUser.CurrentUser.ObjectId;
            order[ParseConstants.ORDERS_ATTRIBUTE_EARNEDPOINTS] = earnedPoints;
            order[ParseConstants.ORDERS_ATTRIBUTE_USEDPOINTS] = usedPoints;
            order[ParseConstants.ORDERS_ATTRIBUTE_NOTE] = Cart.Instance.Note;
            order[ParseConstants.ORDERS_ATTRIBUTE_FIRSTTIMEORDER] = !ParseUser.CurrentUser.Get<bool>("NewRewardUsed");
            order[ParseConstants.ORDERS_ATTRIBUTE_ORDERARRAY] = Newtonsoft.Json.JsonConvert.SerializeObject(orderList);
            order[ParseConstants.ORDERS_ATTRIBUTE_CHANGE] = change;
            order[ParseConstants.ORDERS_ATTRIBUTE_CALL] = shouldCall;
#if Laundry
            if(laundries != null && laundries.Any())
            { 
		        var laundryToPickup = laundries.Where(x => x[ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER] == null || (string) x[ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER] == "").Select(x => x.ObjectId).FirstOrDefault();
                var laundriesToDropOff = laundries.Where(x => x[ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER] != null && (string)x[ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER] != "").Select(x => x.ObjectId).ToList();
                order[ParseConstants.ORDERS_ATTRIBUTE_PICKEDUPSERVICEORDER] = laundryToPickup;
                order[ParseConstants.ORDERS_ATTRIBUTE_DROPOFFSERVICEORDER] = laundriesToDropOff.Any() ?  Newtonsoft.Json.JsonConvert.SerializeObject(laundriesToDropOff) : string.Empty;
            }
#endif
#if Stamp
            order[ParseConstants.ORDERS_ATTRIBUTE_STAMPREWARDS] = stamps != null && stamps.Any() ? Newtonsoft.Json.JsonConvert.SerializeObject(stamps.Select(x => x.ObjectId).ToList()) : string.Empty;
#endif

            bool bIsSucceeded = false;

            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 5000;
                try
                {
                    if (order == null)
                    {
                        break;
                    }
                    System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                    await order.SaveAsync(cancellation.Token);
#if Loyalty
                    //will be updated when the order is completed
                    //EndUser.DefaultInstance.UserPoints += earnedPoints;
                    //await EndUserModel.UpdateEndUser();
                    //MyDevice.Footer.UpdatePoints();

				    if (Cart.Instance.PointsUsed)
				    {
                        EndUser.DefaultInstance.UserPoints -= usedPoints;
                        await EndUserModel.UpdateEndUser();
                        MyDevice.Footer.UpdatePoints();
                    }
#endif
#if RewardNewUser
                    if (ParseUser.CurrentUser!=null&&!ParseUser.CurrentUser.Get<bool>("NewRewardUsed") && bIsCash)
				    {
                        System.Threading.CancellationTokenSource userCancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        ParseUser.CurrentUser["NewRewardUsed"] = true;
                        await ParseUser.CurrentUser.SaveAsync(userCancellation.Token);
                    }
#endif
                    if (!ParseUser.CurrentUser.ContainsKey("NewRewardUsed") || !ParseUser.CurrentUser.Get<bool>("NewRewardUsed"))
                    {
                        CancellationTokenSource userCancellation = new CancellationTokenSource(cancelTime);
                        ParseUser.CurrentUser["NewRewardUsed"] = true;
                        await ParseUser.CurrentUser.SaveAsync(userCancellation.Token);
                    }

                    bIsSucceeded = true;
                    break;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    Crashes.TrackError(e);
                }
            }

#if Laundry
            //update laundries with order id
		    await LaundryModel.UpdateLaundriesWithOrderId(user, order.ObjectId, order.CreatedAt);
            //delete all laundries
            Cart.Instance.ClearAllLaundries();
#endif
#if Stamp
            Cart.Instance.ClearAllStamps();
#endif
            return bIsSucceeded;
        }

        //0 for tracking 
        //1 for history
        public async static Task<Dictionary<int, List<object>>> GetOrdersForTrackingAndHistory()
        {
            var tempDictionary = new Dictionary<int, List<object>>();
            var statusClassList = new List<StatusClass>();
            var historyClassList = new List<HistoryClass>();
            string endUserID = "";
            if (ParseUser.CurrentUser != null)
                endUserID = ParseUser.CurrentUser.ObjectId;
            var orderQuery = ParseObject.GetQuery(ParseConstants.ORDERS_CLASS_NAME).
                WhereEqualTo(ParseConstants.ORDERS_ATTRIBUTE_ENDUSERID, endUserID).
                OrderByDescending("updatedAt");

            IEnumerable<ParseObject> orderObjects = null;

#if Stamp
            //get stamp rewards from remote
            var stampRewards = await StampRewardModel.FetchStampRewards();
#endif

            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 5000;
                try
                {
                    if (orderQuery != null)
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        orderObjects = await orderQuery.FindAsync(cancellation.Token);
                    }
                    break;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    Crashes.TrackError(e);
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance?.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                    System.Diagnostics.Debug.WriteLine("Toast from: GetOrdersForTrackingAndHistory " + e.Message);
                }
            }
            if (orderObjects == null)
                return tempDictionary;

            foreach (var order in orderObjects)
            {
                if (order != null)
                {
                    if (order.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_STATUS))
                    {
                        int status = order.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_STATUS);
                        //track
                        if (status < 3)
                        {
                            List<string> productOrderList = new List<string>();
                            List<string> historyProducts = new List<string>();
                            var orderString = order.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_ORDERARRAY) ? order.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_ORDERARRAY) : "";
                            var orderObjectList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderObject>>(orderString);

#if Stamp
                    string stampRewardsString = order.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_STAMPREWARDS) ? order.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_STAMPREWARDS) : string.Empty;
                    if (!string.IsNullOrWhiteSpace(stampRewardsString))
                    {
                        var stampRewardsArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(stampRewardsString);

                        foreach (var stampId in stampRewardsArray)
                        {
                            if (stampRewards.ContainsKey(stampId))
                                productOrderList.Add(stampRewards[stampId]);
                        }
                    }
#endif

                            int totalProductCount = 0;
                            foreach (var orderobject in orderObjectList)
                            {
                                string orderObjectString = "Quantity:" + orderobject.Quantity + ";" +
                                    "Product:" + orderobject.Product + ";" + "Description:" +
                                    orderobject.Description + ";" + "Price:" + orderobject.Price + ";" + "IsAvailable:" + orderobject.IsAvailable + ";" + "Barcode:" + orderobject.Barcode
                                    + ";" + "Quantity:" + orderobject.Quantity + ";" + "PricePerItem:" + orderobject.PricePerItem + ";" + "DiscountedPricePerItem:" + (string.IsNullOrWhiteSpace(orderobject.DiscountedPricePerItem) ? "0" : orderobject.DiscountedPricePerItem)
                                                                                    + ";" + "Vat:" + (string.IsNullOrWhiteSpace(orderobject.Vat) ? "0" : orderobject.Vat) + ";" + "IsCanceled:" + orderobject.IsCanceled;

                                productOrderList.Add(orderObjectString);
                                historyProducts.Add(orderobject.ProductId);
                                totalProductCount += Convert.ToInt32(orderobject.Quantity);

                            }

                            var branch = order.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_BRANCH);
                            var storeId = order.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_STORE);
                            var branchNumber = BranchHelper.BranchList.First(x => x.ID == storeId).PhoneNumber;
                            var deliveryStaffName = "";
                            var deliveryStaffPhone = "";
                            var dispatchDate = "";
                            var approveDate = "";
                            string estimatedDeliveryTime = "";
                            if (status >= (int)OrderStatus.CONFIRMED)
                            {
                                if (order.ContainsKey(ParseConstants.ORDERS_APPROVE_DATE))
                                {
                                    approveDate = (order?.Get<DateTime>(ParseConstants.ORDERS_APPROVE_DATE) - TimeSpan.FromHours(1)).ToString();
                                }
                            }

                            if (status == (int)OrderStatus.IN_TRANSIT)
                            {

                                if (order.ContainsKey(ParseConstants.ORDERS_DELIVERY_STAFF_ID))
                                {
                                    ParseObject deliveryStaff = await GetDeliveryStaff(order.Get<string>(ParseConstants.ORDERS_DELIVERY_STAFF_ID));
                                    if (deliveryStaff != null)
                                    {
                                        deliveryStaffName = deliveryStaff?.Get<string>(ParseConstants.DELIVERYSTAFF_NAME) + " " + deliveryStaff?.Get<string>(ParseConstants.DELIVERYSTAFF_SURNAME);
                                        deliveryStaffPhone = deliveryStaff?.Get<string>(ParseConstants.DELIVERYSTAFF_PHONE);
                                    }
                                }

                                if (order.ContainsKey(ParseConstants.ORDERS_DISPATCH_DATE))
                                {
                                    dispatchDate = (order?.Get<DateTime>(ParseConstants.ORDERS_DISPATCH_DATE) - TimeSpan.FromHours(1)).ToString();
                                }
                            }
                            var createdDate = order?.CreatedAt.ToString();
                            var updatedDate = order?.UpdatedAt.ToString();
                            var address = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_ADDRESS);
                            var addressDesc = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_ADDRESSDESC);
                            var addressLine3 = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_ADDRESSLINE3);
                            var name = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_USERNAME);
                            var surname = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_SURNAME);
                            var phone = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_PHONE);
                            var earnedPoints = order.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_EARNEDPOINTS);
                            var usedPoints = order.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_USEDPOINTS) ? order.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_USEDPOINTS) : 0;
#if Loyalty
                    var pointDiscount = usedPoints / MyDevice.PointsPerAed;
#else
                            var pointDiscount = 0;
#endif

                            var change = order.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_CHANGE);
                            order?.TryGetValue<string>(ParseConstants.ORDERS_ATTRIBUTE_ESTIMATEDDELIVERYTIME, out estimatedDeliveryTime);
                            statusClassList.Add(new StatusClass(order.ObjectId, productOrderList, historyProducts, address, addressDesc, addressLine3, name, surname, phone, createdDate,
                                branch, branchNumber, (OrderStatus)status, deliveryStaffName, deliveryStaffPhone, approveDate, dispatchDate, updatedDate, earnedPoints, estimatedDeliveryTime, totalProductCount, change, pointDiscount));
                        }
                        //history
                        else
                        {
                            List<string> productOrderList = new List<string>();
                            List<string> historyProducts = new List<string>();
                            var orderString = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_ORDERARRAY);
                            var orderObjectList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OrderObject>>(orderString);

#if Stamp
                    string stampRewardsString = order.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_STAMPREWARDS) ? order.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_STAMPREWARDS) : string.Empty;
                    if (!string.IsNullOrWhiteSpace(stampRewardsString))
                    {
                        var stampRewardsArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(stampRewardsString);
                        
                        foreach (var stampId in stampRewardsArray)
                        {
                            if (stampRewards.ContainsKey(stampId))
                                productOrderList.Add(stampRewards[stampId]);
                        }
                    }
#endif

                            int totalProductCount = 0;
                            foreach (var orderobject in orderObjectList)
                            {

                                string orderObjectString = "Quantity:" + orderobject.Quantity + ";" +
                                    "Product:" + orderobject.Product + ";" + "Description:" +
                                    orderobject.Description + ";" + "Price:" + orderobject.Price + ";" + "IsAvailable:" + orderobject.IsAvailable + ";" + "Barcode:" + orderobject.Barcode
                                    + ";" + "Quantity:" + orderobject.Quantity + ";" + "PricePerItem:" + orderobject.PricePerItem + ";" + "DiscountedPricePerItem:" + (string.IsNullOrWhiteSpace(orderobject.DiscountedPricePerItem) ? "0" : orderobject.DiscountedPricePerItem)
                                                                                    + ";" + "Vat:" + (string.IsNullOrWhiteSpace(orderobject.Vat) ? "0" : orderobject.Vat) + ";" + "IsCanceled:" + orderobject.IsCanceled;
                                productOrderList.Add(orderObjectString);
                                historyProducts.Add(orderobject.ProductId);
                                totalProductCount += Convert.ToInt32(orderobject.Quantity);
                            }
                            var createdDate = order?.CreatedAt.ToString();
                            var branch = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_BRANCH);
                            var date = order?.CreatedAt.ToString();
                            var address = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_ADDRESS);
                            var addressDesc = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_ADDRESSDESC);
                            var addressLine3 = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_ADDRESSLINE3);
                            var name = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_USERNAME);
                            var surname = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_SURNAME);
                            var phone = order?.Get<string>(ParseConstants.ORDERS_ATTRIBUTE_PHONE);
                            var earnedPoints = order.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_EARNEDPOINTS);
                            var usedPoints = order.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_USEDPOINTS) ? order.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_USEDPOINTS) : 0;
#if Loyalty
                    var pointDiscount = usedPoints / MyDevice.PointsPerAed;
#else
                            var pointDiscount = 0;
#endif
                            var updatedDate = order.UpdatedAt.ToString();

                            var approveDate = order.ContainsKey(ParseConstants.ORDERS_APPROVE_DATE) ? (order.Get<DateTime>(ParseConstants.ORDERS_APPROVE_DATE) - TimeSpan.FromHours(1)).ToString() : "";


                            DateTime dateTime;
                            order.TryGetValue<DateTime>(ParseConstants.ORDERS_FINISH_DATE, out dateTime);

                            string finishDate = "";
                            if (dateTime != null && dateTime.Year != 0001)
                            {
                                finishDate = (dateTime - TimeSpan.FromHours(1)).ToString();
                            }

                            var dispatchDate = order.ContainsKey(ParseConstants.ORDERS_DISPATCH_DATE) ? (order.Get<DateTime>(ParseConstants.ORDERS_DISPATCH_DATE) - TimeSpan.FromHours(1)).ToString() : "";
                            //var finishDate1 = order.ContainsKey(ParseConstants.ORDERS_FINISH_DATE) ? (order.Get<DateTime>(ParseConstants.ORDERS_FINISH_DATE) - TimeSpan.FromHours(1)).ToString() : "";



                            var change = order.ContainsKey(ParseConstants.ORDERS_ATTRIBUTE_CHANGE) ? order.Get<int>(ParseConstants.ORDERS_ATTRIBUTE_CHANGE) : 0;
                            historyClassList.Add(new HistoryClass(order.ObjectId, productOrderList, historyProducts, address, addressDesc, addressLine3, name, surname, phone, date, branch, earnedPoints, updatedDate, (OrderStatus)status, createdDate, approveDate, dispatchDate, totalProductCount, change, finishDate, pointDiscount));
                        }
                    }
                }
            }


            tempDictionary.Add(0, statusClassList?.Cast<object>().ToList());
            tempDictionary.Add(1, historyClassList?.Cast<object>().ToList());

#if Laundry
            //get laundries and add them to dictionary
		    await LaundryModel.GetLaundriesForTrackingAndHistory(tempDictionary);
#endif

            return tempDictionary;
        }

        private async static Task<ParseObject> GetDeliveryStaff(string DeliveryStaffName)
        {
            ParseObject deliveryStaffQuery = null;

            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 5000;
                try
                {
                    System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                    deliveryStaffQuery = await ParseObject.GetQuery(ParseConstants.DELIVERYSTAFF_CLASS_NAME).
                                                          WhereEqualTo(ParseConstants.DELIVERYSTAFF_NAME, DeliveryStaffName).FirstAsync(cancellation.Token);
                    break;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    Crashes.TrackError(e);
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                    System.Diagnostics.Debug.WriteLine("Toast from: GetDeliveryStaff " + e.Message);
                }
            }

            return deliveryStaffQuery;
        }


        public async static Task<double> GetDeliveredOrdersCount()
        {
            var tempDictionary = new Dictionary<int, List<object>>();
            var statusClassList = new List<StatusClass>();
            var historyClassList = new List<HistoryClass>();
            string endUserID = "";
            if (ParseUser.CurrentUser != null)
                endUserID = ParseUser.CurrentUser.ObjectId;
            var orderQuery = ParseObject.GetQuery(ParseConstants.ORDERS_CLASS_NAME).
                WhereEqualTo(ParseConstants.ORDERS_ATTRIBUTE_ENDUSERID, endUserID).
                WhereEqualTo(ParseConstants.ORDERS_ATTRIBUTE_STATUS, (int)OrderStatus.COMPLETED).
                OrderByDescending("updatedAt");

            IEnumerable<ParseObject> orderObjects = null;

#if Stamp
            //get stamp rewards from remote
            var stampRewards = await StampRewardModel.FetchStampRewards();
#endif

            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 5000;
                try
                {
                    if (orderQuery != null)
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        orderObjects = await orderQuery.FindAsync(cancellation.Token);
                    }
                    break;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    Crashes.TrackError(e);
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance?.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                    System.Diagnostics.Debug.WriteLine("Toast from: GetOrdersForTrackingAndHistory " + e.Message);
                }
            }
            if (orderObjects == null)
                return 0;


            return orderObjects.Count();
        }

    }
}

