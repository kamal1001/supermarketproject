﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using Microsoft.AppCenter.Crashes;
using Parse;
using Plugin.Connectivity;

namespace SupermarketProject.Models.Remote
{
    public static class AddressModel
    {
        public static async Task<bool> SendAddressToRemote(AddressClass address)
        {
            bool isSuccess = false;

            if (CrossConnectivity.Current.IsConnected)
            {
                ParseObject addressParseObject = new ParseObject(ParseConstants.AddressClassName);

                addressParseObject[ParseConstants.AddressAttributeUserId] = address.UserID;
                addressParseObject[ParseConstants.AddressAttributePhoneNumber] = address.PhoneNumber;
                addressParseObject[ParseConstants.AddressAttributeAddress] = address.Address;
                addressParseObject[ParseConstants.AddressAttributeAddressDescription] = address.AddressDescription;
                addressParseObject[ParseConstants.AddressAttributeAddressLine3] = address.AddressLine3;
                addressParseObject[ParseConstants.AddressAttributeIsActive] = address.IsActive;
                addressParseObject[ParseConstants.AddressAttributeBuildingId] = address.BuildingId;
                addressParseObject[ParseConstants.AddressAttributeApartmentNumber] = address.ApartmentNumber;
                addressParseObject[ParseConstants.AddressAttributeBranchId] = address.BranchId;
                addressParseObject[ParseConstants.AddressAttributeNote] = address.Note;

                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 10000;
                    try
                    {
                        if (addressParseObject != null)
                        {
                            System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                            await addressParseObject.SaveAsync(cancellation.Token);
                            //var a = addressParseObject.ObjectId;
                            address.Id = addressParseObject.ObjectId;

                            AddressClassFunctions.AddAddress(address);

                            isSuccess = true;
                        }
                        break;
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex);
                        System.Diagnostics.Debug.WriteLine(ex.ToString());
                        //string message = MyDevice.InternetErrorMessage1;
                        //Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                    }
                }
                /*if (!test) 
                {
                    int cancelTime = 3000;
                    string message = "We couldn't send your suggestion. Please check your internet connection.";
                    Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));

                }*/
            }
            return isSuccess;
        }

        public static async Task<bool> FetchAddressesAndSaveItToLocal()
        {
            bool isSuccess = false;

            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    var addressQuery = ParseObject.GetQuery(ParseConstants.AddressClassName).WhereEqualTo(ParseConstants.AddressAttributeUserId, ParseUser.CurrentUser?.ObjectId)?.WhereExists(ParseConstants.AddressAttributeBranchId);
                    IEnumerable<ParseObject> addressObjects = null;
                    var addressList = new List<AddressClass>();
                    for (int t = 0; t < 3; t++)
                    {
                        int cancelTime = 15000;
                        try
                        {
                            System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                            addressObjects = await addressQuery?.FindAsync(cancellation.Token);
                            isSuccess = true;
                            break;
                        }
                        catch (Exception ex)
                        {
                            Crashes.TrackError(ex);
                            System.Diagnostics.Debug.WriteLine(ex.ToString());

                            //string message = MyDevice.InternetErrorMessage1;
                            //Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                            //System.Diagnostics.Debug.WriteLine("Toast from: GetDeliveryStaff " + e.Message);
                        }
                    }

                    foreach (var addressObject in addressObjects)
                    {
                        addressList.Add(new AddressClass
                        {
                            Address = addressObject.ContainsKey(ParseConstants.AddressAttributeAddress) ? addressObject.Get<string>(ParseConstants.AddressAttributeAddress) : string.Empty,
                            AddressDescription = addressObject.ContainsKey(ParseConstants.AddressAttributeAddressDescription) ? addressObject.Get<string>(ParseConstants.AddressAttributeAddressDescription) : string.Empty,
                            AddressLine3 = addressObject.ContainsKey(ParseConstants.AddressAttributeAddressLine3) ? addressObject.Get<string>(ParseConstants.AddressAttributeAddressLine3) : string.Empty,
                            Id = addressObject.ObjectId,
                            PhoneNumber = addressObject.ContainsKey(ParseConstants.AddressAttributePhoneNumber) ? addressObject.Get<string>(ParseConstants.AddressAttributePhoneNumber) : string.Empty,
                            UserID = ParseUser.CurrentUser?.ObjectId,
                            BuildingId = addressObject.ContainsKey(ParseConstants.AddressAttributeBuildingId) ? addressObject.Get<string>(ParseConstants.AddressAttributeBuildingId) : string.Empty,
                            ApartmentNumber = addressObject.ContainsKey(ParseConstants.AddressAttributeApartmentNumber) ? addressObject.Get<string>(ParseConstants.AddressAttributeApartmentNumber) : string.Empty,
                            BranchId = addressObject.ContainsKey(ParseConstants.AddressAttributeBranchId) ? addressObject.Get<int>(ParseConstants.AddressAttributeBranchId) : 0,
                            Note = addressObject.ContainsKey(ParseConstants.AddressAttributeNote) ? addressObject.Get<string>(ParseConstants.AddressAttributeNote) : string.Empty,
                            IsDeleted = addressObject.ContainsKey("IsDeleted") ? addressObject.Get<bool>("IsDeleted") : false
                        });
                    }

                    if (addressList?.Count > 0)
                    {
                        AddressClassFunctions.AddAddressList(addressList);
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                System.Diagnostics.Debug.WriteLine(ex);
            }

            return isSuccess;
        }

        public static async Task<bool> DeleteAddress(AddressClass address)
        {
            bool isSuccess = false;

            if (CrossConnectivity.Current.IsConnected)
            {
                ParseObject addressObject;

                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 15000;
                    try
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        addressObject = await ParseObject.GetQuery(ParseConstants.AddressClassName).WhereEqualTo(ParseConstants.ObjectId, address.Id)?.FirstOrDefaultAsync(cancellation.Token);
                        cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        if (addressObject != null)
                        {
                            await addressObject.DeleteAsync(cancellation.Token);
                            isSuccess = true;
                        }
                        break;
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex);
                        System.Diagnostics.Debug.WriteLine(ex.ToString());

                        //string message = MyDevice.InternetErrorMessage1;
                        //Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                        //System.Diagnostics.Debug.WriteLine("Toast from: GetDeliveryStaff " + e.Message);
                    }
                }
            }

            return isSuccess;
        }
        public static async Task<bool> UpdatePhoneNumber(string newPhoneNumber)
        {
            bool isSuccess = false;

            if (CrossConnectivity.Current.IsConnected)
            {
                var addressQuery = ParseObject.GetQuery(ParseConstants.AddressClassName).WhereEqualTo(ParseConstants.AddressAttributeUserId, ParseUser.CurrentUser?.ObjectId)?.WhereExists(ParseConstants.AddressAttributeBranchId);
                IEnumerable<ParseObject> addressObjects = null;
                var addressList = new List<AddressClass>();
                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 15000;
                    try
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        addressObjects = await addressQuery?.FindAsync(cancellation.Token);
                        isSuccess = true;
                        break;
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex);
                        System.Diagnostics.Debug.WriteLine(ex.ToString());

                        //string message = MyDevice.InternetErrorMessage1; 
                        //Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime)); 
                        //System.Diagnostics.Debug.WriteLine("Toast from: GetDeliveryStaff " + e.Message); 
                    }
                }
                foreach (var addressObject in addressObjects)
                {
                    addressObject[ParseConstants.AddressAttributePhoneNumber] = newPhoneNumber;
                    await addressObject.SaveAsync();

                    isSuccess = true;
                }
            }

            return isSuccess;
        }

        public static async Task<bool> UpdateAddress(AddressClass address)
        {
            bool isSuccess = false;

            if (CrossConnectivity.Current.IsConnected)
            {
                ParseObject addressObject;

                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 15000;
                    try
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        addressObject = await ParseObject.GetQuery(ParseConstants.AddressClassName).WhereEqualTo(ParseConstants.ObjectId, address.Id).FirstOrDefaultAsync(cancellation.Token);
                        addressObject[ParseConstants.AddressAttributeUserId] = address.UserID;
                        addressObject[ParseConstants.AddressAttributePhoneNumber] = address.PhoneNumber;
                        addressObject[ParseConstants.AddressAttributeAddress] = address.Address;
                        addressObject[ParseConstants.AddressAttributeAddressDescription] = address.AddressDescription;
                        addressObject[ParseConstants.AddressAttributeAddressLine3] = address.AddressLine3;
                        addressObject[ParseConstants.AddressAttributeIsActive] = address.IsActive;
                        addressObject[ParseConstants.AddressAttributeBuildingId] = address.BuildingId;
                        addressObject[ParseConstants.AddressAttributeApartmentNumber] = address.ApartmentNumber;
                        addressObject[ParseConstants.AddressAttributeBranchId] = address.BranchId;
                        addressObject[ParseConstants.AddressAttributeNote] = address.Note;
                        cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        await addressObject?.SaveAsync(cancellation.Token);
                        isSuccess = true;
                        AddressClassFunctions.UpdateAddress(address);
                        break;
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex);
                        System.Diagnostics.Debug.WriteLine(ex.ToString());

                        //string message = MyDevice.InternetErrorMessage1;
                        //Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                        //System.Diagnostics.Debug.WriteLine("Toast from: GetDeliveryStaff " + e.Message);
                    }
                }
            }
            return isSuccess;
        }
    }
}
