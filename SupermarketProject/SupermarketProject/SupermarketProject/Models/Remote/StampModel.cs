﻿using System;
using SupermarketProject.Common.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Parse;
using SupermarketProject.Models.Local;
using System.Linq;
using SupermarketProject.Common.Objects;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Models.Remote
{
    public class StampModel
    {
        public static List<StampClass> StampList = new List<StampClass>();
        public static async Task FetchStamps()
        {
            DateTime? localUpdate = UserClassFunctions.GetStampsUpdatedDateFromUser();
            DateTime? remoteUpdate = null;
            int queryLimit = 1000;
            var stampQuery = ParseObject.GetQuery(ParseConstants.STAMPS_CLASS_NAME).OrderBy(ParseConstants.STAMPS_ATTRIBUTE_TITLE).WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate).Limit(queryLimit);

            IEnumerable<ParseObject> stampObjects = null;

            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 5000;
                try
                {
                    if (stampQuery != null)
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        stampObjects = await stampQuery.FindAsync(cancellation.Token);
                    }
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                    System.Diagnostics.Debug.WriteLine("Toast from: FetchStamps " + e.Message);
                }
            }

            if (stampObjects != null)
            {
                var enumerable = stampObjects as ParseObject[] ?? stampObjects.ToArray();
                if (!enumerable.Any())
                {
                    PopulateStamps();
                    return;
                }

                List<StampClass> stamps = new List<StampClass>();
                foreach (var stampObject in enumerable)
                {
                    bool isDeleted = stampObject.Get<bool>("IsDeleted");

                    StampClass stamp = new StampClass();
                    stamp.objectId = stampObject.ObjectId;
                    stamp.Title = stampObject.Get<string>(ParseConstants.STAMPS_ATTRIBUTE_TITLE);
                    stamp.Description = stampObject.Get<string>(ParseConstants.STAMPS_ATTRIBUTE_DESCRIPTION);
                    stamp.ImageId = stampObject.Get<string>(ParseConstants.STAMPS_ATTRIBUTE_IMAGEID);
                    stamp.Count = stampObject.Get<int>(ParseConstants.STAMPS_ATTRIBUTE_COUNT);
                    stamp.StampType = stampObject.Get<string>(ParseConstants.STAMPS_ATTRIBUTE_STAMPTYPE);


                    var varList = stampObject.Get<IEnumerable<object>>(ParseConstants.STAMPS_ATTRIBUTE_STAMPVARIABLES).Cast<String>().ToList();
                    foreach (string variable in varList)
                    {
                        stamp.StampVariables += variable;

                        if (variable != varList.Last())
                            stamp.StampVariables += ",";
                    }

                    if (isDeleted)
                    {
                        StampClassFunctions.RemoveStamp(stampObject.ObjectId);
                    }
                    else
                    {
                        stamps.Add(stamp);
                    }
                    if (remoteUpdate == null || stampObject.UpdatedAt > remoteUpdate)
                        remoteUpdate = stampObject.UpdatedAt;
                }
                StampClassFunctions.AddStamp(stamps);
            }


            UserClassFunctions.AddStampsUpdateDateToUser(remoteUpdate);
            PopulateStamps();


        }

        private static void PopulateStamps()
        {
            StampList = StampClassFunctions.GetStamps();

            foreach (StampClass stampClass in StampList)
            {
                //mark stamp related products
                var stampProducts = GetStampProducts(stampClass);
                foreach (Product stampProduct in stampProducts)
                {
                    stampProduct.IsInStamp = true;
                    stampProduct.ProductRelatedStamps.Add(stampClass);
                }
            }
        }

        public static StampClass GetStamp(string stampId)
        {
            return StampList.Find(x => x.objectId == stampId);
        }

        public static List<Product> GetStampProducts(StampClass stamp)
        {
            List<Product> products = new List<Product>();

            var stampVars = stamp.StampVariables.Split(',').ToList();
            foreach (string stampVar in stampVars)
            {
                switch (stamp.StampType)
                {
                    case "1":
                        products.AddRange(ProductModel.ProductDictionary.Values.Where(x => x.RealCategoryID == stampVar));
                        break;
                    case "2":
                        var product = ProductModel.ProductDictionary.Values.FirstOrDefault(x => x.ProductID == stampVar);
                        if (product != null && product.ProductID == stampVar)
                            products.Add(product);
                        break;
                }
            }


            return products;
        }

    }
}
