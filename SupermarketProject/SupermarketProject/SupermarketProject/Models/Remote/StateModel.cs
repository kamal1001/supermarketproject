﻿using System;
using SupermarketProject.Common.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Parse;
using SupermarketProject.Models.Local;
using System.Linq;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;

namespace SupermarketProject.Models.Remote
{
    public class StateModel
    {
        public static List<StateClass> StateList;
        public static async Task FetchStates()
        {
            DateTime? localUpdate = UserClassFunctions.GetStatesUpdatedDateFromUser();
            DateTime? remoteUpdate = null;
            int queryLimit = 1000;
            var stateQuery = ParseObject.GetQuery("State").OrderBy("StateName").WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate).Limit(queryLimit);

            IEnumerable<ParseObject> stateObjects = null;

            if (CrossConnectivity.Current.IsConnected)
            {
                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 5000;
                    try
                    {
                        if (stateQuery != null)
                        {
                            System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                            stateObjects = await stateQuery.FindAsync(cancellation.Token);
                        }
                        break;
                    }
                    catch (Exception e)
                    {
                        Crashes.TrackError(e);
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                        string message = MyDevice.InternetErrorMessage1;
                        Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                        System.Diagnostics.Debug.WriteLine("Toast from: FetchStates " + e.Message);
                    }
                }
            }

            if (stateObjects != null)
            {
                var enumerable = stateObjects as ParseObject[] ?? stateObjects.ToArray();
                if (!enumerable.Any())
                {
                    StateList = StateClassFunctions.GetStates();
                    return;
                }

                List<StateClass> stateList = new List<StateClass>();
                foreach (var stateObject in enumerable)
                {
                    StateClass state = new StateClass();
                    state.objectId = stateObject.ObjectId;
                    state.StateName = stateObject.Get<string>("StateName");
                    state.IsDeleted = stateObject.ContainsKey("IsDeleted") && stateObject.Get<bool>("IsDeleted");

                    stateList.Add(state);
                    if (remoteUpdate == null || stateObject.UpdatedAt > remoteUpdate)
                        remoteUpdate = stateObject.UpdatedAt;
                }
                StateClassFunctions.AddState(stateList);
            }
            if (remoteUpdate != null)
                UserClassFunctions.AddStatesUpdateDateToUser(remoteUpdate);
            StateList = StateClassFunctions.GetStates();
        }

    }
}
