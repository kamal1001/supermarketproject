﻿using System;
using SupermarketProject.Common.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Parse;
using SupermarketProject.Models.Local;
using System.Linq;
using SupermarketProject.Common.Objects;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Models.Remote
{
    public static class CategoryModel
    {
        public static Dictionary<string, Category> CategoryDictionary;
        public static List<Category> ParentCategoryList;
#if CategoryLevel3
        public static List<Category> MainCategoryList;
#endif

        static CategoryModel()
        {
            InitializeMemberVariables();
        }

        private static void InitializeMemberVariables()
        {
            CategoryDictionary = new Dictionary<string, Category>(StringComparer.Ordinal);
            ParentCategoryList = new List<Category>();
#if CategoryLevel3
            MainCategoryList = new List<Category>();
#endif
        }

        public static void PopulateCategoryDictionaries()
        {
            foreach (CategoryClass categoryObj in CategoryClassFunctions.GetCategories())
            {
                List<string> subCategoryList = new List<string>();
                if (categoryObj.Sub != null)
                    subCategoryList = categoryObj.Sub.Split(',')?.ToList<string>();

                string EnviorementImagePath = MyDevice.BrandFolderName + ".SavedImages." + categoryObj.ImageID + ".jpg";
                string ImagePath = ImageModel.mRootFolderPath + "/" + ParseConstants.IMAGE_FOLDER_NAME + "/" + categoryObj.ImageID + ".jpg";
#if CategoryLevel3
                Category tempCategory = new Category(categoryObj.Name, categoryObj.ImageName + ".jpg", ImagePath, EnviorementImagePath, categoryObj.isInTheList, categoryObj.isSubCategory, categoryObj.objectId, subCategoryList, categoryObj.ParentCategory, priority:categoryObj.Priority);

                if (String.IsNullOrWhiteSpace(categoryObj.ParentCategory) && !categoryObj.isSubCategory)
                {
                    MainCategoryList.Add(tempCategory);
                }
                else 
                {
                    CategoryDictionary.Add(tempCategory.CategoryID,tempCategory);
                    if (!categoryObj.isSubCategory)
                        ParentCategoryList.Add(tempCategory);
                }
#else
#if Loyalty
                Category tempCategory = new Category(categoryObj.Name, categoryObj.ImageName + ".jpg", ImagePath, EnviorementImagePath, categoryObj.isInTheList, categoryObj.isSubCategory, categoryObj.objectId, subCategoryList, Convert.ToDecimal(categoryObj.LoyaltyRatio), priority:categoryObj.Priority);
#else
                Category tempCategory = new Category(categoryObj.Name, categoryObj.ImageID + ".jpg", ImagePath, EnviorementImagePath, categoryObj.isInTheList, categoryObj.isSubCategory, categoryObj.objectId, subCategoryList, priority: categoryObj.Priority);
#endif

                if (!CategoryDictionary.ContainsKey(tempCategory.CategoryID))
                {
                    CategoryDictionary.Add(tempCategory.CategoryID, tempCategory);
                    if (!categoryObj.isSubCategory)
                        ParentCategoryList.Add(tempCategory);
                }
#endif
            }
        }

        public static async Task FetchCategories(LoadingPage loadingPage)
        {
            DateTime? localUpdate = UserClassFunctions.GetCategoriesUpdatedDateFromUser();
            var categoryQuery = ParseObject.GetQuery(ParseConstants.CATEGORIES_CLASS_NAME).
                WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate).
                OrderBy(ParseConstants.CATEGORY_ATTRIBUTE_NAME);

            categoryQuery = categoryQuery.Select(ParseConstants.CATEGORY_ATTRIBUTE_NAME)
                                         .Select(ParseConstants.CATEGORY_ATTRIBUTE_IMAGEID)
                                         .Select(ParseConstants.CATEGORY_ATTRIBUTE_PRIORITY)
                                         .Select(ParseConstants.CATEGORY_ATTRIBUTE_ISSUBCATEGORYNAME)
                                         .Select(ParseConstants.CATEGORY_ATTRIBUTE_ISINTHELIST)
#if Loyalty
                                         .Select(ParseConstants.CATEGORY_ATTRIBUTE_LOYALTYRATIO)
#endif
#if CategoryLevel3
                                         .Select(ParseConstants.CATEGORY_ATTRIBUTE_PARENTCATEGORY)
#endif
                                         .Select(ParseConstants.CATEGORY_ATTRIBUTE_SUB);


            int queryLimit = 1000;
            int j = 0;
            DateTime? remoteUpdate = null;
            List<CategoryClass> tempList = new List<CategoryClass>();
            bool anyErrorOccured = false;
            int i = 0;

            while (true)
            {
                IEnumerable<ParseObject> categoryObjects = null;
                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 15000;
                    try
                    {
                        if (categoryQuery != null)
                        {
                            System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                            categoryObjects = await categoryQuery.Limit(queryLimit).Skip(i).FindAsync(cancellation.Token);
                        }
                        break;
                    }
                    catch (Exception e)
                    {
                        Crashes.TrackError(e);
                        string message = MyDevice.InternetErrorMessage1;
                        Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                        System.Diagnostics.Debug.WriteLine("Toast from: FetchCategories " + e.ToString());
                        if (t == 2)
                            anyErrorOccured = true;
                    }
                }
                if (categoryObjects == null || categoryObjects.Count() == 0)
                    break;
                foreach (var categoryObject in categoryObjects)
                {
                    CategoryClass tempCategory = new CategoryClass();
                    tempCategory.objectId = categoryObject.ObjectId;
                    tempCategory.Name = categoryObject.Get<string>(ParseConstants.CATEGORY_ATTRIBUTE_NAME);
                    tempCategory.ImageID = categoryObject.Get<string>(ParseConstants.CATEGORY_ATTRIBUTE_IMAGEID);
                    tempCategory.Priority = categoryObject.Get<int>(ParseConstants.CATEGORY_ATTRIBUTE_PRIORITY);
                    tempCategory.isSubCategory = categoryObject.Get<bool>(ParseConstants.CATEGORY_ATTRIBUTE_ISSUBCATEGORYNAME);
                    tempCategory.isInTheList = categoryObject.Get<bool>(ParseConstants.CATEGORY_ATTRIBUTE_ISINTHELIST);
#if Loyalty

                    int loyaltyRatio = categoryObject.ContainsKey(ParseConstants.CATEGORY_ATTRIBUTE_LOYALTYRATIO)
                        ? categoryObject.Get<int>(ParseConstants.CATEGORY_ATTRIBUTE_LOYALTYRATIO) : -1;
                    tempCategory.LoyaltyRatio = loyaltyRatio.ToString();
#endif
#if CategoryLevel3
                    string parentCategory = "";
                    categoryObject.TryGetValue<string>(ParseConstants.CATEGORY_ATTRIBUTE_PARENTCATEGORY,out parentCategory);
                    tempCategory.ParentCategory = parentCategory;
#endif
                    var subcategoryList = categoryObject.Get<IEnumerable<object>>(ParseConstants.CATEGORY_ATTRIBUTE_SUB).Cast<string>().ToList();

                    foreach (string sub in subcategoryList)
                    {
                        tempCategory.Sub += sub;
                        if (sub != subcategoryList.Last())
                            tempCategory.Sub += ",";
                    }

                    tempList.Add(tempCategory);
                    if (remoteUpdate == null || categoryObject.UpdatedAt > remoteUpdate)
                        remoteUpdate = categoryObject.UpdatedAt;
                    j++;
                    if (j % 50 == 0)
                    {
                        double scrollPos = 0.5f + i * 0.2f / 200;
                        loadingPage.ProgressBar1.Progress = scrollPos;
                    }
                }
                if (categoryObjects.Count() < queryLimit)
                    break;
                else
                    i += queryLimit;
            }
            if (tempList.Count > 0)
            {
                CategoryClassFunctions.AddCategory(tempList);
                if (remoteUpdate != null && !anyErrorOccured)
                    UserClassFunctions.AddCategoriesUpdateDateToUser(remoteUpdate);
            }
        }
    }
}

