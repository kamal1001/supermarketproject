﻿using System;
using SupermarketProject.Common.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Parse;
using SupermarketProject.Models.Local;
using System.Linq;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;

namespace SupermarketProject.Models.Remote
{
    public class AreaModel
    {
        public static List<AreaClass> AreaList;
        public static async Task FetchAreas()
        {
            DateTime? localUpdate = UserClassFunctions.GetAreasUpdatedDateFromUser();
            DateTime? remoteUpdate = null;
            int queryLimit = 1000;
            var areaQuery = ParseObject.GetQuery("Area").OrderBy("AreaName").WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate).Limit(queryLimit);

            IEnumerable<ParseObject> areaObjects = null;

            if (CrossConnectivity.Current.IsConnected)
            {
                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 5000;
                    try
                    {
                        if (areaQuery != null)
                        {
                            System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                            areaObjects = await areaQuery.FindAsync(cancellation.Token);
                        }
                        break;
                    }
                    catch (Exception e)
                    {
                        Crashes.TrackError(e);
                        string message = MyDevice.InternetErrorMessage1;
                        Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                        System.Diagnostics.Debug.WriteLine("Toast from: FetchAreas " + e.ToString());
                    }
                }
            }

            if (areaObjects != null)
            {
                var enumerable = areaObjects as ParseObject[] ?? areaObjects.ToArray();
                if (!enumerable.Any())
                {
                    AreaList = AreaClassFunctions.GetAreas();
                    return;
                }

                List<AreaClass> areaList = new List<AreaClass>();
                foreach (var areaObject in enumerable)
                {
                    AreaClass area = new AreaClass();
                    area.objectId = areaObject.ObjectId;
                    area.AreaName = areaObject.Get<string>("AreaName");
                    area.StateId = areaObject.Get<string>("StateId");
                    area.IsDeleted = areaObject.ContainsKey("IsDeleted") && areaObject.Get<bool>("IsDeleted");

                    areaList.Add(area);
                    if (remoteUpdate == null || areaObject.UpdatedAt > remoteUpdate)
                        remoteUpdate = areaObject.UpdatedAt;
                }
                AreaClassFunctions.AddArea(areaList);
            }
            if (remoteUpdate != null)
                UserClassFunctions.AddAreasUpdateDateToUser(remoteUpdate);
            AreaList = AreaClassFunctions.GetAreas();
        }

        public static AreaClass GetArea(string areaId)
        {
            return AreaList.Find(x => x.objectId == areaId);
        }

    }
}
