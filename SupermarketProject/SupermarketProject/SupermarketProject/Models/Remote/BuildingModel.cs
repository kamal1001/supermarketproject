﻿using System;
using SupermarketProject.Common.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Parse;
using SupermarketProject.Models.Local;
using System.Linq;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;

namespace SupermarketProject.Models.Remote
{
    public class BuildingModel
    {
        public static List<BuildingClass> BuildingList;
        public static async Task FetchBuildings()
        {
            DateTime? localUpdate = UserClassFunctions.GetBuildingsUpdatedDateFromUser();
            DateTime? remoteUpdate = null;
            int queryLimit = 1000;
            int i = 0;
            var buildingQuery = ParseObject.GetQuery("Building").OrderBy("Index").WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate).Limit(queryLimit);

            IEnumerable<ParseObject> buildingObjects = null;
            List<ParseObject> allObjects = new List<ParseObject>();

            if (CrossConnectivity.Current.IsConnected)
            {
                while (true)
                {
                    for (int t = 0; t < 3; t++)
                    {
                        int cancelTime = 5000;
                        try
                        {
                            if (buildingQuery != null)
                            {
                                System.Threading.CancellationTokenSource cancellation =
                                    new System.Threading.CancellationTokenSource(cancelTime);
                                buildingObjects = await buildingQuery.Limit(queryLimit).Skip(i).FindAsync(cancellation.Token);
                            }
                            break;
                        }
                        catch (Exception e)
                        {
                            Crashes.TrackError(e);
                            string message = MyDevice.InternetErrorMessage1;
                            Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message)
                                .SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White)
                                .SetDuration(cancelTime));
                            System.Diagnostics.Debug.WriteLine("Toast from: FetchBuildings " + e.ToString());
                        }
                    }

                    if (buildingObjects == null)
                        break;

                    var collection = buildingObjects as ParseObject[] ?? buildingObjects.ToArray();

                    allObjects.AddRange(collection);

                    if (collection.Count() < queryLimit)
                        break;

                    i += queryLimit;
                }
            }

            if (allObjects.Any())
            {
                List<BuildingClass> buildingList = new List<BuildingClass>();
                foreach (var buildingObject in allObjects)
                {
                    BuildingClass building = new BuildingClass
                    {
                        objectId = buildingObject.ObjectId,
                        BuildingName = buildingObject.ContainsKey("BuildingName") ? buildingObject.Get<string>("BuildingName") : "N/A",
                        AreaId = buildingObject.ContainsKey("AreaId") ? buildingObject.Get<string>("AreaId") : "N/A",
                        Index = buildingObject.ContainsKey("Index") ? buildingObject.Get<int>("Index") : 0,
                        IsCluster = buildingObject.ContainsKey("IsCluster") && buildingObject.Get<bool>("IsCluster"),
                        IsDeleted = buildingObject.ContainsKey("IsDeleted") && buildingObject.Get<bool>("IsDeleted")
                    };

                    buildingList?.Add(building);
                    if (remoteUpdate == null || buildingObject.UpdatedAt > remoteUpdate)
                        remoteUpdate = buildingObject.UpdatedAt;
                }
                BuildingClassFunctions.AddBuilding(buildingList);
            }
            else
            {
                BuildingList = BuildingClassFunctions.GetBuildings();
                return;
            }

            if (remoteUpdate != null)
                UserClassFunctions.AddBuildingsUpdateDateToUser(remoteUpdate);
            BuildingList = BuildingClassFunctions.GetBuildings();
        }

    }
}
