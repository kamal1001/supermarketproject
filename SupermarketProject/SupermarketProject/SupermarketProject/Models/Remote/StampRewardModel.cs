﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using SupermarketProject.Common.Objects;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using Parse;
using Microsoft.AppCenter.Crashes;

#if Stamp
namespace SupermarketProject.Models.Remote
{
	public class StampRewardModel
	{

        public static async Task<List<ParseObject>> SendStampRewardsToRemote()
        {
            bool bIsSucceeded = false;
            var stampOrders = new List<ParseObject>();

            foreach (StampRewardClass stampRewardClass in Cart.CartStamps)
            {

                ParseObject stampRewardObject = new ParseObject(ParseConstants.STAMPREWARDS_CLASS_NAME);

                stampRewardObject[ParseConstants.STAMPREWARD_ATTRIBUTE_USER] = ParseUser.CurrentUser;

                ParseObject stampObject = new ParseObject(ParseConstants.STAMPS_CLASS_NAME);
                stampObject.ObjectId = stampRewardClass.StampId;
                stampRewardObject[ParseConstants.STAMPREWARD_ATTRIBUTE_STAMP] = stampObject;
                ParseObject productObject = new ParseObject(ParseConstants.PRODUCTS_CLASS_NAME);
                productObject.ObjectId = stampRewardClass.ProductId;
                stampRewardObject[ParseConstants.STAMPREWARD_ATTRIBUTE_PRODUCT] = productObject;

                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 5000;
                    try
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        await stampRewardObject.SaveAsync(cancellation.Token);

                        bIsSucceeded = true;
                        break;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                        Crashes.TrackError(e);
                        Debug.WriteLine("Error sending stamp rewards : " + e.Message);
                    }
                }

                if (!bIsSucceeded)
                    break;

                stampOrders.Add(stampRewardObject);

                //reset user stamp
                await UserStampModel.AddUserStamp(stampRewardClass.StampId, 0);
            }

            return bIsSucceeded ? stampOrders : null;

        }

        public static async Task<Dictionary<string,string>> FetchStampRewards()
        {                 
                       
            var query = ParseObject.GetQuery(ParseConstants.STAMPREWARDS_CLASS_NAME).Include("_User").Include("Product").
                WhereEqualTo(ParseConstants.STAMPREWARD_ATTRIBUTE_USER, ParseUser.CurrentUser);

            Dictionary<string,string> tempList = new Dictionary<string, string>();

            IEnumerable<ParseObject> stampRewardObjects = null;
            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 15000;
                try
                {
                    System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                    stampRewardObjects = await query.FindAsync(cancellation.Token);                    
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                }
            }            


            foreach (var stampRewardObject in stampRewardObjects)
            {
                var stampProduct = stampRewardObject.Get<ParseObject>(ParseConstants.STAMPREWARD_ATTRIBUTE_PRODUCT);
                string orderObjectString = "Quantity:" + "1" + ";" +
                             "Product:" + stampProduct.Get<string>(ParseConstants.PRODUCT_ATTRIBUTE_NAME) + ";" + "Description:" +
                            stampProduct.Get<string>(ParseConstants.PRODUCT_ATTRIBUTE_QUANTITY) + ";" + "Price:" + "0" + ";" + "IsAvailable:" + true + ";" + "Barcode:" + stampProduct.Get<string>(ParseConstants.PRODUCT_ATTRIBUTE_BARCODE);

                tempList.Add(stampRewardObject.ObjectId, orderObjectString);
            }

            return tempList;
        }


    }
}
#endif
