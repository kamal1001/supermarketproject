﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using SupermarketProject.Common.Objects;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using SupermarketProject.Models.Local.Helpers;
using Parse;
using Microsoft.AppCenter.Crashes;

#if Laundry

namespace SupermarketProject.Models.Remote
{
    public class LaundryDetail
    {
        public string ServiceName { get; set; }
        public decimal Price { get; set; }
        public string DocketNumber { get; set; }
    }

    public static class LaundryModel
    {
        public enum LaundryStatus { RECEIVED, PICKUP_ON_THE_WAY, IN_PROCESS, READY, DROP_OFF_REQUESTED, DROP_OFF_ON_THE_WAY, COMPLETED };

        private static List<ParseObject> _laundyOrders;

        public static async Task<List<ParseObject>> SendLaundryOrdersToRemote(UserClass user)
        {
            bool bIsSucceeded = false;
            _laundyOrders = new List<ParseObject>();

            foreach (LaundryClass laundryClass in Cart.CartLaundries)
            {
                AddressClass address = AddressClassFunctions.GetActiveAddress();

                
                ParseObject laundry;
                if (!string.IsNullOrEmpty(laundryClass.ObjectId))
                {
                    laundry = await GetLaundry(laundryClass.ObjectId);
                }
                else
                {
                    laundry = new ParseObject(ParseConstants.LAUNDRY_CLASS_NAME);
                }
               
                laundry[ParseConstants.LAUNDRY_ATTRIBUTE_DROPOFFORDER] = string.IsNullOrEmpty(laundryClass.DropOffOrder) ? "" : laundryClass.DropOffOrder;
                laundry[ParseConstants.LAUNDRY_ATTRIBUTE_ENDUSERID] = ParseUser.CurrentUser.ObjectId;
                //laundry[ParseConstants.LAUNDRY_ATTRIBUTE_FINISHDATE] = laundryClass.FinishDate;
                laundry[ParseConstants.LAUNDRY_ATTRIBUTE_NAME] = ParseUser.CurrentUser.Get<string>("FirstName");
                laundry[ParseConstants.LAUNDRY_ATTRIBUTE_PHONE] = address.PhoneNumber;
                laundry[ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER] = string.IsNullOrEmpty(laundryClass.PickedUpOrder) ? "" : laundryClass.PickedUpOrder;
                //laundry[ParseConstants.LAUNDRY_ATTRIBUTE_READYDATE] = laundryClass.ReadyDate;
                laundry[ParseConstants.LAUNDRY_ATTRIBUTE_STATUS] = laundryClass.Status;
                laundry[ParseConstants.LAUNDRY_ATTRIBUTE_STORE] = user.ActiveBranch;
                laundry[ParseConstants.LAUNDRY_ATTRIBUTE_SURNAME] = ParseUser.CurrentUser.Get<string>("LastName");

                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 5000;
                    try
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        await laundry.SaveAsync(cancellation.Token);

                        bIsSucceeded = true;
                        break;
                    }
                    catch (Exception e)
                    {
                        Crashes.TrackError(e);
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                        Debug.WriteLine("Error sending laundry orders : " + e.Message);
                    }
                }

                if(!bIsSucceeded)
                    break;
                
                _laundyOrders.Add(laundry);
            }

            return bIsSucceeded ? _laundyOrders : null;
            
        }

        public static async Task<bool> UpdateLaundriesWithOrderId(UserClass user, string orderId, DateTime? dropOffTime = null)
        {
            bool bIsSucceeded = false;

            foreach (ParseObject laundyOrder in _laundyOrders)
            {
                if (laundyOrder[ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER] == null || laundyOrder[ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER].ToString() == "")
                {
                    //laundry to be picked
                    laundyOrder[ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER] = orderId;
                }
                else
                {
                    //laundry to be dropped
                    laundyOrder[ParseConstants.LAUNDRY_ATTRIBUTE_DROPOFFORDER] = orderId;
                    laundyOrder[ParseConstants.LAUNDRY_ATTRIBUTE_DROPOFFREQUESTDATE] = dropOffTime != null ? dropOffTime.Value.AddHours(1) : dropOffTime;
                }

                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 5000;
                    try
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        await laundyOrder.SaveAsync(cancellation.Token);

                        bIsSucceeded = true;
                        break;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.ToString());
                        Crashes.TrackError(e);
                    }
                }

                if (!bIsSucceeded)
                    break;
            }

            return bIsSucceeded;

        }

        private static async Task<ParseObject> GetLaundry(string objectId)
        {
            var laundryQuery = ParseObject.GetQuery(ParseConstants.LAUNDRY_CLASS_NAME).
              WhereEqualTo(ParseConstants.ObjectId, objectId).Limit(1);

            IEnumerable<ParseObject> laundryObjects = null;

            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 5000;
                try
                {
                    System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                    laundryObjects = await laundryQuery.FindAsync(cancellation.Token);
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    Debug.WriteLine("Toast from: GetLaundry " + e.Message);
                }
            }

            var laundries = laundryObjects?.ToList();

            if (laundries == null || !laundries.Any())
                return null;

            return laundries.First();
        }

        public static async Task GetLaundriesForTrackingAndHistory(Dictionary<int, List<object>> orderDictionary)
        {
            var statusClassList = new List<LaundryStatusClass>();
            var historyClassList = new List<LaundryHistoryClass>();

            string endUserID = "";
            if (ParseUser.CurrentUser != null)
                endUserID = ParseUser.CurrentUser.ObjectId;

            var laundryQuery = ParseObject.GetQuery(ParseConstants.LAUNDRY_CLASS_NAME).
                WhereEqualTo(ParseConstants.LAUNDRY_ATTRIBUTE_ENDUSERID, endUserID).
                OrderByDescending("updatedAt");

            IEnumerable<ParseObject> laundryObjects = null;

            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 5000;
                try
                {
                    System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                    laundryObjects = await laundryQuery.FindAsync(cancellation.Token);
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                    System.Diagnostics.Debug.WriteLine("Toast from: GetLaundriesForTrackingAndHistory " + e.Message);
                }
            }


            if (laundryObjects == null)
                return;

            foreach (var laundry in laundryObjects)
            {
                int status = laundry.Get<int>(ParseConstants.LAUNDRY_ATTRIBUTE_STATUS);

                //track
                if (status < 6)
                {
                    int store = laundry.Get<int>(ParseConstants.LAUNDRY_ATTRIBUTE_STORE);
                    string name = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_NAME);
                    string surname = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_SURNAME);
                    string phone = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_PHONE);
                    string createdAt = laundry.CreatedAt.ToString();
                    string readyDate = laundry.Keys.Contains(ParseConstants.LAUNDRY_ATTRIBUTE_READYDATE) ? (laundry.Get<DateTime>(ParseConstants.LAUNDRY_ATTRIBUTE_READYDATE).AddHours(-1)).ToString() : "";
                    string updateDate = laundry.UpdatedAt.ToString();
                    string pickedUpOrderId = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER);
                    string details = laundry.Keys.Contains(ParseConstants.LAUNDRY_ATTRIBUTE_SERVICEORDERDETAILS) ? laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_SERVICEORDERDETAILS) :"";
                    List<LaundryDetail> laundryDetailList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LaundryDetail>>(details);

                    double price = laundry.Keys.Contains(ParseConstants.LAUNDRY_ATTRIBUTE_PRICE) ? laundry.Get<double>(ParseConstants.LAUNDRY_ATTRIBUTE_PRICE) : 0;
                    IOrder pickedUpOrder = orderDictionary[0].Cast<StatusClass>().ToList().FirstOrDefault(x => x.ObjectId == pickedUpOrderId);
                    if(pickedUpOrder == null)
                    { 
                        pickedUpOrder = orderDictionary[1].Cast<HistoryClass>().ToList().FirstOrDefault(x => x.ObjectId == pickedUpOrderId);
                        if (pickedUpOrder != null)
                            orderDictionary[1].Remove(pickedUpOrder);
                    }
                    else
                    {
                        orderDictionary[0].Remove(pickedUpOrder);
                    }

                    LaundryStatusClass laundryStatus;
                    if (laundry.Keys.Contains(ParseConstants.LAUNDRY_ATTRIBUTE_DROPOFFORDER))
                    { 
                        string dropOffOrderId = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_DROPOFFORDER);
                        if (!string.IsNullOrEmpty(dropOffOrderId))
                        { 
                            IOrder dropOffOrder = orderDictionary[0].Cast<StatusClass>().ToList().FirstOrDefault(x => x.ObjectId == dropOffOrderId);
                            if (dropOffOrder == null)
                            { 
                                dropOffOrder = orderDictionary[1].Cast<HistoryClass>().ToList().FirstOrDefault(x => x.ObjectId == dropOffOrderId);
                                if (dropOffOrder != null)
                                    orderDictionary[1].Remove(dropOffOrder);
                            }
                            else
                            {
                                orderDictionary[0].Remove(dropOffOrder);
                            }
                            laundryStatus = new LaundryStatusClass(laundry.ObjectId, store, name, surname, phone, createdAt, readyDate, updateDate, (LaundryStatus)status, pickedUpOrder, price, laundryDetailList, dropOffOrder);
                        }
                        else
                        {
                            laundryStatus = new LaundryStatusClass(laundry.ObjectId, store, name, surname, phone, createdAt, readyDate, updateDate, (LaundryStatus)status, pickedUpOrder, price, laundryDetailList);
                        }
                    }
                    else
                    {
                        laundryStatus = new LaundryStatusClass(laundry.ObjectId, store, name, surname, phone, createdAt, readyDate, updateDate, (LaundryStatus)status, pickedUpOrder, price, laundryDetailList);
                    }
                    
                    statusClassList.Add(laundryStatus);
                }
                //history
                else
                {
                    int store = laundry.Get<int>(ParseConstants.LAUNDRY_ATTRIBUTE_STORE);
                    string name = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_NAME);
                    string surname = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_SURNAME);
                    string phone = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_PHONE);
                    string createdAt = laundry.CreatedAt.ToString();
                    string readyDate = laundry.Keys.Contains(ParseConstants.LAUNDRY_ATTRIBUTE_READYDATE) ? (laundry.Get<DateTime>(ParseConstants.LAUNDRY_ATTRIBUTE_READYDATE).AddHours(-1)).ToString() : "";
                    string updateDate = laundry.UpdatedAt.ToString();
                    string finishDate = laundry.Keys.Contains(ParseConstants.LAUNDRY_ATTRIBUTE_FINISHDATE) ? (laundry.Get<DateTime>(ParseConstants.LAUNDRY_ATTRIBUTE_FINISHDATE).AddHours(-1)).ToString() : "";
                    double price = laundry.Keys.Contains(ParseConstants.LAUNDRY_ATTRIBUTE_PRICE) ? laundry.Get<double>(ParseConstants.LAUNDRY_ATTRIBUTE_PRICE) : 0;

                    string pickedUpOrderId = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_PICKEDUPORDER);

                    IOrder pickedUpOrder = orderDictionary[0].Cast<StatusClass>().ToList().FirstOrDefault(x => x.ObjectId == pickedUpOrderId);
                    if (pickedUpOrder == null)
                    { 
                        pickedUpOrder = orderDictionary[1].Cast<HistoryClass>().ToList().FirstOrDefault(x => x.ObjectId == pickedUpOrderId);
                        if (pickedUpOrder != null)
                            orderDictionary[1].Remove(pickedUpOrder);
                    }
                    else
                    {
                        orderDictionary[0].Remove(pickedUpOrder);
                    }

                    string dropOffOrderId = laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_DROPOFFORDER);
                    IOrder dropOffOrder = orderDictionary[0].Cast<StatusClass>().ToList().FirstOrDefault(x => x.ObjectId == dropOffOrderId);
                    if (dropOffOrder == null)
                    { 
                        dropOffOrder = orderDictionary[1].Cast<HistoryClass>().ToList().FirstOrDefault(x => x.ObjectId == dropOffOrderId);
                        if (dropOffOrder != null)
                            orderDictionary[1].Remove(dropOffOrder);
                    }
                    else
                    {
                        orderDictionary[0].Remove(dropOffOrder);
                    }

                    string details = laundry.Keys.Contains(ParseConstants.LAUNDRY_ATTRIBUTE_SERVICEORDERDETAILS) ? laundry.Get<string>(ParseConstants.LAUNDRY_ATTRIBUTE_SERVICEORDERDETAILS) : "";
                    List<LaundryDetail> laundryDetailList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<LaundryDetail>>(details);

                    LaundryHistoryClass laundryHistory = new LaundryHistoryClass(store,name,surname,phone,createdAt,readyDate,updateDate,finishDate,(LaundryStatus)status,pickedUpOrder,dropOffOrder, price, laundryDetailList);
                    historyClassList.Add(laundryHistory);
                }
            }

            orderDictionary[0].AddRange(statusClassList);
            orderDictionary[0] = orderDictionary[0].Cast<IStatus>().OrderByDescending(x => x.UpdateDate).Cast<object>().ToList();
            orderDictionary[1].AddRange(historyClassList);
            orderDictionary[1] = orderDictionary[1].Cast<IHistory>().OrderByDescending(x => x.UpdateDate).Cast<object>().ToList();

        }
    }

}

#endif
