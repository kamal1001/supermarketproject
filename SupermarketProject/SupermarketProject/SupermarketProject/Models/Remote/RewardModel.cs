﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupermarketProject.Common.Objects;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local;
using Microsoft.AppCenter.Crashes;
using Parse;

namespace SupermarketProject.Models.Remote
{
    public class RewardModel
    {
        public static List<Reward> RewardList;

        public static async Task FetchRewards()
        {
            DateTime? localUpdate = UserClassFunctions.GetRewardsUpdatedDateFromUser();
            DateTime? remoteUpdate = null;

            var rewardsQuery = ParseObject.GetQuery(ParseConstants.REWARDS_CLASS_NAME).WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate);


            List<RewardClass> tempList = new List<RewardClass>();

            IEnumerable<ParseObject> rewardObjects = null;
            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 15000;
                try
                {
                    if (rewardsQuery != null)
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        rewardObjects = await rewardsQuery.FindAsync(cancellation.Token);
                    }
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                }
            }

            if (rewardObjects == null || rewardObjects.Count() <= 0)
            {
                PopulateRewardList();
                return;
            }


            foreach (var rewardObject in rewardObjects)
            {
                RewardClass tempReward = new RewardClass
                {
                    ObjectId = rewardObject.ObjectId,
                    Name = rewardObject.Get<string>(ParseConstants.RewardsAttributeName),
                    Points = rewardObject.Get<int>(ParseConstants.RewardsAttributePoints),
                    IsDeleted = rewardObject.Get<bool>(ParseConstants.RewardsAttributeIsDeleted)
                };

                if (remoteUpdate == null || rewardObject.UpdatedAt > remoteUpdate)
                    remoteUpdate = rewardObject.UpdatedAt;

                tempList.Add(tempReward);
            }

            if (tempList.Count > 0)
            {
                RewardClassFunctions.AddRewards(tempList);
                if (remoteUpdate != null)
                    UserClassFunctions.AddRewardsUpdateDateToUser(remoteUpdate);
            }

            PopulateRewardList();
        }

        public static void PopulateRewardList()
        {
            RewardList = new List<Reward>();

            foreach (var rewardClass in RewardClassFunctions.GetRewardList().Where(x => !x.IsDeleted))
            {
                RewardList.Add(new Reward
                {
                    Name = rewardClass.Name,
                    Points = rewardClass.Points
                });
            }
        }
    }
}
