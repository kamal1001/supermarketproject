﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SupermarketProject.Models.Local;
using Microsoft.AppCenter.Crashes;
using Parse;

namespace SupermarketProject.Models.Remote
{
	public static class FavoriteModel
	{
		public static HashSet<string> FavoriteDictionary;
		static FavoriteModel()
		{
			InitializeMemberVariables();
		}
		private static void InitializeMemberVariables()
		{
			FavoriteDictionary = new HashSet<string>(StringComparer.Ordinal);
		}
		public static void PopulateFavoriteDictionaries()            
		{
            FavoriteDictionary.Clear();
            var favorities = FavoritiesClassFunctions.GetProductIDs();
			if (favorities == null)
				return;
			foreach ( var favorite in favorities)
			{
				FavoriteDictionary.Add(favorite.productID);
			}
		}

		public static void AddProductID(string productID)
		{

			FavoriteDictionary.Add(productID);
			FavoritiesClassFunctions.AddProductID(productID);

		}

		public static void RemoveProductID(string productID)
		{
			FavoriteDictionary.Remove(productID);
			FavoritiesClassFunctions.RemoveProductID(productID);
		}

        public static async Task<bool> SendFavoritesToRemote(string productIdString)
        {
            if (ParseUser.CurrentUser.ContainsKey("Favorites") && (string)ParseUser.CurrentUser["Favorites"] == productIdString)
                return true;

			for (int t = 0; t < 3; t++)
            {
                int cancelTime = 10000;
                try
                {
                    ParseUser.CurrentUser["Favorites"] = productIdString;
                    await ParseUser.CurrentUser.SaveAsync();
                    return true;                    
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                    return false;
                }
            }
            return false;
        }
    }

}
