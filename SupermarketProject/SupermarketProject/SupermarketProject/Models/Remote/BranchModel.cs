﻿using System;
using SupermarketProject.Common.Utilities;
using System.Threading.Tasks;
using System.Collections.Generic;
using Parse;
using SupermarketProject.Models.Local;
using System.Linq;
using SupermarketProject.Common;
using Microsoft.AppCenter.Crashes;
using Plugin.Connectivity;

namespace SupermarketProject.Models.Remote
{
    public class BranchModel
    {
        public static async Task FetchBranches()
        {
            DateTime? localUpdate = UserClassFunctions.GetBranchesUpdatedDateFromUser();
            DateTime? remoteUpdate = null;
            var branchQuery = ParseObject.GetQuery("Branches").OrderBy("ID").WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate);

            IEnumerable<ParseObject> branchObjects = null;

            if (CrossConnectivity.Current.IsConnected)
            {
                for (int t = 0; t < 3; t++)
                {
                    int cancelTime = 5000;
                    try
                    {
                        if (branchQuery != null)
                        {
                            System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                            branchObjects = await branchQuery.FindAsync(cancellation.Token);
                        }
                        break;
                    }
                    catch (Exception e)
                    {
                        Crashes.TrackError(e);
                        string message = MyDevice.InternetErrorMessage1;
                        Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                        System.Diagnostics.Debug.WriteLine("Toast from: FetchBranches " + e.ToString());
                    }
                }
            }
            if (branchObjects == null || branchObjects.Count() == 0)
            {
                PopulateBranchList();
                return;
            }
            List<BranchClass> branchList = new List<BranchClass>();
            foreach (var branchObject in branchObjects)
            {
                //System.Diagnostics.Debug.WriteLine ("aq2");
                BranchClass branch = new BranchClass();
                branch.ID = branchObject.Get<int>("ID");
                branch.Name = branchObject.Get<string>("Name");
                branch.isStoreOpen = branchObject.Get<bool>("isStoreOpen");
                branch.lat = branchObject.Get<double>("lat");
                branch.lng = branchObject.Get<double>("lng");
                branch.MailAddress = branchObject.Get<string>("MailAddress");
                branch.OpenHours = branchObject.Get<string>("OpenHours");
                branch.PhoneNumber = branchObject.Get<string>("PhoneNumber");
                branch.IsDeleted = branchObject.ContainsKey("IsDeleted") && branchObject.Get<bool>("IsDeleted");

                IList<IDictionary<string, double>> pinPointsList = branchObject.Get<IList<IDictionary<string, double>>>("pinPoints");
                string pinPointsListString = Newtonsoft.Json.JsonConvert.SerializeObject(pinPointsList);
                branch.pinPoints = pinPointsListString;

                IList<IDictionary<string, object>> areaList = branchObject.ContainsKey("Areas") ? branchObject.Get<IList<IDictionary<string, object>>>("Areas") : null;
                if (areaList != null)
                {
                    string areaListString = Newtonsoft.Json.JsonConvert.SerializeObject(areaList);
                    branch.BranchAreas = areaListString;
                }

                var buildingList = branchObject.ContainsKey("Buildings") ? branchObject.Get<IList<string>>("Buildings") : null;
                if (buildingList != null)
                {
                    string buildingListString = Newtonsoft.Json.JsonConvert.SerializeObject(buildingList);
                    branch.Buildings = buildingListString;
                }

                branchList.Add(branch);
                if (remoteUpdate == null || branchObject.UpdatedAt > remoteUpdate)
                    remoteUpdate = branchObject.UpdatedAt;
            }
            BranchClassFunctions.AddBranch(branchList);
            UserClassFunctions.AddBranchesUpdateDateToUser(remoteUpdate);
            PopulateBranchList();
        }

        public static void PopulateBranchList()
        {
            BranchHelper.BranchList = new List<Branch>();
            foreach (BranchClass branch in BranchClassFunctions.GetBranches())
            {
                //System.Diagnostics.Debug.WriteLine ("aq1");
                if (!branch.isStoreOpen)
                    continue;
                int ID = branch.ID;
                string Name = branch.Name;
                double lat = branch.lat;
                double lng = branch.lng;
                bool isDeleted = branch.IsDeleted;

                PinPoint location = new PinPoint(lat, lng);

                IList<IDictionary<string, double>> pinPointsList = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IDictionary<string, double>>>(branch.pinPoints);
                List<PinPoint> pinPoints = new List<PinPoint>();
                foreach (var pinPoint in pinPointsList)
                    pinPoints.Add(new PinPoint(pinPoint["lat"], pinPoint["lng"]));

                List<BranchArea> branchAreas = new List<BranchArea>();
                if (!string.IsNullOrWhiteSpace(branch.BranchAreas))
                {
                    IList<IDictionary<string, object>> branchAreaList = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IDictionary<string, object>>>(branch.BranchAreas);

                    foreach (var brancArea in branchAreaList)
                    {
                        object idObject;
                        object minOrderWithoutObject;
                        object minOrderObject;

                        brancArea.TryGetValue("Id", out idObject);
                        brancArea.TryGetValue("minOrder", out minOrderObject);
                        brancArea.TryGetValue("minOrderWithout", out minOrderWithoutObject);

                        string areaId = idObject as string;
                        int minOrderWithout = Convert.ToInt32(minOrderWithoutObject);
                        int minOrder = Convert.ToInt32(minOrderObject);
                        branchAreas.Add(new BranchArea(areaId, minOrderWithout, minOrder));
                    }
                }

                List<string> buildingList = null;
                if (!string.IsNullOrWhiteSpace(branch.Buildings))
                    buildingList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(branch.Buildings);


                BranchHelper.BranchList.Add(new Branch(ID, Name, branchAreas, buildingList, pinPoints, location, branch.MailAddress, branch.OpenHours, branch.PhoneNumber, branch.IsDeleted));
            }

            BranchHelper.CompleteBranchList = new List<Branch>();
            BranchHelper.CompleteBranchList = BranchHelper.BranchList;

            //For Displaying branches in Store Info
            foreach (BranchClass branch in BranchClassFunctions.GetBranches())
            {
                if (branch.isStoreOpen)
                    continue;

                int ID = branch.ID;
                string Name = branch.Name;
                double lat = branch.lat;
                double lng = branch.lng;

                PinPoint location = new PinPoint(lat, lng);

                IList<IDictionary<string, double>> pinPointsList = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IDictionary<string, double>>>(branch.pinPoints);
                List<PinPoint> pinPoints = new List<PinPoint>();
                foreach (var pinPoint in pinPointsList)
                    pinPoints.Add(new PinPoint(pinPoint["lat"], pinPoint["lng"]));

                List<BranchArea> branchAreas = new List<BranchArea>();
                if (!string.IsNullOrWhiteSpace(branch.BranchAreas))
                {
                    IList<IDictionary<string, object>> branchAreaList = Newtonsoft.Json.JsonConvert.DeserializeObject<IList<IDictionary<string, object>>>(branch.BranchAreas);

                    foreach (var brancArea in branchAreaList)
                    {
                        object idObject;
                        object minOrderWithoutObject;
                        object minOrderObject;

                        brancArea.TryGetValue("Id", out idObject);
                        brancArea.TryGetValue("minOrder", out minOrderObject);
                        brancArea.TryGetValue("minOrderWithout", out minOrderWithoutObject);

                        string areaId = idObject as string;
                        int minOrderWithout = Convert.ToInt32(minOrderWithoutObject);
                        int minOrder = Convert.ToInt32(minOrderObject);
                        branchAreas.Add(new BranchArea(areaId, minOrderWithout, minOrder));
                    }
                }

                List<string> buildingList = null;
                if (!string.IsNullOrWhiteSpace(branch.Buildings))
                    buildingList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(branch.Buildings);

                BranchHelper.CompleteBranchList.Add(new Branch(ID, Name, branchAreas, buildingList, pinPoints, location, branch.MailAddress, branch.OpenHours, branch.PhoneNumber, branch.IsDeleted));
            }
        }
    }
}
