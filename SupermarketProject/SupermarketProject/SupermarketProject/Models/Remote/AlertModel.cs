﻿using System;
using SupermarketProject.Common.Utilities;
using PCLStorage;
using System.Threading.Tasks;
using System.Collections.Generic;
using Parse;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using SupermarketProject.Models.Local;
using System.Linq;
using SupermarketProject.Common.Objects;
using Xamarin.Forms;
using SupermarketProject.Common;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Models.Remote
{
    public enum AlertType
    {
        Address_Add_Fail, Address_Add_Success, Address_DeleteConfirmation_Alert, Address_Update_Same,
        TobaccoCategory_Enter, Category_QuantityCanDiffer_Enter, ReOrder_Enter,
        Cart_EmptyBasket, Cart_MissingAddress, Cart_PriceError_First, Cart_PriceError_Second, Cart_SignInRequired, Cart_NeedLogin,

        ExitAppAlert, Maintenance_Alert,
        First_Time_User_Internet_Alert, Update_Alert,
        Connection_Alert, Feedback_Alert,
        Receipt_NoAddress_Alert, Receipt_NoConnection_Alert, Receipt_ShopClose_Alert1, Receipt_ShopClose_Alert2, Receipt_Success_Alert,
#if RewardNewUser
        Receipt_NewUserRewardApplicableToCash_Alert,
#endif
        Search_NotFound_Alert,
        Suggestion_SendedSuccesfully_Alert,
        Complaint_SentSuccesully_Alert,
        PasswordReset_Succesfully_Alert, PasswordReset_WrongEmail_Alert,
        Select_Branch_Alert,
        FirstTimeNewAddressAlert,
        Restart_App_Alert,
        //stamp related
        Stamp_NotSelected, Stamp_Congrats, Stamp_WrongPin, Stamp_Order_Contains_Stamp, Stamp_Reward_Already_Exists, Stamp_Error, Stamp_Login,
        Branch_Change_Alert,
#if Laundry
        Laundry_NewOrder_Alert, Laundry_Price_Alert, Laundry_Request_DropOff_Alert, Laundry_Already_DropOffRequested_Alert, Laundry_Remove_Alert, Laundry_Already_Exists_Alert, Laundry_DropOff_Ready_Alert,Laundry_Ready_Push_Popup
#endif
    }
    public class AlertModel
    {
        public static List<AlertClass> AlertList;
        public static async Task FetchAlerts()
        {
            DateTime? localUpdate = UserClassFunctions.GetAlertsUpdatedDateFromUser();
            DateTime? remoteUpdate = null;
            var AlertQuery = ParseObject.GetQuery("Alert").WhereGreaterThan(ParseConstants.UPDATEDATE_NAME, localUpdate);

            IEnumerable<ParseObject> AlertObjects = null;

            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 5000;
                try
                {
                    if (AlertQuery != null)
                    {
                        System.Threading.CancellationTokenSource cancellation = new System.Threading.CancellationTokenSource(cancelTime);
                        AlertObjects = await AlertQuery.FindAsync(cancellation.Token);
                    }
                    break;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    string message = MyDevice.InternetErrorMessage1;
                    Acr.UserDialogs.UserDialogs.Instance.Toast(new Acr.UserDialogs.ToastConfig(message).SetBackgroundColor(System.Drawing.Color.Red).SetMessageTextColor(System.Drawing.Color.White).SetDuration(cancelTime));
                    System.Diagnostics.Debug.WriteLine("Toast from: FetchAlerts " + e.ToString());
                }
            }
            if (AlertObjects == null || AlertObjects.Count() == 0)
            {
                PopulateAlertList();
                return;
            }
            List<AlertClass> NewAlertList = new List<AlertClass>();
            foreach (var AlertObject in AlertObjects)
            {
                AlertClass Alert = new AlertClass();
                Alert.Type = AlertObject.Get<string>("Type");
                Alert.Title = AlertObject.Get<string>("Title");
                Alert.Message = AlertObject.Get<string>("Message");
                Alert.OkText = AlertObject.Get<string>("OkText");
                Alert.CancelText = AlertObject.Get<string>("CancelText");

                NewAlertList.Add(Alert);
                if (remoteUpdate == null || AlertObject.UpdatedAt > remoteUpdate)
                    remoteUpdate = AlertObject.UpdatedAt;
            }
            AlertClassFunctions.AddAlert(NewAlertList);
            if (remoteUpdate != null)
                UserClassFunctions.AddAlertUpdateDateToUser(remoteUpdate);
            PopulateAlertList();
        }

        public static void PopulateAlertList()
        {
            AlertList = AlertClassFunctions.GetAlerts();
        }
        public static AlertClass GetAlert(AlertType alertType)
        {
            AlertClass alert = null;
            if (AlertList != null)
                alert = AlertList.Find((obj) => obj.Type == alertType.ToString());
            if (alert == null)
            {
                switch (alertType)
                {
                    case AlertType.Address_Add_Fail:
                        alert = new AlertClass()
                        {
                            Type = "Address_Add_Fail",
                            Title = "Please Check",
                            Message = "Please complete all the fields",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Address_Update_Same:
                        alert = new AlertClass()
                        {
                            Type = "Address_Update_Same",
                            Title = "Please Check",
                            Message = "Address is already the same!",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Address_Add_Success:
                        alert = new AlertClass()
                        {
                            Type = "Address_Add_Success",
                            Title = "User Information Submitted",
                            Message = "You have successfully submitted your information",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.TobaccoCategory_Enter:
                        alert = new AlertClass()
                        {
                            Type = "TobaccoCategory_Enter",
                            Title = "Please Check",
                            Message = "I am over 20 years old and I know smoking is bad for my health.",
                            OkText = "AGREE",
                            CancelText = "DISAGREE"
                        };
                        break;
                    case AlertType.ReOrder_Enter:
                        alert = new AlertClass()
                        {
                            Type = "ReOrder_Enter",
                            Title = "Please Check",
                            Message = "Some items that are no longer in stock will not be reordered. Please review your cart before proceeding",
                            OkText = "OK",
                            CancelText = "CANCEL"
                        };
                        break;
                    case AlertType.Category_QuantityCanDiffer_Enter:
                        alert = new AlertClass()
                        {
                            Type = "Category_QuantityCanDiffer_Enter",
                            Title = "Please Remember",
                            Message = "Delivered quantity might differ from the actual ordered quantity by ± 50 grams.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Cart_EmptyBasket:
                        alert = new AlertClass()
                        {
                            Type = "Cart_EmptyBasket",
                            Title = "Sorry",
                            Message = "Please add products to your basket",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Cart_MissingAddress:
                        alert = new AlertClass()
                        {
                            Type = "Cart_MissingAddress",
                            Title = "Sorry",
                            Message = "Please Enter Your Address On Settings Page",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Cart_PriceError_First:
                        alert = new AlertClass()
                        {
                            Type = "Cart_PriceError",
                            Title = "Sorry",
                            Message = "Minimum Order amount is AED {0} which is calculated after Cigarette Packets and Recharge Cards prices.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Cart_PriceError_Second:
                        alert = new AlertClass()
                        {
                            Type = "Cart_PriceError",
                            Title = "Sorry",
                            Message = "Minimum Order amount is AED {0}.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Feedback_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Feedback_Alert",
                            Title = "Thank You",
                            Message = "Your valuable feedback has been noted.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Cart_SignInRequired:
                        alert = new AlertClass()
                        {
                            Type = "Cart_SignInRequired",
                            Title = "Sign In Required!",
                            Message = "To add items to Cart, please proceed to Sign In or create a new account.",
                            OkText = "Sign In/Up",
                            CancelText = "Cancel"
                        };
                        break;
                    case AlertType.Cart_NeedLogin:
                        alert = new AlertClass()
                        {
                            Type = "Cart_MissingAddress",
                            Title = "Sorry",
                            Message = "You must be signed in to place an order.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.ExitAppAlert:
                        alert = new AlertClass()
                        {
                            Type = "ExitAppAlert",
                            Title = "Quit",
                            Message = "Are you sure you want to quit?",
                            OkText = "Yes",
                            CancelText = "No"
                        };
                        break;
                    case AlertType.Maintenance_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Maintenance_Alert",
                            Title = "Maintenance",
                            Message = "The {0} App is currently under maintenance, please try again later",
                            OkText = "OK",
                            CancelText = "No"
                        };
                        break;
                    case AlertType.First_Time_User_Internet_Alert:
                        alert = new AlertClass()
                        {
                            Type = "First_Time_User_Internet_Alert",
                            Title = "Internet Error",
                            Message = "For the first time users, the app needs to fully load without any internet interruptions, please try again when you have better internet connection.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Update_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Update_Alert",
                            Title = "Please Update",
                            Message = "Please Update Before Using App",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Connection_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Connection_Alert",
                            Title = "Connection Error",
                            Message = "Check your internet connection.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Receipt_NoAddress_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Receipt_NoAddress_Alert",
                            Title = "No Address",
                            Message = "Please Add An Address.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Receipt_NoConnection_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Receipt_NoConnection_Alert",
                            Title = "Connection Error",
                            Message = "Your order couldn't be received. Check your internet connection and try again.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Receipt_ShopClose_Alert1:
                        alert = new AlertClass()
                        {
                            Type = "Receipt_ShopClose_Alert",
                            Title = "Sorry",
                            Message = "Online shopping for {0} {1} is currently closed. Kindly choose a different branch or try again after some time.",
                            OkText = "OK",
                            CancelText = "CHANGE BRANCH"
                        };
                        break;
                    case AlertType.Receipt_ShopClose_Alert2:
                        alert = new AlertClass()
                        {
                            Type = "Receipt_ShopClose_Alert",
                            Title = "Sorry",
                            Message = "{0} {1} is open between {3} and {2}. We are looking forward to serving you at {3}",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Receipt_Success_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Receipt_Success_Alert",
                            Title = "Order Sent",
                            Message = "Please check the quality of the products you are going to receive before you pay to the delivery boy. Your satisfaction is our top priority at all times.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Select_Branch_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Select_Branch_Alert",
                            Title = "Branch",
                            Message = "Please select a branch",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
#if RewardNewUser
                    case AlertType.Receipt_NewUserRewardApplicableToCash_Alert:
				        alert = new AlertClass()
				        {
				            Type = "Receipt_NewUserRewardApplicableToCash_Alert",
				            Title = "Please Check",
				            Message = "New user discount is only applicable to cash orders. Are you sure you want to continue?",
				            OkText = "OK",
				            CancelText = "CANCEL"
				        };
				        break;
#endif
                    case AlertType.Search_NotFound_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Search_NotFound_Alert",
                            Title = "Please Check",
                            Message = "No products found in your search criteria",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Address_DeleteConfirmation_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Address_DeleteConfirmation_Alert",
                            Title = "Are You Sure?",
                            Message = "Are you sure, you want to delete this address?",
                            OkText = "YES",
                            CancelText = "NO"
                        };
                        break;
                    case AlertType.Suggestion_SendedSuccesfully_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Suggestion_SendedSuccesfully_Alert",
                            Title = "Thank You",
                            Message = "Suggestion Sent Succesfully",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Complaint_SentSuccesully_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Complaint_SendedSuccesfully_Alert",
                            Title = "Thank You",
                            Message = "Feedback Sent Succesfully",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.PasswordReset_Succesfully_Alert:
                        alert = new AlertClass()
                        {
                            Type = "PasswordReset_Succesfully_Alert",
                            Title = "Password Reset",
                            Message = "Password reset link sent your e-mail",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.PasswordReset_WrongEmail_Alert:
                        alert = new AlertClass()
                        {
                            Type = "PasswordReset_WrongEmail_Alert",
                            Title = "Email not found",
                            Message = "Please check your email",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.FirstTimeNewAddressAlert:
                        alert = new AlertClass()
                        {
                            Type = "FirstTimeNewAddressAlert",
                            Title = "App Update Changes to Location",
                            Message = "To avoid any inconvenience and improve your experience, we have updated our location selection functionality so that you can now only place an order from an address that falls within our Delivery Ranges. You will be now asked to select the desired Delivery Address each time you order, for more accurate product results. Please note, you will have to re-create and save addresses again but this will need to be done only once. We thank you for your cooperation.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Branch_Change_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Branch_Change_Alert",
                            Title = "Detected Change in Delivery Branch!",
                            Message = "Price and Stock Availability of items in your Cart might change due to change in Delivery Branch. Are you sure you would like to continue?",
                            OkText = "Confirm",
                            CancelText = "Cancel"
                        };
                        break;
#if Laundry
                    case AlertType.Laundry_NewOrder_Alert:
                    alert = new AlertClass()
                    {
                        Type = "Laundry_NewOrder_Alert",
                        Title = "Place a laundry order?",
                        Message = "Our staff will come pick up your laundry!",
                        OkText = "Yes",
                        CancelText = "No"
                    };
                    break;
                    case AlertType.Laundry_Price_Alert:
                    alert = new AlertClass()
                    {
                        Type = "Laundry_Price_Alert",
                        Title = "Laundry Order In Cart!",
                        Message = $"Please add a minimum grocery order of AED {MyDevice.MinPriceForLaundry:0.00} and Checkout from Cart to proceed.",
                        OkText = "Okay",
                        CancelText = ""
                    };
                    break;
                    case AlertType.Laundry_Request_DropOff_Alert:
                    alert = new AlertClass()
                    {
                        Type = "Laundry_Request_DropOff_Alert",
                        Title = "Request Laundry Drop Off ?",
                        Message = "Our staff will deliver laundry to you!",
                        OkText = "Yes",
                        CancelText = "No"
                    };
                    break;
                    case AlertType.Laundry_Already_DropOffRequested_Alert:
						alert = new AlertClass()
						{
							Type = "Laundry_Already_DropOffRequested_Alert",
							Title = "Already Dropoff Requested",
							Message = "Already dropoff for this laundry requested!",
							OkText = "OK",
							CancelText = ""
						};
						break;
					case AlertType.Laundry_Remove_Alert:
						alert = new AlertClass()
						{
							Type = "Laundry_Remove_Alert",
							Title = "Cancel Laundry ?",
							Message = "Are you sure you want to cancel the laundry Order?",
							OkText = "Yes",
							CancelText = "No"
						};
						break;
                    case AlertType.Laundry_Already_Exists_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Laundry_Already_Exists_Alert",
                            Title = "Laundry Exists!",
                            Message = "Sorry you already have a laundry order in your cart. Please complete this laundry order before placing your next.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Laundry_DropOff_Ready_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Laundry_DropOff_Ready_Alert",
                            Title = "Laundry Drop Off Ready",
                            Message = "Would you like to request a laundry drop off?",
                            OkText = "YES",
                            CancelText = "NO"
                        };
                        break;
                    case AlertType.Laundry_Ready_Push_Popup:
                        alert = new AlertClass()
                        {
                            Type = "Laundry_Ready_Push_Popup",
                            Title = "Laundry Ready",
                            Message = "Your laundry order is ready! Please pick up at your earliest convience or schedule a drop of with us.",
                            OkText = "Okay",
                            CancelText = ""
                        };
                        break;

#endif
                    case AlertType.Stamp_NotSelected:
                        alert = new AlertClass()
                        {
                            Type = "Stamp_NotSelected",
                            Title = "No Stamp Selected",
                            Message = "Please click on at least one stamp to receive a stamp!",
                            OkText = "Got It!",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Stamp_Congrats:
                        alert = new AlertClass()
                        {
                            Type = "Stamp_Congrats",
                            Title = "CONGRATULATIONS",
                            Message = "You have {0} new Stamps added to your card.\n\nOnly {1} Stamps to go!",
                            OkText = "Okay Great",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Stamp_WrongPin:
                        alert = new AlertClass()
                        {
                            Type = "Stamp_WrongPin",
                            Title = "Error",
                            Message = "You have entered a wrong pin, please try agian.",
                            OkText = "Okay",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Stamp_Order_Contains_Stamp:
                        alert = new AlertClass()
                        {
                            Type = "Stamp_Order_Contains_Stamp",
                            Title = "Stamp Product",
                            Message = "Your order contains stamp product(s). Make sure to verify your stamp on delivery!",
                            OkText = "Okay",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Stamp_Reward_Already_Exists:
                        alert = new AlertClass()
                        {
                            Type = "Stamp_Reward_Already_Exists",
                            Title = "Existing Reward",
                            Message = "That stamp reward already exists in your cart.",
                            OkText = "Okay",
                            CancelText = ""
                        };
                        break;
                    case AlertType.Stamp_Error:
                        alert = new AlertClass()
                        {
                            Type = "Stamp_Error",
                            Title = "Error",
                            Message = "An error occured, please try again.",
                            OkText = "Okay",
                            CancelText = ""
                        };
                        break;

                    case AlertType.Restart_App_Alert:
                        alert = new AlertClass()
                        {
                            Type = "Restart_App_Alert",
                            Title = "Attention!",
                            Message = "Your internet connection was not working properly. App will resync data now.",
                            OkText = "OK",
                            CancelText = ""
                        };
                        break;

                    case AlertType.Stamp_Login:
                        alert = new AlertClass()
                        {
                            Type = "Stamp_Login",
                            Title = "Error",
                            Message = "You need to log in before using stamps.",
                            OkText = "Okay",
                            CancelText = ""
                        };
                        break;

                }
            }
            return alert;
        }
    }
}