﻿using System;
using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using System.Linq;

namespace SupermarketProject.Models.Local
{
	public class CartClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand tableExistCommand;
		public static void Initialize()
		{
			//db = new SQLiteConnection (DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "CartTable");
		}
		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}

		public static void AddOrUpdateProduct(string productId, int productQuantity)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<CartClass>();
			}
			CartClass tempClass = new CartClass();
			tempClass.ProductID = productId;
			tempClass.ProductQuantity = productQuantity;
			MyDevice.db.InsertOrReplace(tempClass);
			tempClass = null;
		}

	    public static void AddProductListToCart(List<CartClass> productList)
	    {
	        if (!TableExists())
	        {
	            MyDevice.db.CreateTable<CartClass>();
	        }

	        MyDevice.db.InsertAll(productList, "OR REPLACE", true);
        }

	    public static List<CartClass> GetProductList()
		{
			List<CartClass> productList;// = new List<CartClass>();

			if (!TableExists())
			{
				productList = new List<CartClass>();
				return productList;
			}
			productList = MyDevice.db.Table<CartClass>().ToList();
			/*var tempProductList = from CartTable in MyDevice.db.Table<CartClass> () select CartTable;

			foreach (var product in tempProductList)
				productList.Add(product);	*/

			return productList;
		}
		public static void DeleteProduct(string productId)
		{

			if (!TableExists())
			{
				MyDevice.db.CreateTable<CartClass>();
			}

			MyDevice.db.Delete<CartClass>(productId);

		}

		public static void ClearTable()
		{
			if (TableExists())
			{
				MyDevice.db.DeleteAll<CartClass>();
				MyDevice.db.Query<CartClass>("UPDATE sqlite_sequence SET seq=0 WHERE name='CartTable';");
			}
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<CartClass>();
			}
		}
	}
}
