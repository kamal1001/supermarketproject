﻿using System;
using SQLite;
using SupermarketProject.Common.Utilities;
using System.Collections.Generic;

namespace SupermarketProject.Models.Local
{
	[Table("Products")]
	public class ProductClass
	{
		[PrimaryKey]
		public string objectId { get; set; }
		public string CategoryId { get; set; }
		public string ImageID { get; set; }
		public string Name { get; set; }
		public string Quantity { get; set; }
		public string ParentCategory{ get; set; }
		//public bool IsTopSelling{ get; set; }
		public int Priority{ get; set; }
		public bool IsGramBased { set; get;}
		public bool IsSelectedByBrand { set; get; }		
		public string Keywords { get; set; }
        public string Barcode { get; set; }
        public bool IsDeleted { get; set; }
#if Loyalty
        public int LoyaltyPoints { get; set; }
#endif
        public double Vat { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}

