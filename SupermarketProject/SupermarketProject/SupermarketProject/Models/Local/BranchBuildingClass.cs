﻿using SQLite;

namespace SupermarketProject.Models.Local
{
    [Table("BranchBuildingTable")]
    public class BranchBuildingClass
    {       
        [PrimaryKey]
        public string objectId { get; set; }
        public int BranchId { get; set; }
        public string BuildingId { get; set; }
        public int MinOrder { get; set; }
        public int MinOrderWithout { get; set; }
        public bool IsDeleted { get; set; }
    }
}

