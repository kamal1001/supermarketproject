﻿using System;
using SQLite;

namespace SupermarketProject.Models.Local
{
    [Table("ProductBranchInfos")]
    public class ProductBranchInfoClass
    {
        [PrimaryKey]
        public string objectId { get; set; }
        public string ProductId { get; set; }
        public string CategoryId { get; set; }
        public int BranchIndex { get; set; }

        public bool IsInStore { get; set; }
        public bool IsInStock { get; set; }
        public bool IsDeleted { get; set; }
        public decimal Price { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
