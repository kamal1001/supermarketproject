﻿using SQLite;

namespace SupermarketProject.Models.Local
{
	[Table("BuildingTable")]
    public class BuildingClass : GeneralPopUpItemModel
	{		
		[PrimaryKey]
		public string objectId { get; set; }
        private string _buildingName;
        public string BuildingName
        {
            get
            {
                return _buildingName;
            }
            set
            {
                _buildingName = value;
                base.Name = value;
            }
        }

        public string AreaId { get; set; }
        public int Index { get; set; }
        public bool IsCluster { get; set; }
        public bool IsDeleted { get; set; }
    }
}

