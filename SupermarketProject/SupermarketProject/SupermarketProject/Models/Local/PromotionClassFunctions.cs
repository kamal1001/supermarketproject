﻿using System;
using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using SupermarketProject.Common.Objects;

namespace SupermarketProject.Models.Local
{
	public class PromotionClassFunctions
	{
		private static SQLiteCommand tableExistCommand;

		public static void Initialize()
		{
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "Promotions");
		}

		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}

		public static void AddPromotion(List<PromotionClass> promotionList)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<PromotionClass>();
			}
			MyDevice.db.InsertAll(promotionList, "OR REPLACE", true);
		}
		public static bool RemovePromotion(string objectId)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<PromotionClass>();
			}

			PromotionClass tempPromotion = (from a in MyDevice.db.Table<PromotionClass>()
			                              where a.objectId == objectId
			                              select a).SingleOrDefault();
		    if (tempPromotion == null || tempPromotion.objectId == null || tempPromotion.objectId != objectId)
		        return false;

		    MyDevice.db.Delete(tempPromotion);

		    return true;				
		}
		public static List<PromotionClass> GetPromotions()
		{
			List<PromotionClass> promotionList;

			if (!TableExists())
			{
				promotionList = new List<PromotionClass>();
				return promotionList;
			}
			promotionList = MyDevice.db.Table<PromotionClass>().Where(p => p.EndDate > MyDevice.DubaiDateTimeNow).OrderByDescending(p=>p.Priority).ThenBy(p=>p.CreatedDate).ToList();
			System.Diagnostics.Debug.WriteLine("Total Promotion Count: " + promotionList.Count);
			return promotionList;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<PromotionClass>();
			}
		}
	}
}
