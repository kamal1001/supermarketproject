﻿using System;
using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using System.Linq;

namespace SupermarketProject.Models.Local
{
	public class ProductClassFunctions
	{
		private static SQLiteCommand tableExistCommand;
		public static void Initialize()
		{
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "Products");
		}

		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}

		public static void AddProduct(List<ProductClass> productList)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<ProductClass>();
			}
			MyDevice.db.InsertAll(productList, "OR REPLACE", true);
		}

		public static List<ProductClass> GetProducts()
		{
			List<ProductClass> productList;

			if (!TableExists())
			{
				productList = new List<ProductClass>();
				return productList;

			}
            productList = MyDevice.db.Table<ProductClass>().Where(p =>!p.IsDeleted && p.IsSelectedByBrand).OrderBy(p => p.Priority).ThenBy(p => p.Name).ToList();
			System.Diagnostics.Debug.WriteLine("Total Product Count: "+productList.Count);
			return productList;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<ProductClass>();
			}
		}
	}
}
