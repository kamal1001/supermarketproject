﻿using SQLite;

namespace SupermarketProject.Models.Local
{
	[Table("StateTable")]
    public class StateClass : GeneralPopUpItemModel
    	{		
		[PrimaryKey]
		public string objectId { get; set; }
        private string _stateName;
        public string StateName
        {
            get
            {
                return _stateName;
            }
            set
            {
                _stateName = value;
                base.Name = value;
            }
        }

	    public bool IsDeleted { get; set; }
    }
}

