﻿using SQLite;
namespace SupermarketProject.Models.Local
{
	[Table("Alert")]
	public class AlertClass
	{
		[PrimaryKey]
		public string Type { get; set; }
		public string Title { get; set; }
		public string Message { get; set; }
		public string OkText { get; set; }
		public string CancelText { get; set; }
	}
}