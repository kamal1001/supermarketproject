﻿using System.Collections.Generic;
using SupermarketProject.Models.Local.Helpers;
using SupermarketProject.Models.Remote;

#if Laundry
namespace SupermarketProject.Models.Local
{
	public class LaundryHistoryClass : IHistory
	{
        public LaundryHistoryClass(int store, string name, string surname, string phone, string createdAt, string readyDate, string updateDate, string finishDate, LaundryModel.LaundryStatus status, IOrder pickedUpOrder, IOrder dropOffOrder, double price, List<LaundryDetail> laundryDetails)
        {
            Store = store;
            Name = name;
            Surname = surname;
            Phone = phone;
            CreatedAt = createdAt;
            ReadyDate = readyDate;
            UpdateDate = updateDate;
            FinishDate = finishDate;
            Status = status;
            PickedUpOrder = pickedUpOrder;
            DropOffOrder = dropOffOrder;
            Price = price;
            LaundryDetails = laundryDetails;
        }

        public int Store { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Phone { get; set; }

        public string CreatedAt { get; set; }

        public string ReadyDate { get; set; }

        public string UpdateDate { get; set; }
        public string FinishDate { get; set; }

        public LaundryModel.LaundryStatus Status { get; set; }

        public IOrder PickedUpOrder { get; set; }

        public IOrder DropOffOrder { get; set; }

        public double Price { get; set; }

        public List<LaundryDetail> LaundryDetails { get; set; }
    }
}
#endif
