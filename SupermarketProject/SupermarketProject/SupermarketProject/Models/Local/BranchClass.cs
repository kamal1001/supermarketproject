﻿using System;
using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;

namespace SupermarketProject.Models.Local
{
	[Table("Branches")]
	public class BranchClass
	{
		[PrimaryKey]
		public int ID { get; set; }
		public string Name { get; set; }
		public string BranchAreas { get; set; }
        public string Buildings { get; set; }
        public string pinPoints { get; set; }
		public bool isStoreOpen { get; set; }
		public double lat { get; set; }
		public double lng { get; set; }
        public string MailAddress { get; set; }
	    public string OpenHours { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsDeleted { get; set; }
    }
}

