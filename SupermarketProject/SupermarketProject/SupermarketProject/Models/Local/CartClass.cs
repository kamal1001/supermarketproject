﻿using System;
using SQLite;
using SupermarketProject.Common.Utilities;
using System.Collections.Generic;

namespace SupermarketProject.Models.Local
{
	[Table("CartTable")]
	public class CartClass
	{
		[PrimaryKey]
		public string ProductID { get; set;}
		public int ProductQuantity { get; set; }
		/*
		private bool TableExists<T> (SQLiteConnection connection,string tableName)
		{    
			const string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			var cmd = connection.CreateCommand (cmdText, tableName);
			return cmd.ExecuteScalar<string> () != null;
		}

		public void AddOrUpdateProduct(string productId,int productQuantity)
		{			
			var db = new SQLiteConnection (DBConstants.DB_PATH);

			if (!TableExists<CartClass> (db,"CartTable")) {
				db.CreateTable<CartClass>();
			}

			this.ProductID = productId;
			this.ProductQuantity = productQuantity;
			db.InsertOrReplace (this);

			db.Close ();
		}

		public List<CartClass> GetProductList()
		{
			List<CartClass> productList = new List<CartClass>();

			var db = new SQLiteConnection (DBConstants.DB_PATH);

			if (!TableExists<CartClass> (db,"CartTable")) {
				db.Close ();
				return productList;			
			}

			var tempProductList = from CartTable in db.Table<CartClass> () select CartTable;

			foreach (var product in tempProductList)
				productList.Add(product);			

			db.Close ();

			return productList;
		}
		public void DeleteProduct(string productId)
		{			
			var db = new SQLiteConnection (DBConstants.DB_PATH);

			if (!TableExists<CartClass> (db,"CartTable")) {
				db.CreateTable<CartClass>();
			}

			db.Delete<CartClass> (productId);

			db.Close ();
		}

		public void ClearTable()
		{
			var db = new SQLiteConnection (DBConstants.DB_PATH);
			if (TableExists<CartClass> (db,"CartTable")) {
				db.DeleteAll<CartClass> ();
				db.Query<CartClass> ("UPDATE sqlite_sequence SET seq=0 WHERE name='CartTable';");
			}				
			db.Close ();
		}

		public void ResetTable()
		{
			var db = new SQLiteConnection (DBConstants.DB_PATH);
			if (TableExists<CartClass> (db,"CartTable")) {
				db.DropTable<CartClass> ();
			}				
			db.Close ();
		}*/
	}
}

