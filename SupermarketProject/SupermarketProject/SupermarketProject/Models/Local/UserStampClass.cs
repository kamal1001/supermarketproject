﻿using SQLite;

namespace SupermarketProject.Models.Local
{
	[Table("UserStampTable")]
	public class UserStampClass
	{		
		[PrimaryKey]
		public string objectId { get; set; }
		public string UserId { get; set; }
        public string StampId { get; set; }
        public int Count { get; set; }
    }
}

