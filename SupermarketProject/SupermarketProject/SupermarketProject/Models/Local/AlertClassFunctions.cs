﻿using SQLite;
using SupermarketProject.Common.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace SupermarketProject.Models.Local
{
	public class AlertClassFunctions
	{
		private static SQLiteCommand tableExistCommand;
		public static void Initialize()
		{
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "Alert");
		}
		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}
		public static void AddAlert(List<AlertClass> alertList)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<AlertClass>();
			}
			MyDevice.db.InsertAll(alertList, "OR REPLACE", true);
		}

		public static List<AlertClass> GetAlerts()
		{
			List<AlertClass> alertList;

			if (!TableExists())
			{
				alertList = new List<AlertClass>();
				return alertList;
			}
			alertList = MyDevice.db.Table<AlertClass>().ToList();

			return alertList;
		}
		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<AlertClass>();
			}
		}
	}
}