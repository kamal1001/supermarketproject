﻿using System;
using SQLite;
namespace SupermarketProject.Models.Local
{
    [Xamarin.Forms.Internals.Preserve(AllMembers = true)]
    [Table("Promotions")]
    public class PromotionClass
    {
        [PrimaryKey]
        public string objectId { get; set; }
        public string ImageId { get; set; }
        public string PromotionType { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public string SubDescription { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PromotionVariables { get; set; }
        public string InCategories { get; set; }
        public decimal SaleValue { get; set; }
        public int InProductCount { get; set; }
        public string OutProducts { get; set; }
        public int Priority { get; set; }
        public string Branch { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
