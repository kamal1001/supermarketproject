﻿using SQLite;

namespace SupermarketProject.Models.Local
{
	[Table("StampRewardTable")]
	public class StampRewardClass
	{
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }
        public string objectId { get; set; }
		public string StampId { get; set; }
        public string UserId { get; set; }
        public string ProductId { get; set; }
    }
}

