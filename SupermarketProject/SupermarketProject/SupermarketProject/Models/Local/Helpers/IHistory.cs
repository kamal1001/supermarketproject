﻿namespace SupermarketProject.Models.Local.Helpers
{
    public interface IHistory
    {
        string UpdateDate { get; set; }
    }
}
