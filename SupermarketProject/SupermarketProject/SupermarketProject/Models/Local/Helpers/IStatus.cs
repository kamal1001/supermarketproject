﻿using System.Diagnostics.Contracts;

namespace SupermarketProject.Models.Local.Helpers
{
    public interface IStatus
    {
       string UpdateDate { get; set; }
    }
}
