﻿using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using SupermarketProject.Models.Local;
using System.Linq;
using SupermarketProject.Models.Remote;

namespace SupermarketProject
{
    public class BranchBuildingClassFunctions
    {
        //private static SQLiteConnection db;
        private static SQLiteCommand _tableExistCommand;
        public static void Initialize()
        {
            //MyDevice.db = new SQLiteConnection(DBConstants.DB_PATH);
            string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
            _tableExistCommand = MyDevice.db.CreateCommand(cmdText, "BranchBuildingTable");
        }
        private static bool TableExists()
        {
            return _tableExistCommand.ExecuteScalar<string>() != null;
        }

        public static void AddBranchBuilding(List<BranchBuildingClass> buildingList)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<BranchBuildingClass>();
            }
            MyDevice.db.InsertAll(buildingList, "OR REPLACE", true);
        }

        public static void Delete(string id)
        {

            if (!TableExists())
            {
                MyDevice.db.CreateTable<BranchBuildingClass>();
            }

            MyDevice.db.Delete<BranchBuildingClass>(id);

        }


        public static List<BranchBuildingClass> GetBranchBuilding()
        {
            List<BranchBuildingClass> branchbuildingList;

            if (!TableExists())
            {
                branchbuildingList = new List<BranchBuildingClass>();
                return branchbuildingList;

            }
            var buildingEnum = MyDevice.db.Table<BranchBuildingClass>().Where(x => !x.IsDeleted).ToList();
            branchbuildingList = buildingEnum.ToList();
            return branchbuildingList;
        }

        public static void ResetTable()
        {
            if (TableExists())
            {
                MyDevice.db.DropTable<BranchBuildingClass>();
            }
        }
    }
}
