﻿#if Laundry

using System;
using SQLite;

namespace SupermarketProject.Models.Local
{
    [Table("Laundry")]
    public class LaundryClass
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }
        public string ObjectId { get; set; }
        public int Store { get; set; }
        public string EndUserId { get; set; }
        public DateTime FinishDate { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string DropOffOrder { get; set; }
        public DateTime ReadyDate { get; set; }
        public int Status { get; set; }
        public string PickedUpOrder { get; set; }
        public string Surname { get; set; }
    }
}

#endif
