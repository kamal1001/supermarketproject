﻿using SQLite;

namespace SupermarketProject.Models.Local
{
	[Table("AddressTable")]
	public class AddressClass
	{		
		[PrimaryKey]
		public string Id { get; set; }
		public string UserID { get; set; }
		public string Name { get; set; }
		public string Surname { get; set; }
		public string PhoneNumber{ get; set; }
		public string Address {get; set;}
		public string AddressDescription {get; set;}
		public string AddressLine3 { get; set; }
		public bool IsActive { get; set; }
        public string BuildingId { get; set; }
        public string ApartmentNumber { get; set; }
        public int? BranchId { get; set; }
        public string Note { get; set; }
        public bool IsDeleted { get; set; }
    }
}

