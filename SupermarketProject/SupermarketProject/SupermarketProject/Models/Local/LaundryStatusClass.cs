﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SupermarketProject.Models.Remote;
using SupermarketProject.Models.Local.Helpers;

#if Laundry
namespace SupermarketProject.Models.Local
{
	public class LaundryStatusClass : IStatus//, INotifyPropertyChanged
	{
        public LaundryStatusClass()
        {

        }

        public LaundryStatusClass(string objectId, int store, string name, string surname, string phone, string createdAt, string readyDate, string updateDate, LaundryModel.LaundryStatus status, IOrder pickedUpOrder, double price, List<LaundryDetail> laundryDetails, IOrder dropOffOrder = null)
		{
		    ObjectId = objectId;
		    Store = store;
		    Name = name;
		    Surname = surname;
		    Phone = phone;
		    CreatedAt = createdAt;
		    ReadyDate = readyDate;
		    UpdateDate = updateDate;
		    Status = status;
		    PickedUpOrder = pickedUpOrder;
		    Price = price;
            LaundryDetails = laundryDetails;
		    DropOffOrder = dropOffOrder;
		}

        public string ObjectId { get; set; }

		public int Store { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Phone { get; set; }

        public string CreatedAt { get; set; }

        public string ReadyDate { get; set; }

        public string UpdateDate { get; set; }
        public List<LaundryDetail> LaundryDetails { get; set; }
        //private LaundryModel.LaundryStatus _status;

	    public LaundryModel.LaundryStatus Status { get; set; }
	    //{
	    //    get { return _status; }
     //       set
     //       {
     //           if (value.Equals(_status))
     //           {
     //               return;
     //           }
     //           _status = value;
     //           OnPropertyChanged();
     //       }
	    //}

	    public IOrder PickedUpOrder { get; set; }

        public IOrder DropOffOrder { get; set; }

        public double Price { get; set; }

     //   public event PropertyChangedEventHandler PropertyChanged;

	    //protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    //{
	    //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    //}
    }
}
#endif
