﻿using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using SupermarketProject.Models.Local;
using System.Linq;
using SupermarketProject.Models.Remote;

namespace SupermarketProject
{
	public class AreaClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand _tableExistCommand;
		public static void Initialize()
		{
			//MyDevice.db = new SQLiteConnection(DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			_tableExistCommand = MyDevice.db.CreateCommand(cmdText, "AreaTable");
		}
		private static bool TableExists()
		{
			return _tableExistCommand.ExecuteScalar<string>() != null;
		}

        public static void AddArea(List<AreaClass> areaList)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<AreaClass>();
            }
            MyDevice.db.InsertAll(areaList, "OR REPLACE", true);
        }


        public static List<AreaClass> GetAreas()
		{
			List<AreaClass> areaList;

			if (!TableExists())
			{
				areaList = new List<AreaClass>();
				return areaList;

			}

		    var areaEnum = MyDevice.db.Table<AreaClass>().Where(x => !x.IsDeleted).OrderBy(p => p.AreaName).ToList();
            areaList = areaEnum.Where(x => StateModel.StateList.Any(y => y.objectId == x.StateId)).ToList();

			return areaList;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<AreaClass>();
			}
		}
	}
}
