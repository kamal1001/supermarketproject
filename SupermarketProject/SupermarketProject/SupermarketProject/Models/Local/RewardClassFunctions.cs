﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupermarketProject.Common.Utilities;
using SQLite;

namespace SupermarketProject.Models.Local
{
	public class RewardClassFunctions
	{
		private static SQLiteCommand tableExistCommand;
		public static void Initialize()
		{
			//db = new SQLiteConnection (DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "Rewards");
		}

		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<RewardClass>();
			}
		}

		public static void AddRewards(List<RewardClass> rewardList)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<RewardClass>();
			}
			MyDevice.db.InsertAll(rewardList, "OR REPLACE", true);
		}

		public static List<RewardClass> GetRewardList()
		{
			List<RewardClass> rewardList;

			if (!TableExists())
			{
				rewardList = new List<RewardClass>();
				return rewardList;

			}
			rewardList = MyDevice.db.Table<RewardClass>().OrderBy(x => x.Points).ToList();
			return rewardList;
		}
	}
}
