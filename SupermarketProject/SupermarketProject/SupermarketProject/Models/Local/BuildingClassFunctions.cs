﻿using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using SupermarketProject.Models.Local;
using System.Linq;
using SupermarketProject.Models.Remote;

namespace SupermarketProject
{
	public class BuildingClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand _tableExistCommand;
		public static void Initialize()
		{
			//MyDevice.db = new SQLiteConnection(DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			_tableExistCommand = MyDevice.db.CreateCommand(cmdText, "BuildingTable");
		}
		private static bool TableExists()
		{
			return _tableExistCommand.ExecuteScalar<string>() != null;
		}

        public static void AddBuilding(List<BuildingClass> buildingList)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<BuildingClass>();
            }
            MyDevice.db.InsertAll(buildingList, "OR REPLACE", true);
        }


        public static List<BuildingClass> GetBuildings()
		{
			List<BuildingClass> buildingList;

			if (!TableExists())
			{
				buildingList = new List<BuildingClass>();
				return buildingList;

			}
            var buildingEnum = MyDevice.db.Table<BuildingClass>().Where(x => !x.IsDeleted).OrderBy(p => p.Index).ToList();
            buildingList = buildingEnum.Where(x => AreaModel.AreaList.Any(y => y.objectId == x.AreaId)).ToList();
			return buildingList;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<BuildingClass>();
			}
		}
	}
}
