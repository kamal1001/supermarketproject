﻿using System;
using SupermarketProject.Models.Remote;
using System.Collections.Generic;
using SupermarketProject.Common.Utilities;
using SupermarketProject.Models.Local.Helpers;

namespace SupermarketProject.Models.Local
{
    public class StatusClass : IStatus, IOrder
    {
        public StatusClass(string objectId, List<string> productOrderList, List<string> historyProducts, string address, string addressDesc, string addressLine3,
            string name, string surname, string phone, string createdDate, string branch, string branchNumber,
            OrderModel.OrderStatus orderStatus, string deliveryStaffName, string deliveryStaffPhone, string approveDate,
                            string dispatchDate, string updatedDate, int earnedPoints, string estimatedDeliveryTime, int totalProductCount, int change, double pointDiscount)
        {
            ObjectId = objectId;
            ProductOrderList = productOrderList;
            Branch = branch;
            BranchNumber = branchNumber;
            Status = "Status: ";
            Address = address;
            AddressDescription = addressDesc;
            HistoryProducts = historyProducts;
            AddressLine3 = addressLine3;
            Name = name;
            Surname = surname;
            Phone = phone;
            DeliveryStaffName = deliveryStaffName;
            DeliveryStaffPhone = deliveryStaffPhone;
            OrderStatus = orderStatus;
            CreatedDate = createdDate;
            DispatchDate = dispatchDate;
            ApproveDate = approveDate;
            UpdateDate = updatedDate;
            EarnedPoints = earnedPoints;
            EstimatedDeliveryTime = estimatedDeliveryTime;
            TotalProductCount = totalProductCount;
            Change = change;
            PointDiscount = pointDiscount;
            //System.Diagnostics.Debug.WriteLine ("Status: " + surname);
            switch (orderStatus)
            {
                case OrderModel.OrderStatus.WAITING_CONFIRMATION:
                    Status += $"Your order has been sent successfully. You will receive your order {EstimatedDeliveryTime} upon " + MyDevice.Brand + "'s confirmation.";
                    break;
                case OrderModel.OrderStatus.CONFIRMED:
                    Status += "Your order has been received and is being prepared";
                    break;
                case OrderModel.OrderStatus.IN_TRANSIT:
                    Status += "Your order has been prepared and it has been dispatched. Your delivery boy name is:";
                    break;
                default:
                    break;
            }

            SetPrices();

        }

        private void SetPrices()
        {
            TotalPrice = 0.0f;
            TotalDiscount = 0;
            TotalVat = 0;

            foreach (string order in ProductOrderList)
            {
                var orderPrice = Double.Parse(order.Split(';')[3].Split(':')[1]);
                var isAvailable = Convert.ToBoolean(order.Split(';')[4].Split(':')[1]);
                var quantity = Int32.Parse(order.Split(';')[6].Split(':')[1]);
                var perItemPrice = Double.Parse(order.Split(';')[7].Split(':')[1]);
                var discountedItemPrice = Double.Parse(order.Split(';')[8].Split(':')[1]);
                var vat = Double.Parse(order.Split(';')[9].Split(':')[1]);
                var orderCanceled = Convert.ToBoolean((order.Split(';')[10]).Split(':')[1]);
                if (isAvailable && !orderCanceled)
                {
                    TotalPrice += orderPrice;
                    TotalDiscount += discountedItemPrice == 0 ? 0 : (perItemPrice - discountedItemPrice) * quantity;
                    TotalVat += discountedItemPrice == 0 ? (perItemPrice * vat / 100) * quantity : (discountedItemPrice * vat / 100) * quantity;

                }
            }

        }

        public string ObjectId { get; set; }
        public List<string> ProductOrderList;
        public double TotalPrice { get; set; }
        public string CreatedDate { get; set; }
        public string Branch { get; set; }
        public string BranchNumber { get; set; }
        public string Status { get; set; }
        public string Address { get; set; }
        public string AddressDescription { get; set; }
        public List<string> HistoryProducts { get; set; }
        public string AddressLine3 { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public string DeliveryStaffName { get; set; }
        public string DeliveryStaffPhone { get; set; }
        public string ApproveDate { get; set; }
        public string DispatchDate { get; set; }
        public string UpdateDate { get; set; }
        public string OrderId { get; set; }
        public int EarnedPoints { get; set; }
        public OrderModel.OrderStatus OrderStatus { get; set; }
        public string EstimatedDeliveryTime { get; set; }
        public int TotalProductCount { get; set; }

        public int Change { get; set; }
        public double PointDiscount { get; set; }
        public Double TotalDiscount { get; set; }
        public Double TotalVat { get; set; }
    }
}

