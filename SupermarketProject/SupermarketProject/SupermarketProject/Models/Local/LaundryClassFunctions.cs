﻿#if Laundry

using System.Collections.Generic;
using System.Linq;
using SupermarketProject.Common.Utilities;
using SQLite;

namespace SupermarketProject.Models.Local
{
    public class LaundryClassFunctions
    {
        private static SQLiteCommand tableExistCommand;
        public static void Initialize()
        {
            //db = new SQLiteConnection (DBConstants.DB_PATH);
            string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
            tableExistCommand = MyDevice.db.CreateCommand(cmdText, "Laundry");
        }
        private static bool TableExists()
        {
            return tableExistCommand.ExecuteScalar<string>() != null;
        }

        public static void AddLaundry(LaundryClass laundry)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<LaundryClass>();
            }

            //check for existing draft laundry
            if(laundry.ObjectId == "" && (from x in MyDevice.db.Table<LaundryClass>() where x.ObjectId == "" select x).Any())
                return;

            MyDevice.db.Insert(laundry);

        }

        public static void RemoveLaundry(string objectId)
        {
            if (!TableExists())
            {
                return;
            }

            LaundryClass laundry = (from x in MyDevice.db.Table<LaundryClass>() where x.ObjectId == objectId select x).FirstOrDefault();

            MyDevice.db.Delete(laundry);

        }

        public static void RemoveAllLaundries()
        {
            if (!TableExists())
            {
                return;
            }

            MyDevice.db.DeleteAll<LaundryClass>();
        }

        public static void UpdateLaundry(LaundryClass laundry)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<LaundryClass>();
            }

            LaundryClass tempLaundry = (from a in MyDevice.db.Table<LaundryClass>()
                                        where a.Id == laundry.Id
                                        select a).SingleOrDefault();
            tempLaundry = laundry;

            MyDevice.db.Update(tempLaundry);
        }

        public static List<LaundryClass> GetLaundryList()
        {
            List<LaundryClass> laundryList = new List<LaundryClass>();

            if (!TableExists() || Parse.ParseUser.CurrentUser == null)
            {
                return laundryList;
            }
            //Get active laundry
            string userId = Parse.ParseUser.CurrentUser.ObjectId;
          
            var laundries = from laundryTable in MyDevice.db.Table<LaundryClass>()
                            where laundryTable.EndUserId == userId

                                   select laundryTable;

            foreach (var laundry in laundries)
                laundryList.Add(laundry);

            return laundryList;
        }

        public static LaundryClass GetLaundry(int id)
        {
            LaundryClass laundry = null;

            if (!TableExists() || Parse.ParseUser.CurrentUser == null)
            {
                return null;
            }
            string userId = Parse.ParseUser.CurrentUser.ObjectId;

            var query = from laundryTable in MyDevice.db.Table<LaundryClass>()
                        where laundryTable.Id == id && laundryTable.EndUserId == userId

                        select laundryTable;

            foreach (var tempLaundry in query)
                laundry = tempLaundry;

            return laundry;
        }

        public static void ResetTable()
        {
            if (TableExists())
            {
                MyDevice.db.DropTable<LaundryClass>();
            }
        }
    }
}

#endif