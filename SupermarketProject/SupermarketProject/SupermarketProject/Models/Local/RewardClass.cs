﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace SupermarketProject.Models.Local
{
	[Table("Rewards")]
	public class RewardClass
	{
		[PrimaryKey]
		public string ObjectId { get; set; }
		public string Name { get; set; }
		public int Points { get; set; }
		public bool IsDeleted { get; set; }
	}
}
