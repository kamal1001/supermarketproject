﻿using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using SupermarketProject.Models.Local;
using System.Linq;
using Parse;

namespace SupermarketProject
{
	public class StampRewardClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand _tableExistCommand;
		public static void Initialize()
		{
			//MyDevice.db = new SQLiteConnection(DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			_tableExistCommand = MyDevice.db.CreateCommand(cmdText, "StampRewardTable");
		}
		private static bool TableExists()
		{
			return _tableExistCommand.ExecuteScalar<string>() != null;
		}

        public static void AddStampRewardList(List<StampRewardClass> stampRewardList)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<StampRewardClass>();
            }
            MyDevice.db.InsertAll(stampRewardList, "OR REPLACE", true);
        }

        public static void AddStampReward(StampRewardClass stampReward)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<StampRewardClass>();
            }
            MyDevice.db.Insert(stampReward);
        }

        public static void UpdateStampReward(StampRewardClass stampReward)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<StampRewardClass>();
            }

            StampRewardClass stampClass = (from a in MyDevice.db.Table<StampRewardClass>()
                                        where a.objectId == stampReward.objectId
                                        select a).SingleOrDefault();
            stampClass = stampReward;

            MyDevice.db.Update(stampClass);
        }


        public static List<StampRewardClass> GetStampRewards()
		{
			List<StampRewardClass> stampRewardList;

			if (!TableExists())
			{
				stampRewardList = new List<StampRewardClass>();
				return stampRewardList;

			}

            string userId = ParseUser.CurrentUser.ObjectId;
            stampRewardList = (from a in MyDevice.db.Table<StampRewardClass>()
		        where a.UserId == userId select a).ToList();


            return stampRewardList;
		}

        public static void RemoveStamp(string objectId)
        {
            if (!TableExists())
            {
                return;
            }

            StampRewardClass stampRewardClass = (from x in MyDevice.db.Table<StampRewardClass>() where x.objectId == objectId select x).FirstOrDefault();

            if(stampRewardClass != null)
                MyDevice.db.Delete(stampRewardClass);
        }

        public static void RemoveAllStamps()
        {
            if (!TableExists())
            {
                return;
            }

            MyDevice.db.DeleteAll<StampRewardClass>();
        }

        public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<StampRewardClass>();
			}
		}
	}
}
