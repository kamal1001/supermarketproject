﻿using System;
using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using Xamarin.Forms;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Models.Local
{
    public class UserClassFunctions
    {
        //private static SQLiteConnection db;
        private static SQLiteCommand tableExistCommand;
        public static int ActiveBranch = -1;
        public static void Initialize()
        {
            //db = new SQLiteConnection (DBConstants.DB_PATH);
            string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
            tableExistCommand = MyDevice.db.CreateCommand(cmdText, "User");
        }

        public static bool TableExists()
        {
            return tableExistCommand.ExecuteScalar<string>() != null;
        }

        public static bool CreateUserTable()
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);
            if (!TableExists())
            {
                System.Diagnostics.Debug.WriteLine("!CreateUserTable2");

                MyDevice.db.CreateTable<UserClass>();
                var newUser = new UserClass();
                newUser.ActiveBranch = -1;
                newUser.Id = 1;
                newUser.ImagesUpdateDate = ReleaseConfig.LAST_UPDATEDATE;
                newUser.CategoriesUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.ProductsUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.PromotionsUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.RewardsUpdateDate = DateTime.MinValue;
                newUser.BranchesUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.StatesUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.AreasUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.BranchBuildingUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.BuildingsUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.StampsUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.UserStampsUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.AlertsUpdateDate = new DateTime?(DateTime.MinValue);
                newUser.DBVersion = ReleaseConfig.DB_VERSION;
                newUser.ReleaseVersion = ReleaseConfig.RELEASE_VERSION;
                //app version fix.
                newUser.AppVersion = DependencyService.Get<INativeMethods>().GetVersionCode();
                //newUser.Location = "";
                MyDevice.db.InsertOrReplace(newUser);
                //db.Close ();
                return true;
            }

            //db.Close ();
            return false;
        }

        #region UserLocation

        public static void AddActiveBranchToUser(int activeBranch)
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            if (!TableExists())
            {
                //db.Close ();
                return;
            }
            ActiveBranch = activeBranch;
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.ActiveBranch = activeBranch;
            MyDevice.db.InsertOrReplace(user);

            //db.Close ();
        }

        public static int GetActiveBranchFromUser()
        {
            if (ActiveBranch != -2)
                return ActiveBranch;
            //var db = new SQLiteConnection (DBConstants.DB_PATH);
            try
            {
                if (!TableExists())
                    return -1;

                List<UserClass> userList = MyDevice.db.Query<UserClass>("select ActiveBranch from User WHERE _id = 1");
                UserClass user = userList[0];

                //db.Close ();
                return user.ActiveBranch;
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return -1;
            }
        }
        #endregion

        #region ImagesUpdatesDate
        public static void AddImagesUpdateDateToUser(DateTime? updatedAt)
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            if (!TableExists())
            {
                //db.Close ();
                return;
            }

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.ImagesUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);

            //db.Close ();
        }

        public static DateTime? GetImageUpdatedDateFromUser()
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select ImagesUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];

            //db.Close ();
            return user.ImagesUpdateDate;
        }
        #endregion

        #region CategoriesUpdateDate
        public static void AddCategoriesUpdateDateToUser(DateTime? updatedAt)
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            if (!TableExists())
            {
                //MyDevice.db.Close ();
                return;
            }

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.CategoriesUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);

            //db.Close ();
        }

        public static DateTime? GetCategoriesUpdatedDateFromUser()
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select CategoriesUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];

            //db.Close ();
            return user.CategoriesUpdateDate;
        }
        #endregion

        #region ProductsUpdateDate
        public static void AddProductsUpdateDateToUser(DateTime? updatedAt)
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            if (!TableExists())
            {
                //db.Close ();
                return;
            }

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.ProductsUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);

            //db.Close ();
        }

        public static DateTime? GetProductsUpdatedDateFromUser()
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select ProductsUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];

            //db.Close ();
            return user.ProductsUpdateDate;
        }
        #endregion

        #region PromotionsUpdateDate
        public static void AddPromotionsUpdateDateToUser(DateTime? updatedAt)
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            if (!TableExists())
            {
                //db.Close ();
                return;
            }

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.PromotionsUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);

            //db.Close ();
        }

        public static DateTime? GetPromotionsUpdatedDateFromUser()
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select PromotionsUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];

            //db.Close ();
            return user.PromotionsUpdateDate;
        }
        #endregion

        #region RewardsUpdateDate

        public static void AddRewardsUpdateDateToUser(DateTime? updatedAt)
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            if (!TableExists())
            {
                //db.Close ();
                return;
            }

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.RewardsUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);

            //db.Close ();
        }

        public static DateTime? GetRewardsUpdatedDateFromUser()
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select RewardsUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];

            //db.Close ();
            return user.RewardsUpdateDate;
        }

        #endregion

        #region BranchesUpdateDate
        public static void AddBranchesUpdateDateToUser(DateTime? updatedAt)
        {
            if (!TableExists())
            {
                return;
            }
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.BranchesUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);
        }

        public static DateTime? GetBranchesUpdatedDateFromUser()
        {
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select BranchesUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];
            return user.BranchesUpdateDate;
        }
        #endregion

        #region StatesUpdateDate
        public static void AddStatesUpdateDateToUser(DateTime? updatedAt)
        {
            if (!TableExists())
            {
                return;
            }
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.StatesUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);
        }

        public static DateTime? GetStatesUpdatedDateFromUser()
        {
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select StatesUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];
            return user.StatesUpdateDate;
        }
        #endregion

        #region AreasUpdateDate
        public static void AddAreasUpdateDateToUser(DateTime? updatedAt)
        {
            if (!TableExists())
            {
                return;
            }
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.AreasUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);
        }

        public static DateTime? GetAreasUpdatedDateFromUser()
        {
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select AreasUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];
            return user.AreasUpdateDate;
        }
        #endregion

        #region BranchBuildingUpdateDate
        public static void AddBranchBuildingUpdateDateToUser(DateTime? updatedAt)
        {
            if (!TableExists())
            {
                return;
            }
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.BranchBuildingUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);
        }

        public static DateTime? GetBranchBuildingUpdatedDateFromUser()
        {
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select BranchBuildingUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];
            return user.BranchBuildingUpdateDate;
        }
        #endregion

        #region BuildingsUpdateDate
        public static void AddBuildingsUpdateDateToUser(DateTime? updatedAt)
        {
            if (!TableExists())
            {
                return;
            }
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.BuildingsUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);
        }

        public static DateTime? GetBuildingsUpdatedDateFromUser()
        {
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select BuildingsUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];
            return user.BuildingsUpdateDate;
        }
        #endregion

        #region StampsUpdateDate
        public static void AddStampsUpdateDateToUser(DateTime? updatedAt)
        {
            if (!TableExists())
            {
                return;
            }
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.StampsUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);
        }

        public static DateTime? GetStampsUpdatedDateFromUser()
        {
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select StampsUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];
            return user.StampsUpdateDate;
        }
        #endregion

        #region UserStampsUpdateDate
        public static void AddUserStampsUpdateDateToUser(DateTime? updatedAt)
        {
            if (!TableExists())
            {
                return;
            }
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.UserStampsUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);
        }

        public static DateTime? GetUserStampsUpdatedDateFromUser()
        {
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select UserStampsUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];
            return user.UserStampsUpdateDate;
        }
        #endregion

        #region StampRewardsUpdateDate
#if Stamp
        public static void AddStampRewardsUpdateDateToUser(DateTime? updatedAt)
        {
            if (!TableExists())
            {
                return;
            }
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.StampRewardsUpdateDate = updatedAt;
            MyDevice.db.InsertOrReplace(user);
        }

        public static DateTime? GetStampRewardsUpdatedDateFromUser()
        {
            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select StampRewardsUpdateDate from User WHERE _id = 1");
            UserClass user = userList[0];
            return user.StampRewardsUpdateDate;
        }
#endif
        #endregion

        #region AlertsUpdateDate
        public static void AddAlertUpdateDateToUser(DateTime? updatedAt)
		{
			if (!TableExists())
			{
				return;
			}
			List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
			UserClass user = userList[0];

			user.AlertsUpdateDate = updatedAt;
			MyDevice.db.InsertOrReplace(user);
		}

		public static DateTime? GetAlertsUpdatedDateFromUser()
		{
			List<UserClass> userList = MyDevice.db.Query<UserClass>(" select AlertsUpdateDate from User WHERE _id = 1");
			UserClass user = userList[0];
			return user.AlertsUpdateDate;
		}
#endregion

#region DBVersion
		public static string GetDBVersion()
		{
			//var db = new SQLiteConnection (DBConstants.DB_PATH);

			List<UserClass> userList = MyDevice.db.Query<UserClass>(" select DBVersion from User WHERE _id = 1");
			UserClass user = userList[0];

			//db.Close ();
			return user.DBVersion;
		}
#endregion

#region ReleaseVersion
		public static int GetReleaseVersion()
		{
			//var db = new SQLiteConnection (DBConstants.DB_PATH);

			List<UserClass> userList = MyDevice.db.Query<UserClass>(" select ReleaseVersion from User WHERE _id = 1");
			UserClass user = userList[0];

			//db.Close ();
			return user.ReleaseVersion;
		}
#endregion

#region AppVersion
        public static void AddAppVersionToUser(string appVersion)
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);

            if (!TableExists())
            {
                //db.Close ();
                return;
            }

            List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
            UserClass user = userList[0];

            user.AppVersion = appVersion;
            MyDevice.db.InsertOrReplace(user);

            //db.Close ();
        }

        public static string GetAppVersionFromUser()
        {
            //var db = new SQLiteConnection (DBConstants.DB_PATH);
            if (!TableExists())
                return "-1";
            UserClass user;

            try
            {
                List<UserClass> userList = MyDevice.db.Query<UserClass>(" select AppVersion from User WHERE _id = 1");
                user = userList[0];
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                System.Diagnostics.Debug.WriteLine(e.ToString());
                return "-1";
            }


            //db.Close ();
            return user.AppVersion;
        }
#endregion

		public static void AddUserInfo(int activeBranch)
		{
			//var db = new SQLiteConnection (DBConstants.DB_PATH);

			if (!TableExists())
			{
				MyDevice.db.CreateTable<UserClass>();
			}

			List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User ");
			UserClass user = userList[0];

			user.ActiveBranch = activeBranch;

			MyDevice.db.InsertOrReplace(user);

			//db.Close ();
		}

		public static UserClass GetUser()
		{
			//var db = new SQLiteConnection (DBConstants.DB_PATH);
			UserClass user = null;
			List<UserClass> userList = MyDevice.db.Query<UserClass>(" select * from User WHERE _id = 1");
			if (userList.Count > 0)
				user = userList[0];
			//db.Close ();

			return user;
		}

		//For Development Purposes
		public static void ResetTable()
		{
			//var db = new SQLiteConnection (DBConstants.DB_PATH);
			if (TableExists())
			{
				MyDevice.db.DropTable<UserClass>();
			}
			//db.Close ();
		}
	}
}