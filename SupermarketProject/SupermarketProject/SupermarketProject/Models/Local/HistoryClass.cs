﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using SupermarketProject.Models.Local.Helpers;
using SupermarketProject.Models.Remote;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Models.Local
{
    public class HistoryClass : IHistory, IOrder
    {

        public HistoryClass(string objetId, List<string> productOrderList, List<string> historyProducts, string address, string addressDesc, string addressLine3, string name,
                             string surname, string phone, string date, string branch, int earnedPoints, string updatedDate, OrderModel.OrderStatus orderStatus, string createdDate, string approveDate,
                            string dispatchDate, int totalProductCount, int change, string finishDate, double pointDiscount)
        {
            ObjectId = objetId;
            ProductOrderList = productOrderList;
            Date = date;
            Branch = branch;
            Address = address;
            AddressDescription = addressDesc;
            HistoryProducts = historyProducts;
            AddressLine3 = addressLine3;
            Name = name;
            Surname = surname;
            Phone = phone;
            EarnedPoints = earnedPoints;
            UpdateDate = updatedDate;
            OrderStatus = orderStatus;
            CreatedDate = createdDate;
            DispatchDate = dispatchDate;
            ApproveDate = approveDate;
            TotalProductCount = totalProductCount;
            Change = change;
            FinishDate = finishDate;
            PointDiscount = pointDiscount;
            //System.Diagnostics.Debug.WriteLine ("History " + surname);

            SetPrices();
        }

        private void SetPrices()
        {
            TotalPrice = 0.0f;
            TotalDiscount = 0;
            TotalVat = 0;
            try
            {
                foreach (string order in ProductOrderList)
                {
                    var orderPrice = Double.Parse(order.Split(';')[3].Split(':')[1]);
                    var isAvailable = Convert.ToBoolean(order.Split(';')[4].Split(':')[1]);
                    var quantity = Int32.Parse(order.Split(';')[6].Split(':')[1]);
                    var perItemPrice = Double.Parse(order.Split(';')[7].Split(':')[1]);
                    var discountedItemPrice = Double.Parse(order.Split(';')[8].Split(':')[1]);
                    var vat = Double.Parse(order.Split(';')[9].Split(':')[1]);
                    var orderCanceled = Convert.ToBoolean((order.Split(';')[10]).Split(':')[1]);
                    if (isAvailable && !orderCanceled)
                    {
                        TotalPrice += orderPrice;
                        TotalDiscount += discountedItemPrice == 0 ? 0 : (perItemPrice - discountedItemPrice) * quantity;
                        TotalVat += discountedItemPrice == 0 ? (perItemPrice * vat / 100) * quantity : (discountedItemPrice * vat / 100) * quantity;
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                System.Diagnostics.Debug.WriteLine(ex);
            }

        }

        public string ObjectId { get; set; }
        public List<string> ProductOrderList;
        public double TotalPrice { get; set; }
        public string Date { get; set; }
        public string Branch { get; set; }
        public string Address { get; set; }
        public string AddressDescription { get; set; }
        public List<string> HistoryProducts { get; set; }
        public string AddressLine3 { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
        public int EarnedPoints { get; set; }

        public string CreatedDate { get; set; }
        public string ApproveDate { get; set; }
        public string DispatchDate { get; set; }

        public string FinishDate { get; set; }

        public string UpdateDate { get; set; }
        public OrderModel.OrderStatus OrderStatus { get; set; }
        public int TotalProductCount { get; set; }
        public int Change { get; set; }
        public double PointDiscount { get; set; }
        public Double TotalDiscount { get; set; }
        public Double TotalVat { get; set; }

    }
}

