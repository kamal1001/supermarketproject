﻿using System;
using SQLite;
using SupermarketProject.Common.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace SupermarketProject.Models.Local
{
	[Table("Images")]
	public class ImageClass
	{

		[PrimaryKey]
		public string objectId { get; set; }
		public string Url { get; set; }
	}
}
