﻿using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using SupermarketProject.Models.Local;
using System.Linq;

namespace SupermarketProject
{
	public class StateClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand _tableExistCommand;
		public static void Initialize()
		{
			//MyDevice.db = new SQLiteConnection(DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			_tableExistCommand = MyDevice.db.CreateCommand(cmdText, "StateTable");
		}
		private static bool TableExists()
		{
			return _tableExistCommand.ExecuteScalar<string>() != null;
		}

        public static void AddState(List<StateClass> stateList)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<StateClass>();
            }
            MyDevice.db.InsertAll(stateList, "OR REPLACE", true);
        }


        public static List<StateClass> GetStates()
		{
			List<StateClass> stateList;

			if (!TableExists())
			{
				stateList = new List<StateClass>();
				return stateList;

			}
			stateList = MyDevice.db.Table<StateClass>().Where(x => !x.IsDeleted).OrderBy(p => p.StateName).ToList();

			return stateList;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<StateClass>();
			}
		}
	}
}
