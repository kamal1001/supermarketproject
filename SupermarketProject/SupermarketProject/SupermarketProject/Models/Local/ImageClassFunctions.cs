﻿using SQLite;
using SupermarketProject.Common.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace SupermarketProject.Models.Local
{
	public class ImageClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand tableExistCommand;
		public static void Initialize()
		{
			//db = new SQLiteConnection(DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "Images");
		}
		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}
		public static void AddImage(List<ImageClass> imageList)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<ImageClass>();
			}
			//System.Diagnostics.Debug.WriteLine (categoryList.Count);

			MyDevice.db.InsertAll(imageList, "OR REPLACE", true);
		}

		public static List<ImageClass> GetImages()
		{
			List<ImageClass> imageList;

			if (!TableExists())
			{
				imageList = new List<ImageClass>();
				return imageList;
			}
			imageList = MyDevice.db.Table<ImageClass>()?.ToList();

			return imageList;
		}
		public static void Remove(ImageClass image)
		{
			if (!TableExists())
			{
				return;
			}
			//MyDevice.db.Table<ImageClass>();
			MyDevice.db.Delete(image);
		}
		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<ImageClass>();
			}
		}
	}
}