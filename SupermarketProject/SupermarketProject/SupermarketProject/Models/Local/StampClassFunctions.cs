﻿using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using SupermarketProject.Models.Local;
using System.Linq;

namespace SupermarketProject
{
	public class StampClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand _tableExistCommand;
		public static void Initialize()
		{
			//MyDevice.db = new SQLiteConnection(DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			_tableExistCommand = MyDevice.db.CreateCommand(cmdText, "StampTable");
		}
		private static bool TableExists()
		{
			return _tableExistCommand.ExecuteScalar<string>() != null;
		}

        public static void AddStamp(List<StampClass> stampList)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<StampClass>();
            }
            MyDevice.db.InsertAll(stampList, "OR REPLACE", true);
        }


        public static List<StampClass> GetStamps()
		{
			List<StampClass> stampList;

			if (!TableExists())
			{
				stampList = new List<StampClass>();
				return stampList;

			}
			stampList = MyDevice.db.Table<StampClass>().OrderBy(p => p.Title).ToList();

			return stampList;
		}

        public static bool RemoveStamp(string objectId)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<StampClass>();
            }

            StampClass tempStamp = (from a in MyDevice.db.Table<StampClass>()
                                            where a.objectId == objectId
                                            select a).SingleOrDefault();
            if (tempStamp?.objectId == null || tempStamp.objectId != objectId)
                return false;

            MyDevice.db.Delete(tempStamp);

            return true;
        }

        public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<StampClass>();
			}
		}
	}
}
