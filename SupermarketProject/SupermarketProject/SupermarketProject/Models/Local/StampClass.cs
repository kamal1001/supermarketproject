﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using SQLite;

namespace SupermarketProject.Models.Local
{
	[Table("StampTable")]
	public class StampClass : INotifyPropertyChanged
    {
	    

	    [PrimaryKey]
		public string objectId { get; set; }
		public string Title { get; set; }
        public string Description { get; set; }
        public string ImageId { get; set; }
        public int Count { get; set; }
        //1 for category 
        //2 for product
        public string StampType { get; set; }
        public string StampVariables { get; set; }

	    private int _changeTriggerCounter = 0;
        public int ChangeTriggerCounter
	    {
	        get { return _changeTriggerCounter; }
            set
            {
                if (_changeTriggerCounter == value)
                    return;

                _changeTriggerCounter = value;
                OnPropertyChanged();
            }
        }

	    public event PropertyChangedEventHandler PropertyChanged;

	    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
	    {
	        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	    }
    }
}

