﻿using System;
using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;

namespace SupermarketProject.Models.Local
{
    [Table("User")]
    public class UserClass
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        public int ActiveBranch { get; set; }
        public DateTime? ImagesUpdateDate { get; set; }
        public DateTime? CategoriesUpdateDate { get; set; }
        public DateTime? ProductsUpdateDate { get; set; }
        public DateTime? BranchesUpdateDate { get; set; }
        public DateTime? StatesUpdateDate { get; set; }
        public DateTime? AreasUpdateDate { get; set; }
        public DateTime? BranchBuildingUpdateDate { get; set; }
        public DateTime? BuildingsUpdateDate { get; set; }
        public DateTime? StampsUpdateDate { get; set; }
        public DateTime? UserStampsUpdateDate { get; set; }
#if Stamp
        public DateTime? StampRewardsUpdateDate { get; set; }
#endif
        public DateTime? AlertsUpdateDate { get; set; }
		public DateTime? PromotionsUpdateDate { get; set; }
		public DateTime? RewardsUpdateDate { get; set; }
		public string DBVersion { get; set; }
		public int ReleaseVersion { get; set; }
        public string AppVersion { get; set; }
		//public string Location{ get; set; }


	}

}

