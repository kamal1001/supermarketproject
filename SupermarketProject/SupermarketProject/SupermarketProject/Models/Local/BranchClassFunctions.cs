﻿using System;
using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using SupermarketProject.Models.Local;
using System.Linq;

namespace SupermarketProject
{
	public class BranchClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand tableExistCommand;
		public static void Initialize()
		{
			//MyDevice.db = new SQLiteConnection(DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "Branches");
		}
		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}
		public static void AddBranch(List<BranchClass> branchList)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<BranchClass>();
			}
			MyDevice.db.InsertAll(branchList, "OR REPLACE", true);
		}

		public static List<BranchClass> GetBranches()
		{
			List<BranchClass> branchList;

			if (!TableExists())
			{
				branchList = new List<BranchClass>();
				return branchList;

			}
            branchList = MyDevice.db.Table<BranchClass>().Where(x => !x.IsDeleted).OrderBy(p => p.ID).ToList();

            return branchList;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<BranchClass>();
			}
		}
	}
}
