﻿using System.Collections.Generic;
using System.Linq;
using SupermarketProject.Common.Utilities;
using SQLite;

namespace SupermarketProject.Models.Local
{
    public class ProductBranchInfoClassFunctions
    {
        private static SQLiteCommand tableExistCommand;
        public static void Initialize()
        {
            string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
            tableExistCommand = MyDevice.db.CreateCommand(cmdText, "ProductBranchInfos");
        }

        private static bool TableExists()
        {
            return tableExistCommand.ExecuteScalar<string>() != null;
        }

        public static void AddProductBranchInfos(List<ProductBranchInfoClass> productBranchInfoList)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<ProductBranchInfoClass>();
            }
            MyDevice.db.InsertAll(productBranchInfoList, "OR REPLACE", true);
        }

        public static List<ProductBranchInfoClass> GetProductBranchInfosByBranch(int branchIndex)
        {
            List<ProductBranchInfoClass> productBranchInfoList;

            if (!TableExists())
            {
                productBranchInfoList = new List<ProductBranchInfoClass>();
                return productBranchInfoList;

            }
            productBranchInfoList = MyDevice.db.Table<ProductBranchInfoClass>().Where(p => !p.IsDeleted && p.BranchIndex == branchIndex).ToList();
            
            return productBranchInfoList;
        }

        public static void ResetTable()
        {
            if (TableExists())
            {
                MyDevice.db.DropTable<ProductBranchInfoClass>();
            }
        }
    }
}

