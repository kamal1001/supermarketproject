﻿using System;
using SQLite;
using System.Linq;
using SupermarketProject.Common.Utilities;
using System.Collections.Generic;
using SupermarketProject.Models.Remote;
using Com.OneSignal;

namespace SupermarketProject.Models.Local
{
	public class AddressClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand tableExistCommand;
		public static void Initialize()
		{
			//db = new SQLiteConnection (DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "AddressTable");
		}
		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}

		public static void AddAddressList(List<AddressClass> addressList)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<AddressClass>();
			}
			MyDevice.db.InsertAll(addressList, "OR REPLACE", true);
		}

		public static void AddAddress(AddressClass address)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<AddressClass>();
			}

			MyDevice.db.Insert(address);

		}

		public static void RemoveAdress(AddressClass addre)
		{
			if (!TableExists())
			{
				return;
			}

			MyDevice.db.Delete(addre);

		}

		public static void UpdateAddress(AddressClass address)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<AddressClass>();
			}

			AddressClass tempAddress = (from a in MyDevice.db.Table<AddressClass>()
										where a.Id == address.Id
										select a).SingleOrDefault();
			tempAddress = address;

			MyDevice.db.Update(tempAddress);
		}

		public static List<AddressClass> GetAddressList(int branchId)
		{
			List<AddressClass> addressList = new List<AddressClass>();

			if (!TableExists()|| Parse.ParseUser.CurrentUser == null)
			{
				return addressList;
			}
			//Get active address
			string userId = Parse.ParseUser.CurrentUser.ObjectId;

            var activeAddresses = (from a in MyDevice.db.Table<AddressClass>()
                                   where a.IsActive == true && a.UserID == userId && a.BranchId != null && a.IsDeleted == false
                                   select a);

            AddressClass activeAddress = activeAddresses?.SingleOrDefault(a => BuildingModel.BuildingList.Any(y => y.objectId == a.BuildingId) && BranchHelper.BranchList.Any(y => y.ID == a.BranchId));

            if (activeAddress != null)
				addressList?.Add(activeAddress);

			var passiveAddresses = (from AddressTable in MyDevice.db.Table<AddressClass>()
								   where AddressTable.IsActive == false && AddressTable.UserID == userId
                                   && AddressTable.BranchId != null && AddressTable.IsDeleted == false
                                   select AddressTable)?.ToList();

            passiveAddresses = passiveAddresses?.Where(a => BuildingModel.BuildingList.Any(y => y.objectId == a.BuildingId) && BranchHelper.BranchList.Any(y => y.ID == a.BranchId))?.ToList();

            foreach (var tempAdd in passiveAddresses)
				addressList?.Add(tempAdd);

            if(activeAddress == null && addressList != null &&  addressList.Count > 0)
				MakeActive(addressList[0]);

			return addressList;
		}

        public static List<AddressClass> GetAllAddresses()
        {
            List<AddressClass> addressList = new List<AddressClass>();

            if (!TableExists() || Parse.ParseUser.CurrentUser == null)
            {
                return addressList;
            }
            //Get active address
            string userId = Parse.ParseUser.CurrentUser.ObjectId;

            var activeAddresses = (from a in MyDevice.db.Table<AddressClass>()
                                          where a.IsActive == true && a.UserID == userId && a.BuildingId != null && a.ApartmentNumber != null && a.BranchId != null && a.IsDeleted == false
                                          select a);

            AddressClass activeAddress = activeAddresses?.SingleOrDefault(a => BuildingModel.BuildingList.Any(y => y.objectId == a.BuildingId) && BranchHelper.BranchList.Any(y => y.ID == a.BranchId));

            //Checking if the area is still being serviced by that branch
            if (activeAddress != null)
            {
                var currentBranch = BranchHelper.BranchList.Where(x => x.ID == activeAddress.BranchId).ToList()[0];
                var currentBuilding = BuildingModel.BuildingList.Where(y => (y.objectId == activeAddress.BuildingId)).ToList()[0];
                var areaExists = currentBranch.Areas.Any(z => z.AreaId == currentBuilding.AreaId);
                if (!areaExists)
                    RemoveAdress(activeAddress);
                else
                    addressList?.Add(activeAddress);
            }

            var passiveAddresses = (from AddressTable in MyDevice.db.Table<AddressClass>()
                                   where AddressTable.IsActive == false && AddressTable.UserID == userId && AddressTable.BuildingId != null && AddressTable.ApartmentNumber != null && AddressTable.BranchId != null && AddressTable.IsDeleted == false
                                   select AddressTable)?.ToList();

            passiveAddresses = passiveAddresses?.Where(a => BuildingModel.BuildingList.Any(y => y.objectId == a.BuildingId) && BranchHelper.BranchList.Any(y => y.ID == a.BranchId))?.ToList();

            foreach (var tempAdd in passiveAddresses)
                addressList?.Add(tempAdd);

            if (activeAddress == null && addressList != null && addressList.Count > 0)
                MakeActive(addressList[0]);

            return addressList;
        }

        public static void MakeActive(AddressClass address)
		{
			if (!TableExists()|| Parse.ParseUser.CurrentUser == null)
			{
				return;
			}
			string userId = Parse.ParseUser.CurrentUser.ObjectId;
			var query = from AddressTable in MyDevice.db.Table<AddressClass>()
						where AddressTable.IsActive && AddressTable.UserID == userId && AddressTable.BranchId != null

						select AddressTable;
			foreach (var tempAdd in query)
			{
				tempAdd.IsActive = false;
				MyDevice.db.InsertOrReplace(tempAdd);
			}

			address.IsActive = true;
			MyDevice.db.InsertOrReplace(address);

            if (address != null && Parse.ParseUser.CurrentUser != null)
            {
                if (address.BranchId.HasValue)
                    OneSignal.Current.SendTag("Branch", address.BranchId.Value.ToString());
                var building = BuildingModel.BuildingList.FirstOrDefault(x => x.objectId == address.BuildingId);
                if (building != null)
                {
                    OneSignal.Current.SendTag("Building", address.BuildingId);
                    var area = AreaModel.AreaList.FirstOrDefault(x => x.objectId == building.AreaId);
                    if (area != null)
                    {
                        OneSignal.Current.SendTag("Area", area.objectId);
                        OneSignal.Current.SendTag("City", area.StateId);
                    }
                }
            }
        }

        public static void UpdateLocalPhoneNumber(string newPhoneNumber)
        {
            if (!TableExists() || Parse.ParseUser.CurrentUser == null)
            {
                return;
            }
            string userId = Parse.ParseUser.CurrentUser.ObjectId;
            var query = from AddressTable in MyDevice.db.Table<AddressClass>()
                        where AddressTable.UserID == userId && AddressTable.BranchId != null

                        select AddressTable;
            foreach (var tempAdd in query)
            {
                tempAdd.PhoneNumber = newPhoneNumber;
                MyDevice.db.InsertOrReplace(tempAdd);
            }
        }

        public static AddressClass GetAddress(string id)
		{
			AddressClass address = null;

			if (!TableExists()|| Parse.ParseUser.CurrentUser == null)
			{
				return address;
			}
			string userId = Parse.ParseUser.CurrentUser.ObjectId;

			var query = (from AddressTable in MyDevice.db.Table<AddressClass>()
						where AddressTable.Id == id && AddressTable.UserID == userId && AddressTable.BranchId != null
                        select AddressTable)?.ToList();

		    query = query?.Where(a => BuildingModel.BuildingList.Any(y => y.objectId == a.BuildingId))?.ToList();

			foreach (var tempAdd in query)
				address = tempAdd;

			return address;
		}

		public static AddressClass GetActiveAddress()
		{
			AddressClass address = null;

			if (!TableExists() || Parse.ParseUser.CurrentUser==null)
			{
				return null;
			}
			string userId = Parse.ParseUser.CurrentUser.ObjectId;
			

			var query = (from AddressTable in MyDevice.db.Table<AddressClass>()
						where 
					AddressTable.IsActive == true &&
			        AddressTable.UserID == userId &&
                    AddressTable.BranchId != null &&
                    AddressTable.IsDeleted == false
                        select AddressTable)?.ToList();

		    query = query.Where(a => BuildingModel.BuildingList.Any(y => y.objectId == a.BuildingId))?.ToList();

            if (query.Any())
			{
				address = query?.First<AddressClass>();
			}
			return address;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<AddressClass>();
			}
		}
	}
}