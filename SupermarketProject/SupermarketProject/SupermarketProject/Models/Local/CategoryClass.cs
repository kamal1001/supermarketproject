﻿using System;
using SQLite;
using SupermarketProject.Common.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace SupermarketProject.Models.Local
{
	[Table("Categories")]
	public class CategoryClass
	{
		[PrimaryKey]
		public string objectId { get; set; }
		public string Sub { get; set; }
		public bool isSubCategory { get; set; }
		public string ImageID { get; set; }
		public string Name { get; set; }
		public int Priority { get; set; }
		public bool isInTheList { get; set; }
#if CategoryLevel3
		public string ParentCategory{ get; set; }
#endif
#if Loyalty
        public string LoyaltyRatio { get; set; }
#endif
    }
}

