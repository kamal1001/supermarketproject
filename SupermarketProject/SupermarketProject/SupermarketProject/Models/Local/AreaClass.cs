﻿using SQLite;

namespace SupermarketProject.Models.Local
{
	[Table("AreaTable")]
    public class AreaClass : GeneralPopUpItemModel
	{		
		[PrimaryKey]
		public string objectId { get; set; }
        private string _areaName;
        public string AreaName
        {
            get
            {
                return _areaName;
            }

            set
            {
                _areaName = value;
                base.Name = value;
            }
        }

        public string StateId { get; set; }
	    public bool IsDeleted { get; set; }
    }
}



