﻿using System;
using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using SupermarketProject.Models.Remote;

namespace SupermarketProject.Models.Local
{
	public class FavoritiesClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand tableExistCommand;
		public static void Initialize()
		{
			//db = new SQLiteConnection (DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "Favorites");
		}

		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}

	    public static void AddProductList(List<string> productIdList)
	    {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<FavoritesClass>();
            }

	        if (productIdList.Count == 1 && string.IsNullOrEmpty(productIdList[0]))
	            return;

            List<FavoritesClass> favoriteList = new List<FavoritesClass>();
	        foreach (var productId in productIdList)
	        {
                favoriteList.Add(new FavoritesClass
                {
                    productID = productId
                });
            }
           

            MyDevice.db.InsertAll(favoriteList, "OR REPLACE", true);

            FavoriteModel.PopulateFavoriteDictionaries();
        }

	    public static void AddProductID(string productID)
		{

			if (!TableExists())
			{
				MyDevice.db.CreateTable<FavoritesClass>();
			}

			FavoritesClass newFavorite = new FavoritesClass();
			newFavorite.productID = productID;
			MyDevice.db.InsertOrReplace(newFavorite);

		}

		public static void RemoveProductID(string productID)
		{
			if (!TableExists())
			{
				return;
			}

			MyDevice.db.Table<FavoritesClass>();

			FavoritesClass favoriteToBeRemoved = new FavoritesClass();
			favoriteToBeRemoved.productID = productID;
			MyDevice.db.Delete(favoriteToBeRemoved);
		}

	    public static string GetProductIdString()
	    {
	        string productIdString = "";
            var favorities = GetProductIDs();
            if (favorities == null)
                return productIdString;
            foreach (var favorite in favorities)
            {
                productIdString += favorite.productID;
                if (favorities.LastOrDefault().productID != favorite.productID)
                    productIdString += ",";
            }
	        return productIdString;
	    }

	    public static TableQuery<FavoritesClass> GetProductIDs()
		{
			//List<string> productIDList = new List<string> ();

			if (!TableExists())
			{
				return null;
			}
			else
				return MyDevice.db.Table<FavoritesClass>();
			/*
			var favoritesTable = db.Table<FavoritesClass> ();

			foreach (var favorite in favoritesTable) {
				productIDList.Add (favorite.productID);
			}

			return productIDList;*/
		}
		/*
		public static bool IsProductFavorite( string productID )
		{
			if (!TableExists()) {
				return false;
			}

			var queryResult = db.Query<FavoritesClass> ("SELECT * FROM Favorites WHERE productID = ?",productID );

			if (queryResult.Count == 0)
				return false;
			else
				return true;			
		}
*/
		//For Development Purposes
		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<FavoritesClass>();
			}
		}
	}
}
