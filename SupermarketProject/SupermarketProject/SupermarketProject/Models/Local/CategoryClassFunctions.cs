﻿using System;
using SQLite;
using SupermarketProject.Common.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace SupermarketProject.Models.Local
{
	public class CategoryClassFunctions
	{
		private static SQLiteCommand tableExistCommand;
		public static void Initialize()
		{
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			tableExistCommand = MyDevice.db.CreateCommand(cmdText, "Categories");
		}
		private static bool TableExists()
		{
			return tableExistCommand.ExecuteScalar<string>() != null;
		}
		public static void AddCategory(List<CategoryClass> categoryList)
		{
			if (!TableExists())
			{
				MyDevice.db.CreateTable<CategoryClass>();
			}
			MyDevice.db.InsertAll(categoryList, "OR REPLACE", true);
		}

		public static List<CategoryClass> GetCategories()
		{
			List<CategoryClass> categoryList;

			if (!TableExists())
			{
				categoryList = new List<CategoryClass>();
				return categoryList;
			}
            categoryList = MyDevice.db.Table<CategoryClass>().Where(p => p.isInTheList).OrderBy(p => p.Priority).ThenBy(p => p.Name)?.ToList();

			return categoryList;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<CategoryClass>();
			}
		}
	}
}
