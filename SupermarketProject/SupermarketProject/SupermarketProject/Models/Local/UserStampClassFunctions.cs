﻿using SupermarketProject.Common.Utilities;
using SQLite;
using System.Collections.Generic;
using SupermarketProject.Models.Local;
using System.Linq;
using Parse;

namespace SupermarketProject
{
	public class UserStampClassFunctions
	{
		//private static SQLiteConnection db;
		private static SQLiteCommand _tableExistCommand;
		public static void Initialize()
		{
			//MyDevice.db = new SQLiteConnection(DBConstants.DB_PATH);
			string cmdText = "SELECT name FROM sqlite_master WHERE type='table' AND name=?";
			_tableExistCommand = MyDevice.db.CreateCommand(cmdText, "UserStampTable");
		}
		private static bool TableExists()
		{
			return _tableExistCommand.ExecuteScalar<string>() != null;
		}

        public static void AddUserStampList(List<UserStampClass> userStampList)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<UserStampClass>();
            }
            MyDevice.db.InsertAll(userStampList, "OR REPLACE", true);
        }

        public static void AddUserStamp(UserStampClass userStamp)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<UserStampClass>();
            }
            MyDevice.db.Insert(userStamp);
        }

        public static void UpdateUserStamp(UserStampClass userStamp)
        {
            if (!TableExists())
            {
                MyDevice.db.CreateTable<UserStampClass>();
            }

            UserStampClass stampClass = (from a in MyDevice.db.Table<UserStampClass>()
                                        where a.objectId == userStamp.objectId
                                        select a).SingleOrDefault();
            stampClass = userStamp;

            MyDevice.db.Update(stampClass);
        }


        public static List<UserStampClass> GetUserStamps()
		{
			List<UserStampClass> userStampList;

			if (!TableExists())
			{
				userStampList = new List<UserStampClass>();
				return userStampList;

			}

            string userId = Parse.ParseUser.CurrentUser.ObjectId;
            userStampList = (from a in MyDevice.db.Table<UserStampClass>()
		        where a.UserId == userId select a).ToList();


            return userStampList;
		}

		public static void ResetTable()
		{
			if (TableExists())
			{
				MyDevice.db.DropTable<UserStampClass>();
			}
		}
	}
}
