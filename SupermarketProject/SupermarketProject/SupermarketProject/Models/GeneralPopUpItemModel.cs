﻿using System;
using SQLite;

namespace SupermarketProject
{
    public abstract class GeneralPopUpItemModel
    {
        [Ignore]
        public string Name { get; set; }

        [Ignore]
        public bool IsSelected { get; set; }
    }
}
