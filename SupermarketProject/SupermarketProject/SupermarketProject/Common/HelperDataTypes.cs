﻿using System;
using System.Collections.Generic;

namespace SupermarketProject.Common
{
	public class PinPoint
	{
		public double lat;
		public double lng;

		public PinPoint(double l1,double l2)
		{
			lat = l1;
			lng = l2;
		}
		public PinPoint()
		{
		}
	}
	public class PinData
	{
		public double lat;
		public double lng;
		public string icon;
		public string text;
		public List<PinPoint> pinPoints;
		public PinData (double lat,double lng,string icon,string text)
		{
			this.lat = lat;
			this.lng = lng;
			this.icon = icon;
			this.text = text;
		}
	}
}

