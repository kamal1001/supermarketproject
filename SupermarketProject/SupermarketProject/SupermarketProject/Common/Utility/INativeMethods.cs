﻿using System;

using System.Threading.Tasks;
using Xamarin.Forms;

namespace SupermarketProject
{
	public interface INativeMethods
	{
		void CloseApp();
		bool FileExist(string image);
		void Logg(string logg);
		string GetImageDownloadPath();
		string GetFileNameFromURL(string url);
	    void ChangeStatusBarColor(Color color);
		string GetTimeURL();
	    string GetVersionCode();
        //void RegisterNotification();
        void DeleteCache();
        void CloseKeyboard();
        bool IsKeyboardHidden();
        void share(string title, string content);
        void StartCartNotificationService();
        void StopCartNotificationService();
        void RateUs();
    }
}

