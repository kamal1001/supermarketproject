﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace SupermarketProject
{
    public static class LocalTime
    {
        private static string _timeUrl = "https://api.timezonedb.com/?zone=Asia%2FDubai&format=json&key=3KQP2BBMXURP";
        private static DateTimeOffset _initialTime;
        private static Stopwatch _stopwatch;

        public static DateTimeOffset Time => _initialTime.Add(_stopwatch.Elapsed);
        public static TimeSpan Offset = TimeSpan.FromHours(4);

        public static string NonMilitaryTimeFormat(TimeSpan time)
        {
            return DateTime.Today.Add(time).ToString("hh:mm tt", CultureInfo.InvariantCulture);
        }

        public static async Task Initialize()
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    client.Timeout = new TimeSpan(0, 0, 5);
                    string response = await client.GetStringAsync(_timeUrl);
                    JObject jObj = JObject.Parse(response);
                    int timeStamp = (int)jObj["timestamp"];
                    Offset = TimeSpan.FromSeconds((int)jObj["gmtOffset"]);

                    if (timeStamp < 500000)
                        _initialTime = DateTimeOffset.Now;
                    DateTimeOffset date = new DateTimeOffset(1970, 1, 1, 0, 0, 0, 0, Offset);

                    date = date.AddSeconds(timeStamp);

                    _initialTime = date;
                }
                catch
                {
                    _initialTime = DateTimeOffset.Now;
                }
                finally
                {
                    _stopwatch = new Stopwatch();
                    _stopwatch.Start();
                }
            }
        }
    }
}

