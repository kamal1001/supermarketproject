﻿namespace SupermarketProject.Common.Utilities
{
	public static class ParseConstants
	{
		//Class Names
		public const string CATEGORIES_CLASS_NAME = "Categories";
		public const string IMAGES_CLASS_NAME = "Images";
	    public const string PRODUCTS_CLASS_NAME = "Products";
	    public const string PRODUCTBRANCHINFOS_CLASS_NAME = "ProductBranchInfos";
        public const string PROMOTIONS_CLASS_NAME = "Promotion";
		public const string REWARDS_CLASS_NAME = "Rewards";
		public const string ORDERS_CLASS_NAME = "Orders";
		public const string DELIVERYSTAFF_CLASS_NAME = "DeliveryStaff";
		public const string SETTINGS_CLASS_NAME = "Settings";
		public const string SUGGESTIONS_CLASS_NAME = "SuggestedProducts";
        public const string FeedbackClassName = "Feedback";
        public const string CustomersClassName = "Customers";
		public const string AddressClassName = "UserAddress";
		public const string EndUserClassName = "EndUser";
        public const string STAMPS_CLASS_NAME = "Stamps";
        public const string USERSTAMPS_CLASS_NAME = "UserStamps";
        public const string STAMPREWARDS_CLASS_NAME = "StampRewards";
#if Laundry
        public const string LAUNDRY_CLASS_NAME = "ServiceOrders";
#endif
        //Common Column Names
        public const string UPDATEDATE_NAME = "updatedAt";
		public const string ObjectId = "objectId";
		//Folder Names
		public const string IMAGE_FOLDER_NAME = "SavedImages";
		//Category Attribute Names
		public const string CATEGORY_ATTRIBUTE_NAME = "Name";
		public const string CATEGORY_ATTRIBUTE_IMAGEID = "ImageID";
		public const string CATEGORY_ATTRIBUTE_ISSUBCATEGORYNAME = "isSubCategory";
		public const string CATEGORY_ATTRIBUTE_SUB = "Sub";
		public const string CATEGORY_ATTRIBUTE_PRIORITY = "Priority";
		public const string CATEGORY_ATTRIBUTE_ISINTHELIST = "isInTheList";
		public const string CATEGORY_ATTRIBUTE_PARENTCATEGORY = "ParentCategory";
#if Loyalty
        public const string CATEGORY_ATTRIBUTE_LOYALTYRATIO = "LoyaltyRatio";
#endif
        //Image Attribute Names
        public const string IMAGE_ATTRIBUTE_IMAGEFILE = "Image"; 
		//Product Attribute Names
		public const string PRODUCT_ATTRIBUTE_NAME = "Name";
		public const string PRODUCT_ATTRIBUTE_CATEGORYID = "Category";
		public const string PRODUCT_ATTRIBUTE_IMAGEID = "ImageID";
		public const string PRODUCT_ATTRIBUTE_PRICE = "Price";
		public const string PRODUCT_ATTRIBUTE_QUANTITY = "Quantity";
		public const string PRODUCT_ATTRIBUTE_PARENTCATEGORY = "ParentCategory";
		public const string PRODUCT_ATTRIBUTE_ISTOPSELLING = "IsTopSelling";
		public const string PRODUCT_ATTRIBUTE_PRIORITY = "Priority";
		public const string PRODUCT_ATTRIBUTE_ISINSTOCK = "IsInStock";
		public const string PRODUCT_ATTRIBUTE_ISSELECTEDBYBRAND = "IsSelectedByBrand";
		public const string PRODUCT_ATTRIBUTE_ISGRAMBASED = "isGramBased";
		public const string PRODUCT_ATTRIBUTE_KEYWORDS = "KeyWords";
	    public const string PRODUCT_ATTRIBUTE_BARCODE = "Barcode";
        public const string PRODUCT_ATTRIBUTE_DELETED = "isDeleted";
#if Loyalty
        public const string PRODUCT_ATTRIBUTE_LOYALTYRATIO = "LoyaltyRatio";
#endif
	    public const string PRODUCT_ATTRIBUTE_VAT = "Vat";

	    public const string PRODUCTBRANCHINFOS_ATTRIBUTE_PRODUCTID = "ProductId";
	    public const string PRODUCTBRANCHINFOS_ATTRIBUTE_BRANCHINDEX = "BranchIndex";
	    public const string PRODUCTBRANCHINFOS_ATTRIBUTE_CATEGORYID = "CategoryId";
        public const string PRODUCTBRANCHINFOS_ATTRIBUTE_PRICE = "Price";
	    public const string PRODUCTBRANCHINFOS_ATTRIBUTE_ISINSTORE = "IsInStore";
	    public const string PRODUCTBRANCHINFOS_ATTRIBUTE_ISINSTOCK = "IsInStock";
	    public const string PRODUCTBRANCHINFOS_ATTRIBUTE_ISDELETED = "IsDeleted";

        //Promotion Attribute Names
        public const string PROMOTION_ATTRIBUTE_TYPE = "PromotionType";
		public const string PROMOTION_ATTRIBUTE_TITLE = "Title";
		public const string PROMOTION_ATTRIBUTE_SUBTITLE = "SubTitle";
		public const string PROMOTION_ATTRIBUTE_DESCRIPTION = "Description";
		public const string PROMOTION_ATTRIBUTE_SUBDESCRIPTION = "SubDescription";
		public const string PROMOTION_ATTRIBUTE_IMAGEID = "ImageId";
		public const string PROMOTION_ATTRIBUTE_STARTDATE = "StartDate";
		public const string PROMOTION_ATTRIBUTE_ENDDATE = "EndDate";
		public const string PROMOTION_ATTRIBUTE_PROMOTIONVARIABLES = "PromotionVariables";
		public const string PROMOTION_ATTRIBUTE_INCATEGORIES = "InCategories";
		public const string PROMOTION_ATTRIBUTE_INPRODUCTCOUNT = "InProductCount";
		public const string PROMOTION_ATTRIBUTE_SALEVALUE = "SaleValue";
		public const string PROMOTION_ATTRIBUTE_OUTPRODUCTS = "OutProducts";
	    public const string PROMOTION_ATTRIBUTE_PRIORITY = "Priority";
        public const string PROMOTION_ATTRIBUTE_BRANCH = "Branch";
        //Orders Attribute Names
        public const string ORDERS_ATTRIBUTE_ADDRESS = "Address";
		public const string ORDERS_ATTRIBUTE_ADDRESSDESC = "AddressDescription";
		public const string ORDERS_ATTRIBUTE_ADDRESSLINE3 = "AddressLine3";
        public const string ORDERS_ATTRIBUTE_ADDRESSNOTE = "AddressNote";
		public const string ORDERS_ATTRIBUTE_USERNAME = "Name";		
		public const string ORDERS_ATTRIBUTE_ORDERARRAY = "Order";
		public const string ORDERS_ATTRIBUTE_PHONE = "Phone";
		public const string ORDERS_ATTRIBUTE_STATUS = "Status";
		public const string ORDERS_ATTRIBUTE_STORE = "Store";
	    public const string ORDERS_ATTRIBUTE_BRANCH = "Region";
        public const string ORDERS_ATTRIBUTE_SURNAME = "SurName";
		public const string ORDERS_ATTRIBUTE_USERID = "userID";
        public const string ORDERS_ATTRIBUTE_ENDUSERID = "EndUserID";

		public const string ORDERS_ATTRIBUTE_EARNEDPOINTS = "EarnedPoints";
	    public const string ORDERS_ATTRIBUTE_USEDPOINTS = "UsedPoints";
        public const string ORDERS_DELIVERY_STAFF_ID = "DeliveryStaffID";
		public const string ORDERS_APPROVE_DATE = "ApproveDate";
		public const string ORDERS_DISPATCH_DATE = "DispatchDate";
        public const string ORDERS_FINISH_DATE = "FinishDate";
        public const string ORDERS_ATTRIBUTE_CHANGE = "Change";
        public const string ORDERS_ATTRIBUTE_CALL = "Call";
        public const string ORDERS_ATTRIBUTE_NOTE = "Note";
        public const string ORDERS_ATTRIBUTE_FIRSTTIMEORDER = "FirstTimeOrder";
        public const string ORDERS_ATTRIBUTE_ESTIMATEDDELIVERYTIME = "EstimatedDeliveryTime";

        public const string ORDERS_ATTRIBUTE_CANCELREASON = "CancelReason";
#if Stamp
        public const string ORDERS_ATTRIBUTE_STAMPREWARDS = "StampRewards";
#endif
#if Laundry
        public const string ORDERS_ATTRIBUTE_PICKEDUPSERVICEORDER = "PickedUpServiceOrder";
        public const string ORDERS_ATTRIBUTE_DROPOFFSERVICEORDER = "DropOffServiceOrder";
        //Laundry Attribute Names
        public const string LAUNDRY_ATTRIBUTE_STORE = "Store";
        public const string LAUNDRY_ATTRIBUTE_ENDUSERID = "EndUserId";
        public const string LAUNDRY_ATTRIBUTE_FINISHDATE = "FinishDate";
        public const string LAUNDRY_ATTRIBUTE_NAME = "Name";
        public const string LAUNDRY_ATTRIBUTE_PHONE = "Phone";
        public const string LAUNDRY_ATTRIBUTE_DROPOFFORDER = "DropOffOrder";
        public const string LAUNDRY_ATTRIBUTE_READYDATE = "ReadyDate";
        public const string LAUNDRY_ATTRIBUTE_STATUS = "Status";
        public const string LAUNDRY_ATTRIBUTE_PICKEDUPORDER = "PickedUpOrder";
        public const string LAUNDRY_ATTRIBUTE_SURNAME = "Surname";
        public const string LAUNDRY_ATTRIBUTE_DROPOFFREQUESTDATE = "DropOffRequestedDate";
        public const string LAUNDRY_ATTRIBUTE_PRICE = "TotalPrice";
        public const string LAUNDRY_ATTRIBUTE_SERVICEORDERDETAILS = "ServiceOrderDetailList";
#endif
        //Delivery staff Attribute Names
        public const string DELIVERYSTAFF_PHONE = "TelNo";
		public const string DELIVERYSTAFF_NAME = "Name";
		public const string DELIVERYSTAFF_SURNAME = "SurName";
		//Settings Attribute Names
		public const string SETTINGS_VERSION = "Version";
		public const string SETTINGS_MAINTENANCE = "Maintenance";
		public const string SETTINGS_ACTIVEHOURS = "ActiveHours";
        public const string SETTINGS_NUM_OF_FEEDBACK_PROMPTS_COUNT = "NumOfFeedbackPrompts";
        public const string SETTINGS_FEEDBACK_PROMPTS_OFFSET = "FeedbackPromptsOffset";
        public const string SETTINGS_STAMPPIN = "StampPin";
        public const string SETTINGS_FEEDBACK_PROMPTS_COUNT = "FeedbackPromptsCount";
        //Suggestions Attribute Names
        public const string SUGGESTIONS_ATTRIBUTE_SUGGESTION = "Suggestion";
		public const string SUGGESTIONS_ATTRIBUTE_STATE = "State";
		public const string SUGGESTIONS_ATTRIBUTE_USER = "User";
	    public const string SUGGESTIONS_ATTRIBUTE_TYPE = "Type";
        //Customer Attribute Names
        public const string CustomerAttributeUserName = "userName";
		public const string CustomerAttributeFirstName = "FirstName";
		public const string CustomerAttributeLastName = "LastName";
		public const string CustomerAttributeBranch = "region";
		public const string CustomerAttributePoints = "Points";
		public const string CustomerAttributeBirthDate = "BirthDate";
		//Address Attribute Names
		public const string AddressAttributeUserId = "UserID";
		public const string AddressAttributePhoneNumber = "PhoneNumber";
		public const string AddressAttributeShopNumber = "ShopNumber";
		public const string AddressAttributeAddress = "Address";
		public const string AddressAttributeAddressDescription = "AddressDescription";
		public const string AddressAttributeAddressLine3 = "AddressLine3";
		public const string AddressAttributeIsActive = "IsActive";
        public const string AddressAttributeBuildingId = "BuildingId";
        public const string AddressAttributeApartmentNumber = "ApartmentNumber";
        public const string AddressAttributeBranchId = "BranchId";
	    public const string AddressAttributeNote = "Note";
        //EndUser Attribute Names
        public const string EndUserAttributeUserId = "UserId";
		public const string EndUserAttributePoints = "Points";
		//Rewards Attribute Names
		public const string RewardsAttributeName = "Name";
		public const string RewardsAttributePoints = "Points";
		public const string RewardsAttributeIsDeleted = "IsDeleted";
        //Stamps Attribute Names
        public const string STAMPS_ATTRIBUTE_TITLE = "Title";
        public const string STAMPS_ATTRIBUTE_DESCRIPTION = "Description";
        public const string STAMPS_ATTRIBUTE_IMAGEID = "ImageId";
        public const string STAMPS_ATTRIBUTE_COUNT = "Count";
        public const string STAMPS_ATTRIBUTE_STAMPTYPE = "StampsType";
        public const string STAMPS_ATTRIBUTE_STAMPVARIABLES = "StampVariables";
        //UserStamps Attribute Names
        public const string USERSTAMPS_ATTRIBUTE_STAMPID = "StampId";
        public const string USERSTAMPS_ATTRIBUTE_USERID = "UserId";
        public const string USERSTAMPS_ATTRIBUTE_COUNT = "Count";
        //StampReward Attribute Names
        public const string STAMPREWARD_ATTRIBUTE_STAMP = "Stamp";
        public const string STAMPREWARD_ATTRIBUTE_USER = "User";
        public const string STAMPREWARD_ATTRIBUTE_PRODUCT = "Product";
        //Feedback Attribute Names
        public const string FEEDBACK_ATTRIBUTE_HowAreYouFeeling = "Feeling";
        public const string FEEDBACK_ATTRIBUTE_Rating = "Rating";
        public const string FEEDBACK_ATTRIBUTE_Comment = "Comment";
        public const string FEEDBACK_ATTRIBUTE_User = "User";
    }
}

