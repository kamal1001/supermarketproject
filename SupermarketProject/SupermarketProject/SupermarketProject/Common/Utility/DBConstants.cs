﻿using PCLStorage;
using Xamarin.Forms;

namespace SupermarketProject.Common.Utilities
{
	public static class DBConstants
	{
		public const string DB_NAME = "westzone";
	    public const string DB_EXTENSION = ".db3";
        public static string DB_FULL_NAME = DB_NAME + DependencyService.Get<INativeMethods>().GetVersionCode() + DB_EXTENSION;
		private static IFolder DB_FOLDER = FileSystem.Current.LocalStorage;
		public static string DB_ROOTFOLDERNAME = DB_FOLDER.Path;
		public static string DB_PATH = DB_ROOTFOLDERNAME + "/" + DB_FULL_NAME;
	}
}

