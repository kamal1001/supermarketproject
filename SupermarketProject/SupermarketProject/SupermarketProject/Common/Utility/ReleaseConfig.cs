using System;

namespace SupermarketProject.Common.Utilities
{
    public static class ReleaseConfig
    {
        public static DateTime LAST_UPDATEDATE;
        //public static string FONT_PATH = "Arial-Rounded-MT-Bold";
        public static string FONT_PATH = "monospace";
        public static string TOBACCO_ID = "rnWTEYgylT";
        public static string MAGAZINE_ID = "1D6DeQ0EBU";
        public static string FRUITS_ID = "rCTqzE9bFG";
        public static string MEAT_ID = "jY9n7tPA8b";
        public static string RECHARGE_CARDS_ID = "VIK0Uad8hr";
        public static string DB_VERSION = "9";
        public static int RELEASE_VERSION = 10;
        public static bool ShouldUseEmbeddedDatabase = true;
        public static bool DefaultOrderStockAvailability = true;

        //previously barsha branch
        //public static string defaultBranchIndex = 1;

        //wharf branch- Al Jaddaf
        public static int DefaultBranchIndex = 9;

        static ReleaseConfig()
        {
            DateTime.TryParse("2015-05-11T06:04:10Z", out LAST_UPDATEDATE);
        }
    }
}