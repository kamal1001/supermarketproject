﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

public static class ViewExtensions
{ 
	public static bool isShiftColorToWorking;
	public static void ShiftColorTo (this VisualElement view, Color sourceColor, Color targetColor, Action<Color> setter, uint length = 250, Easing easing = null, int repeat = 1)
	{
		isShiftColorToWorking = true;
		if(repeat <= 1)
			view.Animate ("ShiftColorTo",
				x => 
				{
					var red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
					var green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
					var blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
					var alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));

					setter(Color.FromRgba(red, green, blue, alpha));
				},
				16,
				length,
				easing,
				(y,a)=>
				{
					isShiftColorToWorking=false;
				}
			);
		else
		view.Animate ("ShiftColorTo",
			x => 
			{
				var red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
				var green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
				var blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
				var alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));

				setter(Color.FromRgba(red, green, blue, alpha));
			},
			16,
			length,
			easing,
			(y,a)=>
			{
					view.ShiftColorTo(targetColor,sourceColor,setter,length,easing,repeat-1);
			}
		);

		/*view.Animate ("ShiftColorTo",
			x => 
			{
				var red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
				var green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
				var blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
				var alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));

				setter(Color.FromRgba(red, green, blue, alpha));
			},
			16,
			length,
			easing,
			(y,a)=>
			{
				view.Animate ("ShiftColorTo",
					x => 
					{
						var red = targetColor.R + (x * (sourceColor.R - targetColor.R));
						var green = targetColor.G + (x * (sourceColor.G - targetColor.G));
						var blue = targetColor.B + (x * (sourceColor.B - targetColor.B));
						var alpha = targetColor.A + (x * (sourceColor.A - targetColor.A));

						setter(Color.FromRgba(red, green, blue, alpha));
					},
					16,
					length,
					easing);
			}
		);*/

	}
}