﻿using System;
using Xamarin.Forms;
using SupermarketProject.MainViews;
using System.Threading.Tasks;
using TwinTechs.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using SupermarketProject.Common.Objects;
using SupermarketProject.Models.Remote;
using Plugin.Connectivity;
using SQLite;
using System.Linq;
using System.Reflection;
using SupermarketProject.Models.Local;
using Com.OneSignal;
using Parse;
using PCLStorage;
using Xamarin.Forms.Xaml;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Common.Utilities
{
    public static class MyDevice
    {
        public static SQLiteConnection db;
        public static string BrandFolderName = "SupermarketProject";
        public static IFastCellCache fastCellCache { get; set; }
        public static IFastCellCache fastGridCellCache { get; set; }
        public static double ScreenWidth;
        public static double ScreenHeight;
        public static int FeedbackPromptsCount;
        public static int FeedbackPromptsOffset;



        public static bool shared { get; set; }//for sharing functionality


        public static Color GreenColor = Color.FromRgb(85, 161, 106);
        public static Color RedColor = Color.FromRgb(238, 16, 11);
        public static Color BlueColor = Color.FromRgb(31, 47, 83);
        public static Color SecondaryColor = Color.FromRgb(219, 51, 56);
        public static Color PrimaryColor = Color.FromRgb(24, 125, 70);
        public static Color GreyColor = Color.FromRgb(215, 215, 215);
        public static int DelayTime = 1;
        public static double SubCategoryLayoutUnderlineHeight = 2;
        //Bar Colors
        public static Color StatusBarColor = PrimaryColor;
        public static Color LoadingBarColor = PrimaryColor;
        public static Color CategoryHeaderColor = Color.White;
        public static Color ProductHeaderColor = Color.White;
        public static Color ProductHeaderAnimationColor = PrimaryColor;
        public static Color StoreInfoCallButtonBorderColor = PrimaryColor;
        public static Color StoreInfoFindButtonBorderColor = SecondaryColor;
        public static Color TrackPageUnderlineColor = SecondaryColor;
        public static int TrackPageUnderlineHeight = 5;
        public static Color ProfilePageLoyaltyLayoutColor = SecondaryColor;
        public static Color FooterColor = SecondaryColor;
        public static Color CartLayoutProfileIconColor = Color.Black;
        public static Color CartBottomLayoutBackgroundColor = Color.White;
        public static Color CartCellMultiplyLayoutBackgroundColor = GreyColor;
        public static Color CartCellPromotionLableBackgroundColor = SecondaryColor;
        public static Color ProductCellPromotionLableBackgroundColor = SecondaryColor;
        public static Color PromotionDetailPageInformationLabelBackgroundColor = PrimaryColor;
        public static Color PromotionDetailPageInformationLabelBorderColor = PrimaryColor;
        //Button Colors
        public static Color BackButtonColor = SecondaryColor;
        public static Color SkipButtonColor = Color.FromRgb(98, 98, 98);
        public static Color SignInButtonColor = PrimaryColor;
        public static Color AddButtonColor = PrimaryColor;
        public static Color AddNewDeliveryAddressButtonColor = PrimaryColor;
        public static Color ContinueToShopButtonColor = PrimaryColor;
        public static Color CheckoutButtonColor = PrimaryColor;
        public static Color CartCellDeleteButtonColor = Color.FromRgb(253, 59, 47);
        public static Color ReceiptPageAgreeButtonColor = PrimaryColor;
        public static Color PaymentPageSelectedButtonColor = PrimaryColor;
        public static Color PaymentPageNotSelectedButtonColor = GreyColor;
        public static Color PaymentPageDoneButtonColor = PrimaryColor;
        public static Color PaymentPageCancelButtonColor = Color.Black;
        public static Color PromotionDetailPagePromotionAddPopUpAddButtonColor = SecondaryColor;
        public static Color PromotionDetailPagePromotionAddPopUpContinueButtonColor = PrimaryColor;
        //Entry Text Colors
        public static Color LoadingBarTextColor = Color.Black;
        public static Color HeaderPriceColor = SecondaryColor;
        public static Color CategoriesPageCategoryandFavouriteTextColor = PrimaryColor;
        public static Color ProductCellPriceColor = Color.Black;
        public static Color CartCellPriceColor = Color.FromRgb(67, 67, 65);
        public static Color CartCellMultiplyLayoutTextColor = Color.White;
        public static Color SearchEntryTextColor = /*SecondaryColor*/ PrimaryColor;
        public static Color CartAddNoteTextColor = Color.White;
        public static Color CartBottomLayoutTextColor = Color.Black;
        public static Color SubCategoryLayoutSeperatorTextColor = Color.Transparent;
        public static Color SubCategoryLayoutTextColor = SecondaryColor;
        public static Color SubCategoryLayoutTextUnderlineColor = PrimaryColor;
        public static Color PromotionDetailPageProductCellOriginalPriceTextColor = Color.Black;
        public static Color PromotionDetailPageProductCellDiscountedPriceTextColor = PrimaryColor;
        public static Color PromotionDetailPageSavingsTextColor = Color.Black;
        public static Color PromotionDetailPageOfferValidTextColor = Color.White;
        public static Color PromotionDetailPageTimerTextColor = Color.White;

        public static string SearchText = "quick search any product...";
        public static string Brand = "West Zone Fresh";
        //if Android
        public static string bundle = "com.appinfo.westzonefresh";
        //if iOS 
        //public static string bundle = "dev.westzonefresh.WestZoneFreshSupermarket";
        public static string DeviceID;
        public static double MenuPadding;
        public static double ViewPadding;
        public static double FontSizeMicro;
        public static double FontSizeSmall;
        public static double FontSizeMedium;
        public static double FontSizeLarge;
        public static double SwipeDistance;
        public static uint AnimationTimer = 300;
        public static IPage currentPage;
        public static RootPage rootPage;
        public static int collectCounter;
        public static double DragBorderMultiplier = 1 / 7f;
        public static int ConnectionCheckTimeout = 1000;

        public static string InternetErrorMessage1 = "Your internet is unstable, the app will still load but it will just take a bit longer";
        public static string InternetErrorMessage2 = "We could not detect internet connection, the app will still load, however the database could not be updated";

        public static string EmbeddedResourceImageDirectoryPath;
        public static HashSet<string> EmbeddedResourceImageNameStrings;
        public static HashSet<string> DownloadedImageNameStrings;

        public static CartLayout mCartLayout;
        public static MenuLayout mMenuLayout;
#if Loyalty
        public static Footer Footer;
        public static int PointsPerAed { get; set; } = 100;
#endif
#if Laundry
        public static decimal MinPriceForLaundry = 10;
#endif
#if RewardNewUser
        public static decimal NewUserRewardPercentage = 10;
#endif

        public static DateTime DubaiDateTimeNow => LocalTime.Time.DateTime;

        public static int DeliveredOrdersCount { get; internal set; }

        public static event Action AuthenticationStateChanged;
        public static Promotion SelectedPromotion;
        public static ObservableCollection<Promotion> PromotionList = new ObservableCollection<Promotion>();

        public enum FontTypes
        {
            MyriadProRegular, MyriadProBold, MyriadProItalic, MyriadProSemibold, MontserratBold, CalibriRegular
        }
        public static Dictionary<FontTypes, string> FontTypeDictionary = new Dictionary<FontTypes, string>()
        {
            {FontTypes.CalibriRegular,"Calibri"},
            {FontTypes.MontserratBold,"Montserrat-Bold"},
            {FontTypes.MyriadProBold,"MyriadPro-Bold"},
            {FontTypes.MyriadProItalic,"MyriadPro-It"},
            {FontTypes.MyriadProRegular,"MyriadPro-Regular"},
            {FontTypes.MyriadProSemibold,"MyriadPro-Semibold"}
        };

        public static string ColorToHex(Color color)
        {
            int red = (int)(color.R * 255f);
            int green = (int)(color.R * 255f);
            int blue = (int)(color.R * 255f);
            int alpha = (int)(color.R * 255f);

            return $"#{red:x2}{green:x2}{blue:x2}{alpha:x2}";
        }
        private static async Task ClearData()
        {
            IFolder tempfolder = await FileSystem.Current.LocalStorage?.GetFolderAsync(DBConstants.DB_ROOTFOLDERNAME);
            foreach (var f in await tempfolder.GetFilesAsync())
            {
                await f?.DeleteAsync();
            }

            var folders = await tempfolder?.GetFoldersAsync();
            foreach (var folder in folders)
            {
                foreach (var file in await folder?.GetFilesAsync())
                {
                    await file?.DeleteAsync();
                }
            }
        }

#if Laundry
        public static async Task OpenLaundryReadyPopup()
        {
            Device.BeginInvokeOnMainThread(async () =>
           {
               AlertClass loundryReadyPopup = AlertModel.GetAlert(AlertType.Laundry_Ready_Push_Popup);
               await Application.Current.MainPage.DisplayAlert(loundryReadyPopup.Title, loundryReadyPopup.Message, loundryReadyPopup.OkText);
           });

        }

#endif

        private static double CurrentUserFeedbackPromptsCount()
        {
            if (ParseUser.CurrentUser != null && ParseUser.CurrentUser.ContainsKey(ParseConstants.SETTINGS_FEEDBACK_PROMPTS_COUNT))
            {
                return ParseUser.CurrentUser.Get<double>(ParseConstants.SETTINGS_FEEDBACK_PROMPTS_COUNT);
            }
            return -1;
        }

        public static bool ShouldLogFeedbackPrompt()
        {
            DeliveredOrdersCount += 1;
            if (DeliveredOrdersCount == 1)
            {
                return true;
            }

            double currPromptsCount = CurrentUserFeedbackPromptsCount();

            bool result = currPromptsCount < FeedbackPromptsCount && DeliveredOrdersCount % FeedbackPromptsOffset == 0;
            DeliveredOrdersCount = -1;

            return result;
        }

        public static async Task<bool> UpdateFeedbackPromptsCount()
        {
            for (int t = 0; t < 3; t++)
            {
                int cancelTime = 10000;
                try
                {
                    var count = CurrentUserFeedbackPromptsCount();
                    ParseUser.CurrentUser[ParseConstants.SETTINGS_FEEDBACK_PROMPTS_COUNT] = ++count;
                    await ParseUser.CurrentUser.SaveAsync();
                    return true;
                }
                catch (Exception e)
                {
                    Crashes.TrackError(e);
                    Debug.WriteLine(e.ToString());
                    return false;
                }
            }

            return false;
        }

        private static async Task MigrateToNewDb()
        {
            //copy db file
            try
            {
                var activeBranch = UserClassFunctions.GetActiveBranchFromUser();
                var productListInCart = CartClassFunctions.GetProductList();
                db.Close();
                db.Dispose();
                db = null;

                await CopyDbFile();

                //connect to new db
                db = new SQLiteConnection(DBConstants.DB_PATH);
                InitDbTables();

                CartClassFunctions.ClearTable();
                if (activeBranch == -1)
                    UserClassFunctions.AddActiveBranchToUser(activeBranch);
                if (productListInCart.Count > 0)
                    CartClassFunctions.AddProductListToCart(productListInCart);

                //get users addresses and favorites
                if (ParseUser.CurrentUser != null)
                {
                    await AddressModel.FetchAddressesAndSaveItToLocal();
                    var favoriteIdList = ParseUser.CurrentUser.ContainsKey("Favorites") ? ParseUser.CurrentUser.Get<string>("Favorites").Split(',').ToList() : null;
                    if (favoriteIdList != null && favoriteIdList.Count > 0)
                        FavoritiesClassFunctions.AddProductList(favoriteIdList);
                }

            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                Debug.WriteLine(ex.ToString());
                Debug.WriteLine("Exception while migrating DB " + ex.Message);
                throw;
            }
        }

        private static async Task CopyDbFile()
        {
            try
            {
                await ClearData();
                var assembly = typeof(App).GetTypeInfo().Assembly;

                //copy stream from resource db
                IFile dbFile = await FileSystem.Current.LocalStorage.CreateFileAsync(DBConstants.DB_PATH, CreationCollisionOption.ReplaceExisting);
                Stream stream = assembly.GetManifestResourceStream($"SupermarketProject.DbFile.{DBConstants.DB_NAME + DBConstants.DB_EXTENSION}");
                Stream fileStream = await dbFile.OpenAsync(FileAccess.ReadAndWrite);
                await stream.CopyToAsync(fileStream);
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                Debug.WriteLine("Exception while copying db file " + ex.Message);
                throw;
            }
        }

        public static void DropTables()
        {
            CategoryClassFunctions.ResetTable();
            ProductBranchInfoClassFunctions.ResetTable();
            ProductClassFunctions.ResetTable();
            UserClassFunctions.ResetTable();
            AddressClassFunctions.ResetTable();
            AlertClassFunctions.ResetTable();
            RewardClassFunctions.ResetTable();
            PromotionClassFunctions.ResetTable();
            BranchClassFunctions.ResetTable();
#if Laundry
            LaundryClassFunctions.ResetTable();
#endif
        }

        public static async Task InitializeDb()
        {
            var currentVersionNumber = DependencyService.Get<INativeMethods>().GetVersionCode();

            if (ReleaseConfig.ShouldUseEmbeddedDatabase)
            {
                bool bConnectedToOldDb = await ConnectToOldDb();
                if (bConnectedToOldDb)
                {
                    InitDbTables();
                    string versionNumberFromLocal = UserClassFunctions.GetAppVersionFromUser();

                    if (!string.Equals(versionNumberFromLocal, currentVersionNumber))
                    {
                        await MigrateToNewDb();
                    }
                    else
                    {
                        //connect to db
                        db = new SQLiteConnection(DBConstants.DB_PATH);
                        InitDbTables();
                    }
                }
                else
                {
                    await CopyDbFile();
                    //connect to db
                    db = new SQLiteConnection(DBConstants.DB_PATH);
                    InitDbTables();
                }

            }
            else
            {
                //connect to db
                db = new SQLiteConnection(DBConstants.DB_PATH);
                InitDbTables();
            }

            UserClassFunctions.AddAppVersionToUser(currentVersionNumber);

        }
        public static bool IsProductSuitableForNewUserPromotion(Product product)
        {
            bool returnValue = ParseUser.CurrentUser != null && !ParseUser.CurrentUser.Get<bool>("NewRewardUsed")
                && product.ParentCategory != ReleaseConfig.TOBACCO_ID
                                        && product.ParentCategory != ReleaseConfig.MAGAZINE_ID
                                        && product.ParentCategory != ReleaseConfig.RECHARGE_CARDS_ID;

            return returnValue;
        }
        private static async Task<bool> ConnectToOldDb()
        {
            IFolder tempfolder = await FileSystem.Current.LocalStorage.GetFolderAsync(DBConstants.DB_ROOTFOLDERNAME);
            foreach (var f in await tempfolder.GetFilesAsync())
            {
                if (f.Name.Contains(DBConstants.DB_NAME) && f.Name.EndsWith(DBConstants.DB_EXTENSION))
                {
                    //create connection to old database
                    db = new SQLiteConnection(f.Path);
                    return true;
                }
            }
            return false;
        }

        private static void InitDbTables()
        {
            db.ExecuteScalar<int>("PRAGMA journal_mode = WAL");
            db.Execute("PRAGMA temp_store = MEMORY");
            db.Execute("PRAGMA synchronous = OFF");

            UserClassFunctions.Initialize();
            FavoritiesClassFunctions.Initialize();
            ProductBranchInfoClassFunctions.Initialize();
            ProductClassFunctions.Initialize();
            AddressClassFunctions.Initialize();
            CartClassFunctions.Initialize();
            CategoryClassFunctions.Initialize();
            BranchClassFunctions.Initialize();
            //address related data
            StateClassFunctions.Initialize();
            AreaClassFunctions.Initialize();
            BranchBuildingClassFunctions.Initialize();
            BuildingClassFunctions.Initialize();

            ImageClassFunctions.Initialize();
            AlertClassFunctions.Initialize();
            PromotionClassFunctions.Initialize();
            RewardClassFunctions.Initialize();
#if Laundry
            LaundryClassFunctions.Initialize();
#endif
#if Stamp
            //stamp related
            StampClassFunctions.Initialize();
            UserStampClassFunctions.Initialize();
            StampRewardClassFunctions.Initialize();
#endif
        }


        public static void Initialize()
        {
            ScreenWidth = App.ScreenWidth;
            ScreenHeight = App.ScreenHeight;
            MenuPadding = ScreenWidth / 25;
            ViewPadding = ScreenWidth / 50;
            FontSizeMicro = ScreenWidth / 36.0;
            FontSizeSmall = ScreenWidth / 25.7;
            FontSizeMedium = ScreenWidth / 20.0;
            FontSizeLarge = ScreenWidth / 16.3;
            SwipeDistance = ScreenWidth / 2;
            EmbeddedResourceImageNameStrings = new HashSet<string>(StringComparer.Ordinal);
            DownloadedImageNameStrings = new HashSet<string>(StringComparer.Ordinal);
        }
        public async static Task<bool> GetNetworkStatus()
        {
            bool result;
            result = await GetNetworkStatusHelper(1);
            return result;
        }

        private async static Task<bool> GetNetworkStatusHelper(int i)
        {
            bool result = false;

            try
            {
                result = await CrossConnectivity.Current.IsRemoteReachable("http://google.com");
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                System.Diagnostics.Debug.WriteLine(e.ToString());
                System.Diagnostics.Debug.WriteLine(e.Message);
                if (i == 0)
                    result = false;
                else
                    result = await GetNetworkStatusHelper(i - 1);
            }

            return result;
        }

        public static double GetScaledSize(double x)
        {
            return ScreenWidth * (x / 640.0);
        }
        public static double GetReversedScaledSize(double x)
        {
            return (x / ScreenWidth) * 640.0;
        }
        public static ScaledSizeConverter StaticScaledSizeConverter = new ScaledSizeConverter();
        public class ScaledSizeConverter : IValueConverter, IMarkupExtension
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                string parameterString = parameter as string;

                if (string.IsNullOrEmpty(parameterString))
                    return 0;

                string[] parameters = parameterString.Split('|');

                List<Tuple<double, int>> doubles = new List<Tuple<double, int>>();
                foreach (string s in parameters)
                {
                    double size;
                    int param;
                    List<string> tuple = s.Split('?').ToList();
                    if (tuple.Count < 2)
                        tuple.Add("0");
                    doubles.Add(new Tuple<double, int>(double.TryParse(tuple[0], out size) ? GetScaledSize(size) : 0, int.TryParse(tuple[1], out param) ? param : 0));
                }

                switch (doubles.Count)
                {
                    case 1:
                        return DecideOnValue(doubles[0]);
                    case 2:
                        return new Thickness(DecideOnValue(doubles[0]), DecideOnValue(doubles[1]));
                    case 4:
                        return new Thickness(DecideOnValue(doubles[0]), DecideOnValue(doubles[1]), DecideOnValue(doubles[2]), DecideOnValue(doubles[3]));
                    case 5:
                        if (parameters[4].Split('?')[0] == "0")
                            return new Thickness(DecideOnValue(doubles[0]), DecideOnValue(doubles[1]), DecideOnValue(doubles[2]), DecideOnValue(doubles[3]));
                        return new Rectangle(DecideOnValue(doubles[0]), DecideOnValue(doubles[1]), DecideOnValue(doubles[2]), DecideOnValue(doubles[3]));
                    default:
                        return 0;
                }
            }

            private double DecideOnValue(Tuple<double, int> tuple)
            {
                switch (tuple.Item2)
                {
                    case 0:
                        return tuple.Item1;
                    case 1:
                        return ScreenHeight - tuple.Item1;
                    case 2:
                        return ScreenWidth - tuple.Item1;
                    default:
                        return 0;
                }
            }

            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                throw new NotSupportedException("Only one way bindings are supported with this converter");

            }
            public object ProvideValue(IServiceProvider serviceProvider)
            {
                return this;
            }
        }
        public static ScaledSizeSubtractConverter StaticScaledSizeSubtractConverter = new ScaledSizeSubtractConverter();
        public class ScaledSizeSubtractConverter : IValueConverter, IMarkupExtension
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                string parameterString = parameter as string;

                if (string.IsNullOrEmpty(parameterString))
                    return 0;

                string[] parameters = parameterString.Split(new char[] { '|' });
                if (parameters.Length == 2)
                {
                    int paramaterCase = 0;
                    int.TryParse(parameters[0], out paramaterCase);

                    double paramaterValue = 0;
                    double.TryParse(parameters[1], out paramaterValue);
                    switch (paramaterCase)
                    {
                        case 0:
                            return ScreenWidth - GetScaledSize(paramaterValue);
                        case 1:
                            return ScreenHeight - GetScaledSize(paramaterValue);
                        default:
                            return GetScaledSize(paramaterValue);
                    }
                }
                else if (parameters.Length == 8)
                {

                    double[] returnValues = new double[4];
                    for (int i = 0; i < 4; i++)
                    {
                        double paramaterValue;
                        int paramaterCase;
                        int.TryParse(parameters[i * 2], out paramaterCase);
                        double.TryParse(parameters[i * 2 + 1], out paramaterValue);

                        switch (paramaterCase)
                        {
                            case 0:
                                returnValues[i] = ScreenWidth - GetScaledSize(paramaterValue);
                                break;
                            case 1:
                                returnValues[i] = ScreenHeight - GetScaledSize(paramaterValue);
                                break;
                            default:
                                returnValues[i] = GetScaledSize(paramaterValue);
                                break;
                        }
                    }
                    return new Rectangle(returnValues[0], returnValues[1], returnValues[2], returnValues[3]);

                }
                else
                    return 0;
            }
            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                throw new NotSupportedException("Only one way bindings are supported with this converter");

            }
            public object ProvideValue(IServiceProvider serviceProvider)
            {
                return this;
            }
        }
        public static double GetScaledFontSize(double x)
        {
            return ScreenWidth * (x / 640.0);
        }

        public class NegateBooleanConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return !(bool)value;
            }
            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return !(bool)value;
            }
        }

        public class NumberToBoolConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return (int)value > 0;
            }
            public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                return !(bool)value;
            }
        }
        public static string GetArrayAsString(List<string> list)
        {
            string retuValue = "";
            foreach (var val in list)
            {
                retuValue += val.ToString();

                if (val != list.Last())
                    retuValue += ",";
            }
            return retuValue;
        }
        public static string GetArrayAsString(List<int> list)
        {
            string retuValue = "";
            foreach (var val in list)
            {
                retuValue += val.ToString();

                if (val != list.Last())
                    retuValue += ",";
            }
            return retuValue;
        }

        public static string GetTimeString(TimeSpan? time)
        {
            string timeString = "";

            if (time != null)
                timeString = time.Value.Days > 0
                    ? $"{time.Value.Days}d:{time.Value.Hours}h:{time.Value.Minutes}m"
                    : $"{time.Value.Hours}h:{time.Value.Minutes}m:{time.Value.Seconds}s";

            return timeString;
        }
        public static string AmPmConverter(int hour, out string hourString)
        {
            string amString;

            if (hour < 12)
            {
                hourString = hour.ToString("00") + ":00";
                amString = "am";
            }
            else if (hour == 12)
            {
                hourString = "12:00";
                amString = "pm";
            }
            else
            {
                hourString = (hour % 12).ToString("00") + ":00";
                amString = "pm";
            }

            return amString;
        }

        public static string Truncate(this string value, int maxChars)
        {
            return value.Length <= maxChars ? value : value.Substring(0, maxChars) + "...";
        }

        public static string FirstCharToUpper(this string value)
        {
            if (String.IsNullOrEmpty(value))
                throw new ArgumentException("ARGH!");

            var stringArray = value.Split(' ');

            if (stringArray == null || stringArray.Length == 0)
            {
                return value[0].ToString().ToUpper() + value.Substring(1);
            }

            string returnValue = "";

            foreach (var arrayString in stringArray)
            {
                returnValue += arrayString[0].ToString().ToUpper() + arrayString.Substring(1) + " ";
            }


            return returnValue.Trim();
        }

        public static async Task<bool> Login(string userName, string password, bool statusChanged)
        {
            bool isSuccess = true;
            try
            {
                await ParseUser.LogInAsync(userName, password).ContinueWith(t =>
                {
                    if (t.IsFaulted || t.IsCanceled)
                    {
                        Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                        {
                            Application.Current.MainPage.DisplayAlert("Alert!", "Invalid username/password.", "OK");
                        });
                        isSuccess = false;
                    }
                });

                if (!isSuccess)
                {
                    return isSuccess;
                }
#if Stamp
            await UserStampModel.FetchUserStamps();
#endif
            }
            catch (ParseException ex)
            {
                isSuccess = false;
                Crashes.TrackError(ex);
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }

            ParseInstallation.CurrentInstallation["user"] = ParseUser.CurrentUser;
            await ParseInstallation.CurrentInstallation.SaveAsync();

            if (statusChanged)
                await AuthenticationStateChange();

            return isSuccess;
        }

        public static async Task AuthenticationStateChange()
        {
            AuthenticationStateChanged?.Invoke();
            if (ParseUser.CurrentUser != null)
            {
                //tag user with id for push
                OneSignal.Current.SendTag("UserId", ParseUser.CurrentUser.ObjectId);

                if (ParseUser.CurrentUser.ContainsKey("Designation"))
                {
                    if (ParseUser.CurrentUser.Get<string>("Designation") == "Mr.")
                        OneSignal.Current.SendTag("Gender", "Male");
                    else
                        OneSignal.Current.SendTag("Gender", "Female");
                }

                int branchId = UserClassFunctions.GetActiveBranchFromUser();
                var activeAddress = AddressClassFunctions.GetAddressList(branchId).FirstOrDefault(x => x.IsActive);

                if (activeAddress != null)
                {
                    if (activeAddress.BranchId.HasValue)
                        OneSignal.Current.SendTag("Branch", activeAddress.BranchId.Value.ToString());
                    var building = BuildingModel.BuildingList.FirstOrDefault(x => x.objectId == activeAddress.BuildingId);
                    if (building != null)
                    {
                        OneSignal.Current.SendTag("Building", activeAddress.BuildingId);
                        var area = AreaModel.AreaList.FirstOrDefault(x => x.objectId == building.AreaId);
                        if (area != null)
                        {
                            OneSignal.Current.SendTag("Area", area.objectId);
                            OneSignal.Current.SendTag("City", area.StateId);
                        }
                    }
                }
            }
            else
            {
                //remove user tag
                OneSignal.Current.DeleteTag("UserId");
                OneSignal.Current.DeleteTag("Gender");
                OneSignal.Current.DeleteTag("Branch");
                OneSignal.Current.DeleteTag("Building");
                OneSignal.Current.DeleteTag("Area");
                OneSignal.Current.DeleteTag("City");
            }
        }
        public static async Task Logout()
        {
            await ParseUser.LogOutAsync();
            await AuthenticationStateChange();
            MyDevice.mCartLayout?.SetUser();
        }

        public static IList<CountryCode> GetCountryCode()
        {
            return new List<CountryCode>
            {
                new CountryCode{AreaCode="+93"},
                new CountryCode{AreaCode="+355"},
                new CountryCode{AreaCode="+213"},
                new CountryCode{AreaCode="+54"},
                new CountryCode{AreaCode="+374"},
                new CountryCode{AreaCode="+61"},
                new CountryCode{AreaCode="+43"},
                new CountryCode{AreaCode="+994"},
                new CountryCode{AreaCode="+973"},
                new CountryCode{AreaCode="+880"},
                new CountryCode{AreaCode="+375"},
                new CountryCode{AreaCode="+32"},
                new CountryCode{AreaCode="+501"},
                new CountryCode{AreaCode="+591"},
                new CountryCode{AreaCode="+387"},
                new CountryCode{AreaCode="+55"},
                new CountryCode{AreaCode="+673"},
                new CountryCode{AreaCode="+359"},
                new CountryCode{AreaCode="+237"},
                new CountryCode{AreaCode="+1"},
                new CountryCode{AreaCode="+56"},
                new CountryCode{AreaCode="+86"},
                new CountryCode{AreaCode="+57"},
                new CountryCode{AreaCode="+269"},
                new CountryCode{AreaCode="+506"},
                new CountryCode{AreaCode="+225"},
                new CountryCode{AreaCode="+385"},
                new CountryCode{AreaCode="+53"},
                new CountryCode{AreaCode="+357"},
                new CountryCode{AreaCode="+420"},
                new CountryCode{AreaCode="+45"},
                new CountryCode{AreaCode="+253"},
                new CountryCode{AreaCode="+593 "},
                new CountryCode{AreaCode="+20"},
                new CountryCode{AreaCode="+503"},
                new CountryCode{AreaCode="+240"},
                new CountryCode{AreaCode="+291"},
                new CountryCode{AreaCode="+372"},
                new CountryCode{AreaCode="+251"},
                new CountryCode{AreaCode="+679"},
                new CountryCode{AreaCode="+358"},
                new CountryCode{AreaCode="+33"},
                new CountryCode{AreaCode="+241"},
                new CountryCode{AreaCode="+220"},
                new CountryCode{AreaCode="+995"},
                new CountryCode{AreaCode="+49"},
                new CountryCode{AreaCode="+233"},
                new CountryCode{AreaCode="+350"},
                new CountryCode{AreaCode="+30"},
                new CountryCode{AreaCode="+299"},
                new CountryCode{AreaCode="+502"},
                new CountryCode{AreaCode="+509"},
                new CountryCode{AreaCode="+504"},
                new CountryCode{AreaCode="+852"},
                new CountryCode{AreaCode="+36"},
                new CountryCode{AreaCode="+354"},
                new CountryCode{AreaCode="+91"},
                new CountryCode{AreaCode="+62"},
                new CountryCode{AreaCode="+98"},
                new CountryCode{AreaCode="+964"},
                new CountryCode{AreaCode="+353"},
                new CountryCode{AreaCode="+39"},
                new CountryCode{AreaCode="+1-876"},
                new CountryCode{AreaCode="+81"},
                new CountryCode{AreaCode="+962"},
                new CountryCode{AreaCode="+254"},
                new CountryCode{AreaCode="+82"},
                new CountryCode{AreaCode="+965"},
                new CountryCode{AreaCode="+961"},
                new CountryCode{AreaCode="+218"},
                new CountryCode{AreaCode="+352"},
                new CountryCode{AreaCode="+853"},
                new CountryCode{AreaCode="+389"},
                new CountryCode{AreaCode="+261"},
                new CountryCode{AreaCode="+60"},
                new CountryCode{AreaCode="+960"},
                new CountryCode{AreaCode="+223"},
                new CountryCode{AreaCode="+356"},
                new CountryCode{AreaCode="+230"},
                new CountryCode{AreaCode="+52"},
                new CountryCode{AreaCode="+377"},
                new CountryCode{AreaCode="+212"},
                new CountryCode{AreaCode="+264"},
                new CountryCode{AreaCode="+977"},
                new CountryCode{AreaCode="+31"},
                new CountryCode{AreaCode="+64"},
                new CountryCode{AreaCode="+234"},
                new CountryCode{AreaCode="+47"},
                new CountryCode{AreaCode="+968"},
                new CountryCode{AreaCode="+92"},
                new CountryCode{AreaCode="+970"},
                new CountryCode{AreaCode="+507"},
                new CountryCode{AreaCode="+675"},
                new CountryCode{AreaCode="+595"},
                new CountryCode{AreaCode="+51"},
                new CountryCode{AreaCode="+63"},
                new CountryCode{AreaCode="+48"},
                new CountryCode{AreaCode="+351"},
                new CountryCode{AreaCode="+974"},
                new CountryCode{AreaCode="+40"},
                new CountryCode{AreaCode="+7"},
                new CountryCode{AreaCode="+250"},
                new CountryCode{AreaCode="+966"},
                new CountryCode{AreaCode="+221"},
                new CountryCode{AreaCode="+248"},
                new CountryCode{AreaCode="+65"},
                new CountryCode{AreaCode="+27"},
                new CountryCode{AreaCode="+34"},
                new CountryCode{AreaCode="+94"},
                new CountryCode{AreaCode="+249"},
                new CountryCode{AreaCode="+46"},
                new CountryCode{AreaCode="+41"},
                new CountryCode{AreaCode="+963"},
                new CountryCode{AreaCode="+886"},
                new CountryCode{AreaCode="+255"},
                new CountryCode{AreaCode="+66"},
                new CountryCode{AreaCode="+216"},
                new CountryCode{AreaCode="+90"},
                new CountryCode{AreaCode="+256"},
                new CountryCode{AreaCode="+380"},
                new CountryCode{AreaCode="+971"},
                new CountryCode{AreaCode="+44"},
                new CountryCode{AreaCode="+1"},
                new CountryCode{AreaCode="+598"},
                new CountryCode{AreaCode="+998"},
                new CountryCode{AreaCode="+418"},
                new CountryCode{AreaCode="+58"},
                new CountryCode{AreaCode="+84"},
                new CountryCode{AreaCode="+967"},
                new CountryCode{AreaCode="+260"},
                new CountryCode{AreaCode="+263"},
            };
        }
    }
    public class CountryCode
    {
        public string AreaCode { get; set; }
    }
}