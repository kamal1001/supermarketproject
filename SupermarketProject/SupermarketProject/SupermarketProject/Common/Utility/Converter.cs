﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using SupermarketProject.Common.Utilities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SupermarketProject.Common.Utility
{
    public enum ConvertType
    {
        Rect, Thick, Double
    }

    public struct ConverterValue
    {
        public double Value { get; set; }

        public double Limit { get; set; }

        public ConverterValueType ValueType { get; set; }
    }

    public enum ConverterValueType
    {
        UseValue, SubFromSW, SubFromSH, SubFromSHWithMaximumLimit, UseTopSafeArea, SubFromSHWithNavigationBottomBar
    }

    public class ScaledSizeConverter : IMarkupExtension, IValueConverter
    {
        public ConvertType ConvertType { get; set; }

        public ConverterValue W { get; set; }

        public ConverterValue H { get; set; }

        public ConverterValue X { get; set; }

        public ConverterValue Y { get; set; }

        public ConverterValue D { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (ConvertType)
            {
                case ConvertType.Double:
                    return DecideOnValue(D);

                case ConvertType.Rect:
                    return new Rectangle(DecideOnValue(X), DecideOnValue(Y), DecideOnValue(W), DecideOnValue(H));

                case ConvertType.Thick:
                    if (W.Value == 0 && H.Value == 0)
                        return new Thickness(DecideOnValue(X), DecideOnValue(Y));
                    else
                        return new Thickness(DecideOnValue(X), DecideOnValue(Y), DecideOnValue(W), DecideOnValue(H));

                default:
                    return 0;
            }
        }

        private double DecideOnValue(ConverterValue D)
        {
            D.Value = MyDevice.GetScaledSize(D.Value);
            switch (D.ValueType)
            {
                case ConverterValueType.UseValue:
                    return D.Value;

                case ConverterValueType.SubFromSW:
                    return MyDevice.ScreenWidth - D.Value;

                case ConverterValueType.SubFromSH:
                    return MyDevice.ScreenHeight - D.Value;

                case ConverterValueType.SubFromSHWithMaximumLimit:
                    var value = MyDevice.ScreenHeight - D.Value;

                    return value > MyDevice.GetScaledSize(D.Limit) ? MyDevice.GetScaledSize(D.Limit) : value;
                case ConverterValueType.UseTopSafeArea:
                    return MyDevice.GetScaledSize(20) + D.Value;
                case ConverterValueType.SubFromSHWithNavigationBottomBar:
                    return MyDevice.ScreenHeight - D.Value - MyDevice.GetScaledSize(50);
                default:
                    return 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("Only one way bindings are supported with this converter");
        }
    }

    public class ByteArrayToImageConverter : IMarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;
            var byteArray = value as byte[];
            Stream stream = new MemoryStream(byteArray);
            ImageSource imageSrc = ImageSource.FromStream(() => stream);
            return imageSrc;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class StringConcatConverter : IMarkupExtension, IValueConverter
    {
        public string PreString { get; set; } = string.Empty;

        public string PostString { get; set; } = string.Empty;

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return PreString + value + PostString;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("Only one way bindings are supported with this converter");
        }
    }

    public class NegateBooleanConverter : IMarkupExtension, IValueConverter
    {
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }

    public class EmbeddedImageSourceConverter : IMarkupExtension, IValueConverter
    {
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string source))
                return null;

            return ImageSource.FromResource("SupermarketProject.FlagImages." + source, typeof(EmbeddedImageSourceConverter).GetTypeInfo().Assembly); ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }
    }
}
