﻿using System;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Microsoft.AppCenter.Crashes;

namespace SupermarketProject.Common.Utilities
{
    public static class Time
    {
        public static async Task<DateTime?> GetTime()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string URL = DependencyService.Get<INativeMethods>().GetTimeURL();
                    client.Timeout = new TimeSpan(0, 0, 5);
                    string response = await client.GetStringAsync(URL);
                    JObject jObj = JObject.Parse(response);
                    int timeStamp = (int)jObj["timestamp"];
                    if (timeStamp < 500000)
                        return null;
                    DateTime date = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    System.Diagnostics.Debug.WriteLine(timeStamp);
                    date = date.AddSeconds(timeStamp);

                    //DateTime date = ConvertTimestampToDateTime (timeStamp);
                    return date;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                System.Diagnostics.Debug.WriteLine(e.ToString());
                return null;
            }
        }

        public static async Task<DateTime?> GetUTCTime()
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string URL = DependencyService.Get<INativeMethods>().GetTimeURL();
                    client.Timeout = new TimeSpan(0, 0, 5);
                    string response = await client.GetStringAsync(URL);
                    JObject jObj = JObject.Parse(response);
                    int timeStamp = (int)jObj["timestamp"];
                    if (timeStamp < 500000)
                        return null;
                    DateTime date = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                    System.Diagnostics.Debug.WriteLine(timeStamp - 10800);
                    date = date.AddSeconds(timeStamp - 10800);

                    //DateTime date = ConvertTimestampToDateTime (timeStamp);
                    return date;

                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                System.Diagnostics.Debug.WriteLine(e.ToString());

                return null;
            }
        }
    }
}

