﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json.Linq;
using Plugin.Connectivity;

namespace SupermarketProject.Common.Utility
{
    public static class ServerInfo
    {
        private const string ParseUrl = "https://westzone.back4app.io/";

        public static async Task<string> GetServerUrl()
        {
            try
            {
                if (CrossConnectivity.Current.IsConnected)
                {
                    using (HttpClient client = new HttpClient())
                    {
                        try
                        {
                            string URL = "https://westzone.1001dubaipanel.ae/api/Server";
                            client.Timeout = new TimeSpan(0, 0, 5);
                            string urlString = await client.GetStringAsync(URL);

                            return string.IsNullOrEmpty(urlString) ? ParseUrl : urlString;
                        }
                        catch (Exception ex)
                        {
                            Crashes.TrackError(ex);
                            Debug.WriteLine("Error getting parse url : " + ex);
                            Debug.WriteLine(ex.ToString());
                            return ParseUrl;
                        }
                    }
                }
            }catch(Exception ex)
            {
                Crashes.TrackError(ex);
                Debug.WriteLine(ex);
            }
            return ParseUrl;
        }

    }
}
