﻿using System.Collections.Generic;
using System.Linq;
using SupermarketProject.Models.Local;
using SupermarketProject.Models.Remote;

namespace SupermarketProject.Common.Utilities
{
    public class BranchArea
    {
        public string AreaId;
        public int minOrderWithout;
        public int minOrder;
        public BranchArea()
        {
        }
        public BranchArea(string areaId, int minOrderWithout, int minOrder)
        {
            this.AreaId = areaId;
            this.minOrder = minOrder;
            this.minOrderWithout = minOrderWithout;
        }
    }
    public class Branch : GeneralPopUpItemModel
    {
        public int ID;
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                base.Name = value;
            }
        }
        public List<BranchArea> Areas;
        public List<string> Buildings;
        public List<PinPoint> PinPoints;
        public PinPoint Location;
        public string MailAddress;
        public string OpenHours;
        public string PhoneNumber { get; set; }
        public bool IsDeleted { get; set; }
        public Branch(int id, string name, List<BranchArea> areas, List<string> buildings, List<PinPoint> pinPoints, PinPoint location, string mailAddress, string openHours, string phoneNumber, bool isDeleted)
        {
            ID = id;
            this.Name = name;
            this.Areas = areas;
            Buildings = buildings;
            this.PinPoints = pinPoints;
            this.Location = location;
            MailAddress = mailAddress;
            OpenHours = openHours;
            PhoneNumber = phoneNumber;
            IsDeleted = isDeleted;
        }
    }
    public static class BranchHelper
    {
        public static List<Branch> BranchList;
        public static List<Branch> CompleteBranchList;

        public static Branch GetBranch(int index)
        {
            return BranchList?.Find(item => item.ID == index);
        }

        public static BranchArea GetCurrentArea()
        {
            int activeBranchId = UserClassFunctions.GetActiveBranchFromUser();
            string activeBuildingId = AddressClassFunctions.GetActiveAddress().BuildingId;
            string activeAreaId = BuildingModel.BuildingList.Find(x => x.objectId == activeBuildingId).AreaId;

            return BranchList?.Find(x => x.ID == activeBranchId).Areas.Find(x => x.AreaId == activeAreaId);
        }

        public static BranchBuildingClass GetCurrentBranchBuilding()
        {
            int activeBranchId = UserClassFunctions.GetActiveBranchFromUser();
            string activeBuildingId = AddressClassFunctions.GetActiveAddress().BuildingId;
            List<BranchBuildingClass> branchBuildings = BranchBuildingModel.BranchBuildingList;

            return branchBuildings?.Find(x => x.BranchId == activeBranchId && x.BuildingId == activeBuildingId);
        }

        public static string DecideBranchName(int branchId)
        {
            Branch branch = BranchList?.Find(item => item.ID == branchId);
            return branch.Name;
        }

        public static string DecideBranchTimings(int branchId)
        {
            Branch branch = BranchList?.Find(item => item.ID == branchId);
            return branch.OpenHours;
        }

        public static List<Branch> GetServingBranches(string buildingId)
        {
            return BranchList?.Where(x => !x.IsDeleted && x.Buildings != null && x.Buildings.Contains(buildingId)).ToList();
        }

    }
}

