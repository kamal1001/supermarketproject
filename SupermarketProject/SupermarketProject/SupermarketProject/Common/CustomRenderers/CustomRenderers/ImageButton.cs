﻿using System;
using SupermarketProject.Common.Utilities;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace SupermarketProject.Common
{
    public class ImageButton : BorderAbsoluteLayout, IDisposable
    {
        public static readonly BindableProperty ImageNameProperty = BindableProperty.Create(nameof(ImageName),
            typeof(string),
            typeof(BorderAbsoluteLayout),
            default(string), propertyChanged: OnImageNameChanged);

        public string ImageName
        {
            get { return (string)GetValue(ImageNameProperty); }
            set { SetValue(ImageNameProperty, value); }
        }
        private static void OnImageNameChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var button = bindable as ImageButton;
            if(!string.IsNullOrWhiteSpace(newvalue as string))
                button._image.Source = newvalue as string;
        }
        public static readonly BindableProperty ImageBoundProperty = BindableProperty.Create(nameof(ImageBound),
            typeof(Rectangle),
            typeof(BorderAbsoluteLayout),
            default(Rectangle), propertyChanged: OnImageBoundChanged);

        public Rectangle ImageBound
        {
            get { return (Rectangle)GetValue(ImageBoundProperty); }
            set { SetValue(ImageBoundProperty, value); }
        }
        private static void OnImageBoundChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var button = bindable as ImageButton;
            if (newvalue != null)
            {
                var bounds = (Rectangle)newvalue;
                button._image.WidthRequest = bounds.Width;
                button._image.HeightRequest = bounds.Height;
                button.Children.Clear();
                button.Children.Add(button._image, bounds);
            }

        }
        private CachedImage _image;
        public ImageButton()
        {
            _image = new CachedImage()
            {
                //WidthRequest = bounds.Width,
                //HeightRequest = bounds.Height,
                CacheDuration = TimeSpan.FromDays(30),
            };
            //Children.Add(_image, bounds);
        }

        public void Dispose()
        {
            Children.Remove(_image);
            _image = null;
        }
    }
}
