﻿using System;
using SupermarketProject.Common.Utilities;
using Xamarin.Forms;

namespace SupermarketProject.Common
{
    public class CustomizableEntry : Entry
    {
        public static readonly BindableProperty FontTypeProperty = BindableProperty.Create("FontType", typeof(MyDevice.FontTypes), typeof(CustomizableEntry), MyDevice.FontTypes.MyriadProRegular);
        public MyDevice.FontTypes FontType
        {
            get { return (MyDevice.FontTypes)GetValue(FontTypeProperty); }
            set { SetValue(FontTypeProperty, value); }
        }
        public static readonly BindableProperty LeftPaddingProperty = BindableProperty.Create("LeftPadding", typeof(int), typeof(CustomizableEntry), 0);
        public int LeftPadding
        {
            get { return (int)GetValue(LeftPaddingProperty); }
            set { SetValue(LeftPaddingProperty, value); }
        }

        public static readonly BindableProperty TopPaddingProperty = BindableProperty.Create("TopPadding", typeof(int), typeof(CustomizableEntry), 0);
        public int TopPadding
        {
            get { return (int)GetValue(TopPaddingProperty); }
            set { SetValue(TopPaddingProperty, value); }
        }


        public static readonly BindableProperty BottomPaddingProperty = BindableProperty.Create("BottomPadding", typeof(int), typeof(CustomizableEntry), 0);
        public int BottomPadding
        {
            get { return (int)GetValue(BottomPaddingProperty); }
            set { SetValue(BottomPaddingProperty, value); }
        }


        public static readonly BindableProperty UnderlineEnabledProperty = BindableProperty.Create("IsUnderlineEnabled", typeof(bool), typeof(CustomizableEntry), false);
        public bool UnderlineEnabled
        {
            get { return (bool)GetValue(UnderlineEnabledProperty); }
            set { SetValue(UnderlineEnabledProperty, value); }
        }

        public static readonly BindableProperty ShouldCenterVerticallyProperty = BindableProperty.Create("ShouldCenterVertically", typeof(bool), typeof(CustomizableEntry), true);
        public bool ShouldCenterVertically
        {
            get { return (bool)GetValue(ShouldCenterVerticallyProperty); }
            set { SetValue(ShouldCenterVerticallyProperty, value); }
        }
    }
}
