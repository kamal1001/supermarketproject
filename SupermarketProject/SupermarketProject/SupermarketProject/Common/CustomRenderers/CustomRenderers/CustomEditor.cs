﻿using SupermarketProject.Common.Utilities;
using Xamarin.Forms;

namespace SupermarketProject.Common.CustomRenderers
{
    public class CustomEditor : Editor
    {
        public static readonly BindableProperty FontTypeProperty = BindableProperty.Create("FontType", typeof(MyDevice.FontTypes), typeof(CustomizableEntry), MyDevice.FontTypes.MyriadProRegular);
        public MyDevice.FontTypes FontType
        {
            get { return (MyDevice.FontTypes)GetValue(FontTypeProperty); }
            set { SetValue(FontTypeProperty, value); }
        }
        public static BindableProperty PlaceholderProperty
        = BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(CustomEditor));

        public static BindableProperty PlaceholderColorProperty
        = BindableProperty.Create(nameof(PlaceholderColor), typeof(Color), typeof(CustomEditor), Color.Gray);

        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public Color PlaceholderColor
        {
            get { return (Color)GetValue(PlaceholderColorProperty); }
            set { SetValue(PlaceholderColorProperty, value); }
        }
    }
}