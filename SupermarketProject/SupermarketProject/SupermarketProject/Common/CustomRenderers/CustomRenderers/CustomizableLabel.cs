﻿using System.Collections.Generic;
using SupermarketProject.Common.Utilities;
using Xamarin.Forms;

namespace SupermarketProject.Common.CustomRenderers
{
	public class CustomizableLabel : Label 
	{
		public static readonly BindableProperty FontTypeProperty = BindableProperty.Create("FontType", typeof(MyDevice.FontTypes), typeof(CustomizableLabel), MyDevice.FontTypes.MyriadProRegular);
		public MyDevice.FontTypes FontType
		{
			get { return (MyDevice.FontTypes)GetValue(FontTypeProperty); }
			set { SetValue(FontTypeProperty, value); }
		}

		public static readonly BindableProperty LineSpacingMultiplierProperty = BindableProperty.Create("LineSpacingMultiplier", typeof(float), typeof(CustomizableLabel), 1.0f);
		public float LineSpacingMultiplier
		{
			get { return (float)GetValue(LineSpacingMultiplierProperty); }
			set { SetValue(LineSpacingMultiplierProperty, value); }
		}

		public static readonly BindableProperty UnderlineEnabledProperty = BindableProperty.Create("UnderlineEnabled", typeof(bool), typeof(CustomizableLabel), false);
		public bool UnderlineEnabled
		{
			get { return (bool)GetValue(UnderlineEnabledProperty); }
			set { SetValue(UnderlineEnabledProperty, value); }
		}

        public static readonly BindableProperty StrikeThroughTextEnabledProperty = BindableProperty.Create("StrikeThroughTextEnabled", typeof(bool), typeof(CustomizableLabel), false);
        public bool StrikeThroughTextEnabled
        {
            get { return (bool)GetValue(StrikeThroughTextEnabledProperty); }
            set { SetValue(StrikeThroughTextEnabledProperty, value); }
        }

        public List<int> StrikeThroughSpansIndexList = new List<int>();
    }
}
