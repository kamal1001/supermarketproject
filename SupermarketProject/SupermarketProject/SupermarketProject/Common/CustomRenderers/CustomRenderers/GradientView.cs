﻿using System;
using Xamarin.Forms;

namespace SupermarketProject
{
    public enum GradientDirection {LeftToRight,RightToLeft,TopToBottom,BottomToTop}
    public class GradientView : View
    {
        public static readonly BindableProperty GradientColorsProperty = BindableProperty.Create("GradientColors", typeof(Color[]), typeof(GradientView), new Color[] { Color.White });

        public Color[] GradientColors
        {
            get { return (Color[])base.GetValue(GradientColorsProperty); }
            set { base.SetValue(GradientColorsProperty, value); }
        }

        public static readonly BindableProperty CornerRadiusProperty = BindableProperty.Create("CornerRadius", typeof(float), typeof(GradientView),0.0f);

        public float CornerRadius
        {
            get { return (float)base.GetValue(CornerRadiusProperty); }
            set { base.SetValue(CornerRadiusProperty, value); }
        }

        public static readonly BindableProperty RoundCornersProperty = BindableProperty.Create("RoundCorners", typeof(bool), typeof(GradientView), false);

        public bool RoundCorners
        {
            get { return (bool)base.GetValue(RoundCornersProperty); }
            set { base.SetValue(RoundCornersProperty, value); }
        }

        public static readonly BindableProperty DirectionProperty = BindableProperty.Create("Direction", typeof(GradientDirection), typeof(GradientView), GradientDirection.TopToBottom);

        public GradientDirection Direction
        {
            get { return (GradientDirection)base.GetValue(DirectionProperty); }
            set { base.SetValue(DirectionProperty, value); }
        }
    }
}
