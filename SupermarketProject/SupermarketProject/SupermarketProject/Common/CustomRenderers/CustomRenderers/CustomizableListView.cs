﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace SupermarketProject.Common.CustomRenderers
{
	public class CustomizableListView : ListView
	{
		public static readonly BindableProperty SelectionColorEnabledProperty = BindableProperty.Create("IsSelectionEnabled", typeof(bool), typeof(CustomizableListView), true);
		public bool IsSelectionEnabled
		{
			get { return (bool)GetValue(SelectionColorEnabledProperty); }
			set { SetValue(SelectionColorEnabledProperty, value); }
		}

		public static readonly BindableProperty RowSpacingProperty = BindableProperty.Create("RowSpacingProperty", typeof(int), typeof(CustomizableListView), 0);
		public int RowSpacing
		{
			get { return (int)GetValue(RowSpacingProperty); }
			set { SetValue(RowSpacingProperty, value); }
		}

		public static readonly BindableProperty YOffsetProperty = BindableProperty.Create("YOffsetProperty", typeof(float), typeof(CustomizableListView), 0f);
		public float YOffset
		{
			get { return (float)GetValue(YOffsetProperty); }
			set { SetValue(YOffsetProperty, value); }
		}

	    public event Action Reload;

	    public void FireReload()
	    {
            Debug.WriteLine("Reload Fired");
            Reload?.Invoke();
	    }
	}
}

