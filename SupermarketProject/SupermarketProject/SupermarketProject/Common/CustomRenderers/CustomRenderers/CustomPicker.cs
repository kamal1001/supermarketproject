﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupermarketProject.Common.Utilities;
using Xamarin.Forms;

namespace SupermarketProject.Common.CustomRenderers
{
    public class CustomPicker : Picker
    {
        public CustomPicker()
        {
            Focused += PickerFocused;
        }
        private void PickerFocused(object sender, FocusEventArgs e)
        {
            if (SelectedIndex == -1 && FocusItemIndex != -1)
            {
                SelectedIndex = FocusItemIndex;
            }
        }

        public static readonly BindableProperty FontTypeProperty = BindableProperty.Create("FontType", typeof(MyDevice.FontTypes), typeof(CustomPicker), MyDevice.FontTypes.MyriadProRegular);
        public MyDevice.FontTypes FontType
        {
            get { return (MyDevice.FontTypes)GetValue(FontTypeProperty); }
            set { SetValue(FontTypeProperty, value); }
        }
        public static BindableProperty FontSizeProperty
        = BindableProperty.Create<CustomPicker, double>(p => p.FontSize, default(double));

        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public static BindableProperty TitleTextColorProperty
        = BindableProperty.Create<CustomPicker, Color>(p => p.TitleTextColor, default(Color));

        public Color TitleTextColor
        {
            get { return (Color)GetValue(TitleTextColorProperty); }
            set { SetValue(TitleTextColorProperty, value); }
        }

        public static readonly BindableProperty LeftPaddingProperty = BindableProperty.Create("LeftPadding", typeof(int), typeof(CustomPicker), 0);
        public int LeftPadding
        {
            get { return (int)GetValue(LeftPaddingProperty); }
            set { SetValue(LeftPaddingProperty, value); }
        }

        public static readonly BindableProperty FocusItemIndexProperty = BindableProperty.Create("FocusItemIndex", typeof(int), typeof(CustomPicker), -1);
        public int FocusItemIndex
        {
            get { return (int)GetValue(FocusItemIndexProperty); }
            set { SetValue(FocusItemIndexProperty, value); }
        }

        public static BindableProperty CustomTextColorProperty
        = BindableProperty.Create<CustomPicker, Color>(p => p.CustomTextColor, default(Color));
        public Color CustomTextColor
        {
            get { return (Color)GetValue(CustomTextColorProperty); }
            set { SetValue(CustomTextColorProperty, value); }
        }

        public static BindableProperty HintTextColorProperty
        = BindableProperty.Create<CustomPicker, Color>(p => p.HintTextColor, default(Color));

        public Color HintTextColor
        {
            get { return (Color)GetValue(HintTextColorProperty); }
            set { SetValue(HintTextColorProperty, value); }
        }

    }
}
