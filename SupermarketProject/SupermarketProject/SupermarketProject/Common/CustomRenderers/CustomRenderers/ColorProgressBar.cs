﻿using System;
using Xamarin.Forms;

namespace SupermarketProject
{
	//https://forums.xamarin.com/discussion/28894/progressbar-color-in-xamarin-forms
	public class ColorProgressBar : ProgressBar
	{
		public static BindableProperty BarColorProperty
			= BindableProperty.Create<ColorProgressBar, Color>(p => p.BarColor, default(Color));

		public Color BarColor
		{
			get { return (Color)GetValue(BarColorProperty); }
			set { SetValue(BarColorProperty, value); }
		}
	}
}
