﻿using System;
using Xamarin.Forms;

namespace SupermarketProject.Common.CustomRenderers
{
    public class CustomScrollView : ScrollView
    {
        public static readonly BindableProperty HideScrollBarProperty = BindableProperty.Create("HideScrollBar", typeof(bool), typeof(CustomScrollView), false);
        public bool HideScrollBar
        {
            get { return (bool)GetValue(HideScrollBarProperty); }
            set { SetValue(HideScrollBarProperty, value); }
        }
    }
}
