﻿using System;
using System.Collections.Generic;

namespace SupermarketProject.Analytics
{
    public interface IAnalytics
    {
        //Firebase Events
        void LogFireBase(string key, Dictionary<string, Object> LogData);

        //Fabric Events
        void LogFabricPurchase(Decimal Price, string Currency, string ItemName, string ItemType, string ItemId, bool Success);
        void LogFabricAddCart(Decimal Price, string Currency, string ItemName, string ItemType, string ItemId);
        void LogFabricStartCheckout(Decimal Price, string Currency, int ItemCount);
        void LogFabricContentView(string ContentName, string ContentType, string ContentId);
        void LogFabricSearch(string Query);
        void LogFabricReatedContent(int Rating, string ContentName, string ContentType, string ContentId);
        void LogFabricSignUp(string Method, bool Success);
        void LogFabricLogin(string Method, bool Success);
    }
}
